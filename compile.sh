#!/bin/bash

ME=$(basename $0)
DIR=$(dirname $0)
FILE_ROOT="js"
MINIFY="false"
NAME="jsu"
OUTPUT_PATH=

TEMP_FILE=

usage()
{
  cat <<EOF

NAME
  $ME -- Utilities for compiling a js library

SYNOPSIS
  $ME -h
  $ME [-r <file root>] -L
  $ME -d
  $ME [-o <output path>] [-m] -c
  $ME [-o <output path>] [-r <file root>] [-m] -A

DESCRIPTION
  The $ME utility has several options for compiling a js library. The second
  and third variants require the manifest to be piped in. All lines that don't
  start with '#' will be considered a filename, and be included in the command.

  The purpose of the 2nd variant is to allow the user to create a manifest file
  that they can then edit by removing or commenting out lines, to then be sent
  into the script.

  The options are as follows:

  -h  Show this help message

  -L  List all of the valid files that could be compiled

  -r  The file root. Defaults to $FILE_ROOT

  -d  List all dependencies

  -c  Compile all the requested files together

  -m  Minify the compiled file

  -A  Compile full project

  -o  Output to a specified path. The name will be $NAME-ver[.min].js. Where
      ver is the value stored in the version file.

USAGE
  A few options for usage:

  1. Create a manifest file, and then feed it to $ME
  >  $ME -L > manifest.txt
  >  cat manifest.txt | $ME -c

  2. Compile all valid files
  >  $ME -L | $ME -c

EOF
}

header()
{
  cat <<EOF
/*

jsu version $(cat $DIR/version)
$(cat $DIR/LICENSE)

*/

EOF
}

generate_manifest()
{
  find "$1" -name '*.js'
}

store_selection()
{
  TEMP_FILE=$(mktemp)
  cat /dev/stdin > "$TEMP_FILE"
}

delete_selection()
{
  rm -f "$TEMP_FILE"
}

included_files()
{
  grep -vE '^#' "$TEMP_FILE"
}

list_dependencies()
{
  cat $(included_files) | grep -E '^// DEPENDENCY:' | sort | uniq
}

compile_()
{
  header
  if [ "$MINIFY" == "true" ]; then
    python3 -m jsmin $(included_files)
  else
    cat $(included_files)
  fi
}

compile_all_()
{
  header
  if [ "$MINIFY" == "true" ]; then
    python3 -m jsmin $(generate_manifest "$FILE_ROOT")
  else
    cat $(generate_manifest "$FILE_ROOT")
  fi
}

output_filename()
{
  v=$(cat "$DIR/version")
  ext="js"
  if [ "$MINIFY" == "true" ]; then
    ext="min.$ext"
  fi
  echo "$NAME-$v.$ext"
}

compile()
{
  store_selection

  if [ -n "$OUTPUT_PATH" ]; then
    MINIFY=false
    compile_ > "$OUTPUT_PATH/$(output_filename)"
    MINIFY=true
    compile_ > "$OUTPUT_PATH/$(output_filename)"
  else
    compile_
  fi

  delete_selection
}

compile_all()
{
  if [ -n "$OUTPUT_PATH" ]; then
    MINIFY=false
    compile_all_ > "$OUTPUT_PATH/$(output_filename)"
    MINIFY=true
    compile_all_ > "$OUTPUT_PATH/$(output_filename)"
  else
    compile_all_
  fi
}

while getopts "hLr:o:dmcA" OPTION; do
  case "$OPTION" in
    h) usage ; exit ;;
    L) generate_manifest "$FILE_ROOT" ; exit ;;
    r) $FILE_ROOT="$OPTARG" ;;
    d) list_dependencies ; exit ;;
    m) MINIFY="true" ;;
    o) OUTPUT_PATH="$OPTARG" ;;
    c) compile ; exit ;;
    A) compile_all ; exit ;;
    ?) usage ; exit 1 ;;
  esac
done
