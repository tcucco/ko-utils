describe("Utility Classes", function() {
  "use strict";

  var I = jsu;

  describe("Statistics", function() {
    var points = [
      [95, 85],
      [85, 95],
      [80, 70],
      [70, 65],
      [60, 70]
    ];

    var S = I.stats;

    it("sum works", function() {
      expect(S.sum(points, S.getX)).toEqual(390);
      expect(S.sum(points, S.getY)).toEqual(385);
    });

    it("average works", function() {
      expect(S.average(points, S.getX)).toEqual(78);
      expect(S.average(points, S.getY)).toEqual(77);
    });

    it("variance population works", function() {
      expect(S.variancePopulation(points, S.getX)).toEqual(146);
      expect(S.variancePopulation(points, S.getY)).toEqual(126);
    });

    it("variance sample works", function() {
      expect(S.varianceSample(points, S.getX)).toEqual(182.5);
      expect(S.varianceSample(points, S.getY)).toEqual(157.5);
    });

    it("standard deviation population", function() {
      expect(S.standardDeviationPopulation(points, S.getX)).toBeCloseTo(12.083, 2);
      expect(S.standardDeviationPopulation(points, S.getY)).toBeCloseTo(11.2249, 2);
    });

    it("standard deviation sample", function() {
      expect(S.standardDeviationSample(points, S.getX)).toBeCloseTo(13.509, 2);
      expect(S.standardDeviationSample(points, S.getY)).toBeCloseTo(12.5499, 2);
    });

    it("linear regression", function() {
      var lr = S.linearRegression(points, S.getX, S.getY);
      var i = lr[0];
      var s = lr[1];
      expect(i).toBeCloseTo(26.780, 2);
      expect(s).toBeCloseTo(0.644, 2);
    });
  });

  describe("Batch Editor", function() {
    xit("works");
  });

  describe("Date Finder", function() {
    describe("start of range", function() {
      var tests = [
        [0, true, "2015-01-05", "2015-01-04", "2015-01-04", "2015-01-04", "2015-01-04"],
        [1, true, "2015-01-05", "2015-01-05", "2015-01-05", "2015-01-05", "2015-01-05"],
        [2, true, "2015-01-05", "2014-12-30", "2014-12-02", "2014-10-07", "2014-01-07"],
        [3, true, "2015-01-05", "2014-12-31", "2014-12-03", "2014-10-01", "2014-01-01"],
        [4, true, "2015-01-05", "2015-01-01", "2015-01-01", "2015-01-01", "2015-01-01"],
        [5, true, "2015-01-05", "2015-01-02", "2015-01-02", "2015-01-02", "2015-01-02"],
        [6, true, "2015-01-05", "2015-01-03", "2015-01-03", "2015-01-03", "2015-01-03"],

        [0, false, "2015-01-05", "2015-01-04", "2015-01-01", "2015-01-01", "2015-01-01"]
      ];

      _.each(tests, function(t) {
        var weekStartsOnDay = t[0];
        var weekBasedBounds = t[1];
        var dt = t[2];
        var sow = t[3];
        var som = t[4];
        var soq = t[5];
        var soy = t[6];

        var df = new I.DateFinder(weekStartsOnDay, weekBasedBounds);
        it(dt + " start of week " + weekStartsOnDay + " " + (weekBasedBounds ? "wbb" : "nwbb"), function() {
          expect(df.getStartOf(dt, "week", "iso")).toEqual(sow);
          expect(df.getStartOf(dt, "month", "iso")).toEqual(som);
          expect(df.getStartOf(dt, "quarter", "iso")).toEqual(soq);
          expect(df.getStartOf(dt, "year", "iso")).toEqual(soy);
        });
      });
    });

    describe("end of range", function() {
      var tests = [
        [0, true, "2014-12-29", "2015-01-03", "2015-01-03", "2015-01-03", "2015-01-03"],
        [1, true, "2014-12-29", "2015-01-04", "2015-01-04", "2015-01-04", "2015-01-04"],
        [2, true, "2014-12-29", "2014-12-29", "2015-01-05", "2015-01-05", "2015-01-05"],
        [3, true, "2014-12-29", "2014-12-30", "2015-01-06", "2015-01-06", "2015-01-06"],
        [4, true, "2014-12-29", "2014-12-31", "2014-12-31", "2014-12-31", "2014-12-31"],
        [5, true, "2014-12-29", "2015-01-01", "2015-01-01", "2015-01-01", "2015-01-01"],
        [6, true, "2014-12-29", "2015-01-02", "2015-01-02", "2015-01-02", "2015-01-02"],

        [0, false, "2014-12-29", "2015-01-03", "2014-12-31", "2014-12-31", "2014-12-31"]
      ];

      _.each(tests, function(t) {
        var weekStartsOnDay = t[0];
        var weekBasedBounds = t[1];
        var dt = t[2];
        var eow = t[3];
        var eom = t[4];
        var eoq = t[5];
        var eoy = t[6];

        var df = new I.DateFinder(weekStartsOnDay, weekBasedBounds);
        it(dt + " start of week " + weekStartsOnDay + " " + (weekBasedBounds ? "wbb" : "nwbb"), function() {
          expect(df.getEndOf(dt, "week", "iso")).toEqual(eow);
          expect(df.getEndOf(dt, "month", "iso")).toEqual(eom);
          expect(df.getEndOf(dt, "quarter", "iso")).toEqual(eoq);
          expect(df.getEndOf(dt, "year", "iso")).toEqual(eoy);
        });
      });
    });

    describe("date range", function() {
      xit("works");
    });
  });

  describe("Delayed Actor", function() {
    var actCount = 0;
    var timeout = 100;
    var action;

    beforeEach(function() {
      action = jasmine.createSpy("action");
      jasmine.clock().install();
    });

    afterEach(function() {
      jasmine.clock().uninstall();
    });

    it("works", function() {
      var da = new I.DelayedActor(action, timeout);

      for (var i = 0; i < 10; ++i) { da.act(); }

      expect(action).not.toHaveBeenCalled();
      jasmine.clock().tick(timeout + 1);
      expect(action).toHaveBeenCalled();
    });
  });

  describe("Drag Drop Manager", function() {
    xit("works");
  });

  describe("Event Aggregator", function() {
    it("works", function() {
      var ea = new I.EventAggregator()
      var unnamedCount = 0;
      var namedCount = 0;
      var sid0 = ea.subscribe(function() { unnamedCount += 1; });
      var sid1 = ea.subscribe("named", function() { namedCount += 1; });
      ea.broadcast(null, "named", {});
      ea.broadcast(null, "something", {});
      expect(unnamedCount).toEqual(2);
      expect(namedCount).toEqual(1);

      ea.unsubscribe(sid0);
      ea.unsubscribe(sid1);
      ea.broadcast(null, "named", {});
      ea.broadcast(null, "something", {});
      expect(unnamedCount).toEqual(2);
      expect(namedCount).toEqual(1);
    });
  });

  describe("Expression Parser", function() {
    var X = jsu.expression;

    describe("Operations", function() {
      var evaluator = new X.Evaluator();
      var expressions = [
        ["false | false", false],
        ["false | true", true],
        ["true | false", true],
        ["true | true", true],
        ["false & false", false],
        ["false & true", false],
        ["true & false", false],
        ["true & true", true],
        ["1 + 2", 3],
        ["1 - 2", -1],
        ["1 = 1", true],
        ["1 = 2", false],
        ["2 = 1", false],
        ["1 > 2", false],
        ["2 > 1", true],
        ["1 > 1", false],
        ["1 >= 2", false],
        ["2 >= 1", true],
        ["1 >= 1", true],
        ["1 < 2", true],
        ["2 < 1", false],
        ["1 < 1", false],
        ["1 <= 2", true],
        ["2 <= 1", false],
        ["1 <= 1", true],
        ["1 != 2", true],
        ["2 != 1", true],
        ["1 != 1", false],
        ["8 * 3", 24],
        ["4 / 2", 2],
        ["!true", false],
        ["!false", true],
        ["!!true", true],
        ["2 ^ 2", 4],
        ["8 ^ 3", 512],
        ["-3", -3],
        ["3 - -3", 6],
        ["+3", 3],
        ["3 - +3", 0],
        ["3 + 4 * 2", 11],
        ["(3 + 4) * 2", 14],
        ["'abc ' + 'def'", "abc def"],
        ['"abc " + "def"', "abc def"],
      ];

      _.each(expressions, function(pair) {
        var ex = pair[0];
        var val = pair[1];
        it(ex + " -> " + val, function() { expect(evaluator.evaluateString(ex)).toEqual(val); });
      });
    });

    describe("Variables", function() {
      var scope = new X.EvaluatorScope();
      scope.pushScope({ getVariable: X.EvaluatorScope.mapToGetter({
        "a": 1,
        "b": 2,
        "$c$3": 4,
        "name": "Trey",
        "space in name": 88
      }) });
      var evaluator = new X.Evaluator(scope);

      var expressions = [
        ["a + b", 3],
        ["b - b", 0],
        ["$c$3", 4],
        ["name + ' ' + name", "Trey Trey"],
        ["[space in name] / 4 = 22", true]
      ];

      _.each(expressions, function(pair) {
        var ex = pair[0];
        var val = pair[1];
        it(ex + " -> " + val, function() { expect(evaluator.evaluateString(ex)).toEqual(val); });
      });
    });

    describe("Functions", function() {
      var scope = new X.EvaluatorScope();
      scope.pushScope({
        getVariable: X.EvaluatorScope.mapToGetter({ "a": 4 }),
        getFunction: X.EvaluatorScope.mapToGetter({
          double: function(v) { return v + v; }
        })
      });
      var evaluator = new X.Evaluator(scope);

      var expressions = [
        ["double(a)", 8]
      ];

      _.each(expressions, function(pair) {
        var ex = pair[0];
        var val = pair[1];
        it(ex + " -> " + val, function() { expect(evaluator.evaluateString(ex)).toEqual(val); });
      });
    });

    describe("Dependency", function() {
      it("Finding variables works", function() {
        var items = [
          { name: "D", expression: "(B + C) / A" },
          { name: "C", expression: "sqrt(B) - A + 10" },
          { name: "A", expression: "3 * 5" },
          { name: "B", expression: "A / 2" },
          { name: "E", expression: "sqrt(pow(A, 2), pow(B, 3)) + sin(theta) - cos(beta)" },
          { name: "beta", expression: "add(1, 2)" }
        ];
        var prepared = {};
        var dependencyChecker = new X.Dependency({
          getName: function(o) { return o.name; },
          getFormula: function(o) { return o.expression; },
          getTokenQueue: function(o) { return prepared[o.name]; },
          setTokenQueue: function(o, tq) { prepared[o.name] = tq; }
        });

        expect(dependencyChecker.getAllVariables(items, items[0])).toEqual({ A: true, B: true, C: true });
        expect(dependencyChecker.getAllVariables(items, items[1])).toEqual({ A: true, B: true });
        expect(dependencyChecker.getAllVariables(items, items[2])).toEqual({});
        expect(dependencyChecker.getAllVariables(items, items[3])).toEqual({ A: true });
        expect(dependencyChecker.getAllVariables(items, items[4])).toEqual({ A: true, B: true, "theta": true, "beta": true });
        expect(dependencyChecker.getFunctions(items[4])).toEqual({ sqrt: true, pow: true, sin: true, cos: true });
      });

      it("Ordering items works", function() {
        var items = [
          { name: "D", expression: "(B + C) / A" },
          { name: "C", expression: "sqrt(B) - A + 10" },
          { name: "A", expression: "3 * 5" },
          { name: "B", expression: "A / 2" }
        ];
        var prepared = {};
        var dependencyChecker = new X.Dependency({
          getName: function(o) { return o.name; },
          getFormula: function(o) { return o.expression; },
          getTokenQueue: function(o) { return prepared[o.name]; },
          setTokenQueue: function(o, tq) { prepared[o.name] = tq; }
        });
        var ordered = dependencyChecker.order(items);
        var idx = _.object(_.map(items, function(i) { return [i.name, _.indexOf(ordered, i)]; }));
        expect(idx["A"]).toBeLessThan(idx["B"]);
        expect(idx["A"]).toBeLessThan(idx["C"]);
        expect(idx["A"]).toBeLessThan(idx["D"]);
        expect(idx["B"]).toBeLessThan(idx["C"]);
        expect(idx["B"]).toBeLessThan(idx["D"]);
        expect(idx["C"]).toBeLessThan(idx["D"]);
      });

      it("Finds circular references", function() {
        var items = [
          { name: "A", expression: "2  / D * 18" },
          { name: "B", expression: "pow(A, 2)" },
          { name: "C", expression: "B * A" },
          { name: "D", expression: "C + B / A" },
          { name: "E", expression: "5 + 6" }
        ];
        var prepared = {};
        var dependencyChecker = new X.Dependency({
          getName: function(o) { return o.name; },
          getFormula: function(o) { return o.expression; },
          getTokenQueue: function(o) { return prepared[o.name]; },
          setTokenQueue: function(o, tq) { prepared[o.name] = tq; }
        });
        var circular = dependencyChecker.findCircularReferences(items);
        expect(circular).toEqual({ A: true, B: true, C: true, D: true });
      });
    });
  });

  describe("Formatter", function() {
    var F = jsu.Formatter;

    it("Date methods work", function() {
      expect(F.date("2014-11-06", "ISO")).toEqual("2014-11-06");
      expect(F.date("2014-11-06", "US")).toEqual("11/06/2014");
      expect(F.date("2014-11-06", "US-Long")).toEqual("11/06/2014");
      expect(F.date("2014-11-06", "US-Short")).toEqual("11/06/14");
      expect(F.date("2014-11-06", "UK")).toEqual("06/11/2014");
      expect(F.date("2014-11-06", "UK-Long")).toEqual("06/11/2014");
      expect(F.date("2014-11-06", "UK-Short")).toEqual("06/11/14");

      expect(F.date("06/11/14", "ISO", "US-Short")).toEqual("2014-06-11");
      expect(F.date("06/11/14", "ISO", "UK-Short")).toEqual("2014-11-06");
    });

    it("Time methods work", function() {
      expect(F.time("01:35:40 pm", "24 HR-Long", "12 HR-Long")).toEqual("13:35:40");
      expect(F.time("01:35:40 pm", "24 HR-Short", "12 HR-Long")).toEqual("13:35");
      expect(F.time("01:35:40 pm", "24 HR", "12 HR-Long")).toEqual("13:35");
      expect(F.time("01:35:40 pm", "12 HR-Long", "12 HR-Long")).toEqual("1:35:40 pm");
      expect(F.time("01:35:40 pm", "12 HR-Short", "12 HR-Long")).toEqual("1:35 pm");
      expect(F.time("01:35:40 pm", "12 HR", "12 HR-Long")).toEqual("1:35 pm");
    });

    it("Date time methods work", function() {
      expect(F.dateTime("11/06/2014 1:35 PM", "ISO", "24 HR-Long", "T")).toEqual("2014-11-06T13:35:00");
    });

    it("Number methods work", function() {
      var n0 = 120000 / 23;

      var tests = [
        [n0, "5,217.391304", { decimalPlaces: -1 } , true],
        [n0, "5,217", { decimalPlaces: 0 }],
        [n0, "5,217.39", { decimalPlaces: 2 }],
        [n0, "5217", { decimalPlaces: 0, thousandsSeparator: "" }],
        [n0, "5.217,39", { decimalPlaces: 2, thousandsSeparator: ".", decimalSeparator: "," }],
        ["abc", "N/A"],
        [2.5, "2.50", { decimalPlaces: 2, decimalMode: "fixed" }],
        [2.5, "2.5", { decimalPlaces: 2, decimalMode: "trimzeros" }],
        [1 / 3, "0.3", { decimalMode: "trimrepeating" }],
        [7 / 12, "0.583", { decimalMode: "trimrepeating+" }],
        [1500, "1.5K", { abbreviate: true }],
        [1820000, "1.82M", { abbreviate: true }],
        [1750000000, "1.75B", { abbreviate: true }],
        [1200000000000, "1.2T", { abbreviate: true }],
        [100, "+100", { signed: true }],
        [-100, "-100"],
        [-100, "-$100", { prefix: "$" }],
        [100, "+$100", { prefix: "$", signed: true }]
      ];

      _.each(tests, function(t) {
        var input = t[0];
        var output = t[1];
        var opts = t[2];

        if (t[3]) {
          expect(F.number(input, opts).indexOf(output)).toEqual(0);
        } else {
          expect(F.number(input, opts)).toEqual(output);
        }
      });
    });

    it("String methods work", function() {
      expect(F.string("{0} is {1} years {2}", "Trey", 32, "o" + "ld")).toEqual("Trey is 32 years old");
    });

    it("Instance methods work", function() {
      var f = new F({
        dateFormat: "US-Short",
        timeFormat: "12 HR-Short",
        timestampJoiner: " ",
        numberFormat: {
          currencySymbol: "$",
          nullString: "--"
        }
      });

      expect(f.dateFormat()).toEqual("MM/DD/YY");
      expect(f.timeFormat()).toEqual("h:mm a");
      expect(f.date("2014-11-06")).toEqual("11/06/14");
      expect(f.time("2014-11-06 13:15")).toEqual("1:15 pm");
      expect(f.number(2.6)).toEqual("2.6");
      expect(f.integer(2.6)).toEqual("3");
      expect(f.money(2.6)).toEqual("$2.60");
      expect(f.compactNumber(8 / 11)).toEqual("0.727");
      expect(f.abbrNumber(1500)).toEqual("1.5K");
      expect(f.string("{0} / {1} = {2}", 2268, 54, 42)).toEqual("2268 / 54 = 42");
    });
  });

  describe("Full Text Search", function() {
    function Book(data) {
      // NOTE: I'm setting the properties up in this nested fashion to test out property chains.
      //       This is very contrived.

      this.from = {
        author: ko.observable(data.author),
        publisher: ko.observable(data.publisher)
      };
      this.title = {
        main: ko.observable(data.title),
        sub: ko.observable(data.subtitle)
      };
      this.description = ko.observable(data.description);
      this.price = ko.observable(data.price);
    }

    function getRawBookData() {
      return [
        {
          "author": "Joyce Carol Oates",
          "description": "Oates's extravagantly horrifying, funny and prolix postmodern Gothic novel purports to be the definitive account of a curse that infected bucolic Princeton, N.J., in 1905 and 1906.",
          "price": "27.99",
          "publisher": "Ecco/HarperCollins",
          "subtitle": null,
          "title": "The Accursed"
        },
        {
          "author": "James Salter",
          "description": "Salter's first novel in more than 30 years, which follows the loves and losses of a World War II veteran, is an ambitious departure from his previous work and, at a stroke, demolishes any talk of twilight.",
          "price": "26.95",
          "publisher": "Knopf",
          "subtitle": null,
          "title": "All That Is"
        },
        {
          "author": "Chimamanda Ngozi Adichie",
          "description": "This witheringly trenchant novel scrutinizes blackness in America, Nigeria and Britain.",
          "price": "26.95",
          "publisher": "Knopf",
          "subtitle": null,
          "title": "Americanah"
        },
        {
          "author": "Thomas Pynchon",
          "description": "Airliners crash not only into the twin towers but into a shaggy-dog tale involving a fraud investigator and a white-collar outlaw in this vital, audacious novel.",
          "price": "28.95",
          "publisher": "Penguin Press",
          "subtitle": null,
          "title": "Bleeding Edge"
        },
        {
          "author": "Edward Hoagland",
          "description": "The adventure-seeking protagonist of Hoagland's novel is swept up in the chaos of southern Sudan.",
          "price": "23.95",
          "publisher": "Arcade",
          "subtitle": "An African Apocalypse",
          "title": "Children Are Diamonds"
        },
        {
          "author": "Dave Eggers",
          "description": "In a disturbing not-too-distant future, human existence flows through the portal of a company that gives Eggers's novel its title.",
          "price": "27.95",
          "publisher": "Knopf/McSweeney's",
          "subtitle": null,
          "title": "The Circle"
        },
        {
          "author": "Edwidge Danticat",
          "description": "Danticat's novel is less about a Haitian girl who disappears on her birthday than about the heart of a magical seaside village.",
          "price": "25.95",
          "publisher": "Knopf",
          "subtitle": null,
          "title": "Claire Of The Sea Light"
        },
        {
          "author": "Aimee Bender",
          "description": "Physical objects help Bender's characters grasp an overwhelming world.",
          "price": "25.95",
          "publisher": "Doubleday",
          "subtitle": "Stories",
          "title": "The Color Master"
        },
        {
          "author": "Anthony Marra",
          "description": "Odds against survival are high for the characters of Marra's extraordinary first novel, set in war-torn Chechnya.",
          "price": "26",
          "publisher": "Hogarth",
          "subtitle": null,
          "title": "A Constellation Of Vital Phenomena"
        },
        {
          "author": "Herman Koch. Translated by Sam Garrett",
          "description": "In this clever, dark Dutch novel, two couples dine out under the cloud of a terrible crime committed by their teenage sons.",
          "price": "24",
          "publisher": "Hogarth",
          "subtitle": null,
          "title": "The Dinner"
        },
        {
          "author": "Andre Dubus III",
          "description": "Four linked stories expose their characters' bottomless needs and stubborn weaknesses.",
          "price": "25.95",
          "publisher": "Norton",
          "subtitle": null,
          "title": "Dirty Love"
        },
        {
          "author": "Jonathan Lethem",
          "description": "Spanning 80 years and three generations, Lethem's novel realistically portrays an enchanted - or disenchanted - garden of American leftists in Queens.",
          "price": "27.95",
          "publisher": "Doubleday",
          "subtitle": null,
          "title": "Dissident Gardens"
        },
        {
          "author": "Stephen King",
          "description": "Now grown up, Danny, the boy with psycho-intuitive powers in \"The Shining,\" helps another threatened magic child in a novel that shares the virtues of King's best work.",
          "price": "30",
          "publisher": "Scribner",
          "subtitle": null,
          "title": "Doctor Sleep"
        },
        {
          "author": "Kathryn Davis",
          "description": "A schoolteacher takes an unusual lover in this astonishing, double-hinged novel set in a fantastical suburbia.",
          "price": "24",
          "publisher": "Graywolf",
          "subtitle": null,
          "title": "Duplex"
        },
        {
          "author": "Elizabeth Graver",
          "description": "A summer house on the Massachusetts coast both shelters and isolates the wealthy family in Graver's eloquent multigenerational novel.",
          "price": "25.99",
          "publisher": "Harper",
          "subtitle": null,
          "title": "The End Of The Point"
        },
        {
          "author": "Rachel Kushner",
          "description": "In Kushner's frequently dazzling second novel, an impressionable artist navigates the volatile worlds of New York and Rome in the 1970s.",
          "price": "26.99",
          "publisher": "Scribner",
          "subtitle": null,
          "title": "The Flamethrowers"
        },
        {
          "author": "Donna Tartt",
          "description": "The \"Goldfinch\" of the title of Tartt's smartly written Dickensian novel is a painting smuggled through the early years of a boy's life - his prize, his guilt and his burden.",
          "price": "30",
          "publisher": "Little, Brown",
          "subtitle": null,
          "title": "The Goldfinch"
        },
        {
          "author": "James McBride",
          "description": "McBride's romp of a novel, the 2013 National Book Award winner, is narrated by a freed slave boy who passes as a girl. It's a risky portrait of the radical abolitionist John Brown in which irreverence becomes a new form of homage.",
          "price": "27.95",
          "publisher": "Riverhead",
          "subtitle": null,
          "title": "The Good Lord Bird"
        },
        {
          "author": "Ramona Ausubel",
          "description": "Ausubel's fantastical collection traces a cycle of transformation: from love to conception to gestation to birth.",
          "price": "26.95",
          "publisher": "Riverhead",
          "subtitle": "Stories",
          "title": "A Guide To Being Born"
        },
        {
          "author": "Lore Segal",
          "description": "In Segal's darkly comic novel, dementia becomes contagious at a Manhattan hospital.",
          "price": "23.95",
          "publisher": "Melville House",
          "subtitle": null,
          "title": "Half The Kingdom"
        },
        {
          "author": "Jamie Quatro",
          "description": "Quatro's strange, thrilling and disarmingly honest first collection draws from a pool of resonant themes (Christianity, marital infidelity, cancer, running) in agile recombinations.",
          "price": "24",
          "publisher": "Grove",
          "subtitle": "Stories",
          "title": "I Want To Show You More"
        },
        {
          "author": "Andrew Sean Greer",
          "description": "A distraught woman inhabits different selves across the 20th century in Greer's elegiac novel.",
          "price": "26.99",
          "publisher": "Ecco/HarperCollins",
          "subtitle": null,
          "title": "The Impossible Lives Of Greta Wells"
        },
        {
          "author": "Javier Mar\u00edas. Translated by Margaret Jull Costa",
          "description": "Amid a proliferation of alternative perspectives, Mar\u00edas's novel explores its female narrator's relationship with the widow and the best friend of a murdered man.",
          "price": "26.95",
          "publisher": "Knopf",
          "subtitle": null,
          "title": "The Infatuations"
        },
        {
          "author": "Meg Wolitzer",
          "description": "Wolitzer's enveloping novel offers a fresh take on the theme of self-invention, with a heroine who asks herself whether the ambitious men and women in her circle have inaccurately defined success.",
          "price": "27.95",
          "publisher": "Riverhead",
          "subtitle": null,
          "title": "The Interestings"
        },
        {
          "author": "Kate Atkinson",
          "description": "Atkinson's heroine, born in 1910, keeps dying and dying again, as she experiences the alternate courses her destiny might have taken.",
          "price": "27.99",
          "publisher": "Reagan Arthur/Little, Brown",
          "subtitle": null,
          "title": "Life After Life"
        },
        {
          "author": "Allan Gurganus",
          "description": "This triptych, set in Gurganus's familiar Falls, N.C., showcases the increasing universality of his imaginative powers.",
          "price": "25.95",
          "publisher": "Liveright",
          "subtitle": "Novellas",
          "title": "Local Souls"
        },
        {
          "author": "Jo Baker",
          "description": "Baker's charming novel offers an affecting look at the world of \"Pride and Prejudice\" from the point of view of the Bennets' servants' hall.",
          "price": "25.95",
          "publisher": "Knopf",
          "subtitle": null,
          "title": "Longbourn"
        },
        {
          "author": "David Rakoff",
          "description": "Rakoff completed his novel-in-couplets, whose characters live the title's verbs, just before his death in 2012.",
          "price": "26.95",
          "publisher": "Doubleday",
          "subtitle": null,
          "title": "Love, Dishonor, Marry, Die, Cherish, Perish"
        },
        {
          "author": "Jhumpa Lahiri",
          "description": "After his radical brother is killed, an Indian scientist brings his widow to join him in America in Lahiri's efficiently written novel.",
          "price": "27.95",
          "publisher": "Knopf",
          "subtitle": null,
          "title": "The Lowland"
        },
        {
          "author": "Eleanor Catton",
          "description": "In her Booker Prize winner, a love story and mystery set in New Zealand, Catton has built a lively parody of a 19th-century novel, while creating something utterly new for the 21st.",
          "price": "27",
          "publisher": "Little, Brown",
          "subtitle": null,
          "title": "The Luminaries"
        },
        {
          "author": "Margaret Atwood",
          "description": "The survivors of \"Oryx and Crake\" and \"The Year of the Flood\" await a final showdown, in a trilogy's concluding entry.",
          "price": "27.95",
          "publisher": "Nan A. Talese/Doubleday",
          "subtitle": null,
          "title": "Maddaddam"
        },
        {
          "author": "Alexander Maksik",
          "description": "Maksik's forceful novel illuminates the life of a Liberian woman who flees her troubled past to seek refuge on an Aegean island.",
          "price": "24.95",
          "publisher": "Knopf",
          "subtitle": null,
          "title": "A Marker To Measure Drift"
        },
        {
          "author": "Frank Bidart",
          "description": "To immerse oneself in these poems is to enter a crowd of unusual characters: artistic geniuses, violent misfits, dramatic self-accusers (including the poet himself).",
          "price": "24",
          "publisher": "Farrar, Straus & Giroux",
          "subtitle": null,
          "title": "Metaphysical Dog"
        },
        {
          "author": "Brenda Shaughnessy",
          "description": "In these emotionally charged and gorgeously constructed poems, Shaughnessy imagines a world without a child's pain.",
          "price": "16",
          "publisher": "Copper Canyon, paper",
          "subtitle": null,
          "title": "Our Andromeda"
        },
        {
          "author": "Amity Gaige",
          "description": "In Gaige's scenic novel, a man with a long-established false identity goes on the run with his 6-year-old daughter.",
          "price": "21.99",
          "publisher": "Twelve",
          "subtitle": null,
          "title": "Schroder"
        },
        {
          "author": "Elizabeth Gilbert",
          "description": "In this winning novel by the author of \"Eat, Pray, Love,\" a botanist's hunger for explanations carries her through the better part of Darwin's century, and to Tahiti.",
          "price": "28.95",
          "publisher": "Viking",
          "subtitle": null,
          "title": "The Signature Of All Things"
        },
        {
          "author": "Alice McDermott",
          "description": "Through scattered recollections, this novel sifts the significance of an ordinary life.",
          "price": "25",
          "publisher": "Farrar, Straus & Giroux",
          "subtitle": null,
          "title": "Someone"
        },
        {
          "author": "Philipp Meyer",
          "description": "Members of a Texas clan grope their way from the ordeals of the frontier to celebrity culture's absurdities in this masterly multigenerational saga.",
          "price": "27.99",
          "publisher": "Ecco/HarperCollins",
          "subtitle": null,
          "title": "The Son"
        },
        {
          "author": "Juan Gabriel V\u00e1squez. Translated by Anne McLean",
          "description": "This gripping Colombian novel, built on the country's tragic history with the drug trade, meditates on love, fate and death.",
          "price": "27.95",
          "publisher": "Riverhead",
          "subtitle": null,
          "title": "The Sound Of Things Falling"
        },
        {
          "author": "J. M. Ledgard",
          "description": "This hard-edged, well-written novel involves a terrorist hostage-taking and a perilous deep-sea dive.",
          "price": "15.95",
          "publisher": "Coffee House, paper",
          "subtitle": null,
          "title": "Submergence"
        },
        {
          "author": "Norman Rush",
          "description": "Amid dark humor both mournful and absurd, former classmates converge on the hilltop estate of a friend who has died in a freak accident.",
          "price": "26.95",
          "publisher": "Knopf",
          "subtitle": null,
          "title": "Subtle Bodies"
        },
        {
          "author": "George Saunders",
          "description": "Saunders's relentless humor and beatific generosity of spirit keep his highly moral tales from succumbing to life's darker aspects.",
          "price": "26",
          "publisher": "Random House",
          "subtitle": "Stories",
          "title": "Tenth Of December"
        },
        {
          "author": "Ayana Mathis",
          "description": "Mathis's deeply felt first novel works at the rough edges of history, within a brutal and poetic allegory of a black family beset by tribulations after the Great Migration to the North.",
          "price": "24.95",
          "publisher": "Knopf",
          "subtitle": null,
          "title": "The Twelve Tribes Of Hattie"
        },
        {
          "author": "David Leavitt",
          "description": "In Leavitt's atmospheric novel of 1940 Lisbon, as two couples await passage to New York, the husbands embark on an affair.",
          "price": "25",
          "publisher": "Bloomsbury",
          "subtitle": null,
          "title": "The Two Hotel Francforts"
        },
        {
          "author": "Amy Tan",
          "description": "This wrenching novel by the author of \"The Joy Luck Club\" follows mother and daughter courtesans over four decades.",
          "price": "29.99",
          "publisher": "Ecco/HarperCollins",
          "subtitle": null,
          "title": "The Valley Of Amazement"
        },
        {
          "author": "Jonathan Miles",
          "description": "Linking disparate characters and story threads, Miles's novel explores varieties of waste and decay in a consumer world.",
          "price": "26",
          "publisher": "Houghton Mifflin Harcourt",
          "subtitle": null,
          "title": "Want Not"
        },
        {
          "author": "Karen Joy Fowler",
          "description": "This surreptitiously smart novel's big reveal slyly recalls a tabloid headline: \"Girl and Chimp Twinned at Birth in Psychological Experiment.\"",
          "price": "26.95",
          "publisher": "Marian Wood/Putnam",
          "subtitle": null,
          "title": "We Are All Completely Beside Ourselves"
        },
        {
          "author": "NoViolet Bulawayo",
          "description": "A Zimbabwean moves to Detroit in Bulawayo's striking first novel.",
          "price": "25",
          "publisher": "Reagan Arthur/Little, Brown",
          "subtitle": null,
          "title": "We Need New Names"
        },
        {
          "author": "Fiona Maazel",
          "description": "Maazel's restlessly antic novel examines the concurrent urges for solitude and intimacy.",
          "price": "26",
          "publisher": "Graywolf",
          "subtitle": null,
          "title": "Woke Up Lonely"
        },
        {
          "author": "Claire Messud",
          "description": "Messud's ingenious, disquieting novel of outsize conflicts tells the story of a thwarted artist who finds herself bewitched by a boy and his parents.",
          "price": "25.95",
          "publisher": "Knopf",
          "subtitle": null,
          "title": "The Woman Upstairs"
        },
        {
          "author": "Alan S. Blinder",
          "description": "The former Fed vice chairman says confidence would have returned faster with better government communication about policy.",
          "price": "29.95",
          "publisher": "Penguin Press",
          "subtitle": "The Financial Crisis, the Response, and the Work Ahead",
          "title": "After The Music Stopped"
        },
        {
          "author": "Sasha Abramsky",
          "description": "This ambitious study, based on Abramsky's travels around the country meeting the poor, both describes and prescribes.",
          "price": "26.99",
          "publisher": "Nation Books",
          "subtitle": "How the Other Half Still Lives",
          "title": "The American Way Of Poverty"
        },
        {
          "author": "Bernard Bailyn",
          "description": "A noted Harvard historian looks at the chaotic decades between Jamestown and King Philip's War.",
          "price": "35",
          "publisher": "Knopf",
          "subtitle": "The Conflict of Civilizations, 1600-1675",
          "title": "The Barbarous Years. The Peopling Of British North America"
        },
        {
          "author": "Anita Raghavan",
          "description": "Indian-Americans populate every aspect of this meticulously reported true-life business thriller.",
          "price": "29",
          "publisher": "Business Plus",
          "subtitle": "The Rise of the Indian-American Elite and the Fall of the Galleon Hedge Fund",
          "title": "The Billionaire's Apprentice"
        },
        {
          "author": "Gary J. Bass",
          "description": "Bass reveals the sordid White House diplomacy that attended the birth of Bangladesh in 1971.",
          "price": "30",
          "publisher": "Knopf",
          "subtitle": "Nixon, Kissinger, and a Forgotten Genocide",
          "title": "The Blood Telegram"
        },
        {
          "author": "Jill Lepore",
          "description": "Ben Franklin's sister bore 12 children and mostly led a life of hardship, but the two corresponded constantly.",
          "price": "27.95",
          "publisher": "Knopf",
          "subtitle": "The Life and Opinions of Jane Franklin",
          "title": "Book Of Ages"
        },
        {
          "author": "Roger Rosenblatt",
          "description": "In his memoir, Rosenblatt recalls being a boy learning to see, and to live, in the city he scrutinizes.",
          "price": "19.99",
          "publisher": "Ecco/HarperCollins",
          "subtitle": "A New York Childhood",
          "title": "The Boy Detective"
        },
        {
          "author": "Doris Kearns Goodwin",
          "description": "Historical parallels in Goodwin's latest time machine implicitly ask us to look at our own age.",
          "price": "40",
          "publisher": "Simon & Schuster",
          "subtitle": "Theodore Roosevelt, William Howard Taft, and the Golden Age of Journalism",
          "title": "The Bully Pulpit"
        },
        {
          "author": "George Johnson",
          "description": "Johnson's fascinating look at cancer reveals certain profound truths about life itself.",
          "price": "27.95",
          "publisher": "Knopf",
          "subtitle": "Unlocking Medicine's Deepest Mystery",
          "title": "The Cancer Chronicles"
        },
        {
          "author": "Max Hastings",
          "description": "This excellent chronicle of World War I's first months by a British military historian dispels some popular myths.",
          "price": "35",
          "publisher": "Knopf",
          "subtitle": "Europe Goes to War",
          "title": "Catastrophe 1914"
        },
        {
          "author": "Eric Schlosser",
          "description": "A disquieting but riveting examination of nuclear risk.",
          "price": "36",
          "publisher": "Penguin Press",
          "subtitle": "Nuclear Weapons, the Damascus Accident, and the Illusion of Safety",
          "title": "Command And Control"
        },
        {
          "author": "Edna O'Brien",
          "description": "O'Brien reflects on a fraught and distinguished life, from the restraints of her Irish childhood to literary stardom.",
          "price": "27.99",
          "publisher": "Little, Brown",
          "subtitle": "A Memoir",
          "title": "Country Girl"
        },
        {
          "author": "Peter Baker",
          "description": "Baker's treatment of the George W. Bush administration is haunted by the question of who was in charge.",
          "price": "35",
          "publisher": "Doubleday",
          "subtitle": "Bush and Cheney in the White House",
          "title": "Days Of Fire"
        },
        {
          "author": "Brenda Wineapple",
          "description": "A masterly Civil War-era history, full of foiled schemes, misfired plans and less-than-happy endings.",
          "price": "35",
          "publisher": "Harper",
          "subtitle": "Confidence, Crisis, and Compromise, 1848-1877",
          "title": "Ecstatic Nation"
        },
        {
          "author": "Jung Chang",
          "description": "Chang portrays Cixi as a proto-feminist and reformer in this authoritative account.",
          "price": "30",
          "publisher": "Knopf",
          "subtitle": "The Concubine Who Launched Modern China",
          "title": "Empress Dowager Cixi"
        },
        {
          "author": "Rebecca Solnit",
          "description": "Digressive essays, loosely about storytelling, reflect a difficult year in Solnit's life.",
          "price": "25.95",
          "publisher": "Viking",
          "subtitle": null,
          "title": "The Faraway Nearby"
        },
        {
          "author": "Sheri Fink",
          "description": "The case of a surgeon suspected of euthanizing patients during the Katrina disaster.",
          "price": "27",
          "publisher": "Crown",
          "subtitle": "Life and Death in a Storm-Ravaged Hospital",
          "title": "Five Days At Memorial"
        },
        {
          "author": "Lawrence Wright",
          "description": "The author of \"The Looming Tower\" takes a calm and neutral stance toward Scientology, but makes clear it's like no other church on earth.",
          "price": "28.95",
          "publisher": "Knopf",
          "subtitle": "Scientology, Hollywood, and the Prison of Belief",
          "title": "Going Clear"
        },
        {
          "author": "Rick Atkinson",
          "description": "The final volume of Atkinson's monumental war trilogy shows that the road to Berlin was far from smooth.",
          "price": "40",
          "publisher": "Holt",
          "subtitle": "The War in Western Europe, 1944-1945",
          "title": "The Guns At Last Light"
        },
        {
          "author": "Jane Ridley",
          "description": "He was vain, gluttonous, promiscuous and none too bright, but \"Bertie\" emerges as an appealing character in Ridley's superb book.",
          "price": "35",
          "publisher": "Random House",
          "subtitle": "A Life of Edward VII, the Playboy Prince",
          "title": "The Heir Apparent"
        },
        {
          "author": "Amanda Lindhout and Sara Corbett",
          "description": "A searing memoir of a young woman's brutal kidnapping in Somalia.",
          "price": "27",
          "publisher": "Scribner",
          "subtitle": null,
          "title": "A House In The Sky"
        },
        {
          "author": "Leo Damrosch",
          "description": "A commanding biography by a Harvard professor.",
          "price": "35",
          "publisher": "Yale University",
          "subtitle": "His Life and His World",
          "title": "Jonathan Swift"
        },
        {
          "author": "Katy Butler",
          "description": "Butler's study of the flaws in end-of-life care mixes personal narrative and tough reporting.",
          "price": "25",
          "publisher": "Scribner",
          "subtitle": "The Path to a Better Way of Death",
          "title": "Knocking On Heaven's Door"
        },
        {
          "author": "Scott Anderson",
          "description": "By contextualizing T. E. Lawrence, Anderson is able to address modern themes like oil, jihad and the Arab-Jewish conflict.",
          "price": "28.95",
          "publisher": "Doubleday",
          "subtitle": "War, Deceit, Imperial Folly and the Making of the Modern Middle East",
          "title": "Lawrence In Arabia"
        },
        {
          "author": "Sheryl Sandberg with Nell Scovell",
          "description": "The lesson conveyed loud and clear by the Facebook executive is that women should step forward and not doubt their ability to combine work and family.",
          "price": "24.95",
          "publisher": "Knopf",
          "subtitle": "Women, Work, and the Will to Lead",
          "title": "Lean In"
        },
        {
          "author": "Robert Kolker",
          "description": "Cases of troubled young Internet prostitutes murdered on Long Island add up to a nuanced look at prostitution today.",
          "price": "25.99",
          "publisher": "Harper",
          "subtitle": "An Unsolved American Mystery",
          "title": "Lost Girls"
        },
        {
          "author": "Mary Ruefle",
          "description": "The poet muses knowingly and merrily on language, writing and speaking sentences that last lifetimes.",
          "price": "25",
          "publisher": "Wave Books, paper",
          "subtitle": "Collected Lectures",
          "title": "Madness, Rack, And Honey"
        },
        {
          "author": "Jeff Guinn",
          "description": "Guinn's tour de force examines Manson's rise and fall, the 1960s music industry and the decade's bizarre ambience.",
          "price": "27.50",
          "publisher": "Simon & Schuster",
          "subtitle": "The Life and Times of Charles Manson",
          "title": "Manson"
        },
        {
          "author": "Megan Marshall",
          "description": "Fuller's extensive intellectual accomplishments are set in contrast with her romantic disappointments.",
          "price": "30",
          "publisher": "Houghton Mifflin Harcourt",
          "subtitle": "A New American Life",
          "title": "Margaret Fuller"
        },
        {
          "author": "Jesmyn Ward",
          "description": "A raw, beautiful elegy for Ward's brother and four male friends, who died young in Mississippi between 2000 and 2004.",
          "price": "26",
          "publisher": "Bloomsbury",
          "subtitle": "A Memoir",
          "title": "Men We Reaped"
        },
        {
          "author": "Carla Kaplan",
          "description": "A remarkable look at the white women who sought a place in the Harlem Renaissance.",
          "price": "28.99",
          "publisher": "Harper",
          "subtitle": "The White Women of the Black Renaissance",
          "title": "Miss Anne In Harlem"
        },
        {
          "author": "Sonia Sotomayor",
          "description": "Mostly skirting her legal views, the Supreme Court justice's memoir reveals much about her family, school and years at Princeton.",
          "price": "27.95",
          "publisher": "Knopf",
          "subtitle": null,
          "title": "My Beloved World"
        },
        {
          "author": "Ari Shavit",
          "description": "Shavit, a columnist for Haaretz, expresses both solidarity with and criticism of his countrymen in this important and powerful book.",
          "price": "28",
          "publisher": "Spiegel & Grau",
          "subtitle": "The Triumph and Tragedy of Israel",
          "title": "My Promised Land"
        },
        {
          "author": "Artemis Cooper",
          "description": "The British wayfarer and travel writer is the subject of Cooper's affectionate, informed biography.",
          "price": "30",
          "publisher": "New York Review Books",
          "subtitle": "An Adventure",
          "title": "Patrick Leigh Fermor"
        },
        {
          "author": "Margalit Fox",
          "description": "Focusing on an unheralded but heroic Brooklyn classics professor, Fox turns the decipherment of Linear B into a detective story.",
          "price": "27.99",
          "publisher": "Ecco/HarperCollins",
          "subtitle": "The Quest to Crack an Ancient Code",
          "title": "The Riddle Of The Labyrinth"
        },
        {
          "author": "Brendan I. Koerner",
          "description": "Refusing to make '60s avatars of the unlikely couple behind a 1972 skyjacking, Koerner finds a deeper truth about the nature of extremism.",
          "price": "26",
          "publisher": "Crown",
          "subtitle": "Love and Terror in the Golden Age of Hijacking",
          "title": "The Skies Belong To Us"
        },
        {
          "author": "Christopher Clark",
          "description": "A Cambridge professor offers a thoroughly comprehensible account of the polarization of a continent, without fixing guilt on one leader or nation.",
          "price": "29.99",
          "publisher": "Harper",
          "subtitle": "How Europe Went to War in 1914",
          "title": "The Sleepwalkers"
        },
        {
          "author": "Amanda Ripley",
          "description": "A look at countries that are outeducating us - Finland, South Korea, Poland - through the eyes of American high school students abroad.",
          "price": "28",
          "publisher": "Simon & Schuster",
          "subtitle": "And How They Got That Way",
          "title": "The Smartest Kids In The World"
        },
        {
          "author": "David Finkel",
          "description": "Finkel tracks soldiers struggling to navigate postwar life, especially the psychologically wounded.",
          "price": "26",
          "publisher": "Sarah Crichton/Farrar, Straus & Giroux",
          "subtitle": null,
          "title": "Thank You For Your Service"
        },
        {
          "author": "Thomas Dyja",
          "description": "This robust cultural history weaves together the stories of the artists, styles and ideas that developed in Chicago before and after World War II.",
          "price": "29.95",
          "publisher": "Penguin Press",
          "subtitle": "When Chicago Built the American Dream",
          "title": "The Third Coast"
        },
        {
          "author": "Mark Leibovich",
          "description": "An entertaining and deeply troubling view of Washington.",
          "price": "27.95",
          "publisher": "Blue Rider",
          "subtitle": "Two Parties and a Funeral - Plus Plenty of Valet Parking! - in America's Gilded Capital",
          "title": "This Town"
        },
        {
          "author": "Lynne Olson",
          "description": "The savage political dispute between Roosevelt and the isolationist movement, presented in spellbinding detail.",
          "price": "30",
          "publisher": "Random House",
          "subtitle": "Roosevelt, Lindbergh, and America's Fight Over World War II, 1939-1941",
          "title": "Those Angry Days"
        },
        {
          "author": "Evgeny Morozov",
          "description": "Digital-age transparency may threaten the spirit of democracy, Morozov warns.",
          "price": "28.99",
          "publisher": "PublicAffairs",
          "subtitle": "The Folly of Technological Solutionism",
          "title": "To Save Everything, Click Here"
        },
        {
          "author": "Cris Beam",
          "description": "Beam's wrenching study is a triumph of narrative reporting and storytelling.",
          "price": "26",
          "publisher": "Houghton Mifflin Harcourt",
          "subtitle": "The Intimate Life of American Foster Care",
          "title": "To The End Of June"
        },
        {
          "author": "Kenneth M. Pollack",
          "description": "The Mideast expert makes the case for living with a nuclear Iran and trying to contain it.",
          "price": "30",
          "publisher": "Simon & Schuster",
          "subtitle": "Iran, the Bomb, and American Strategy",
          "title": "Unthinkable"
        },
        {
          "author": "George Packer",
          "description": "With a nod to John Dos Passos, Packer offers a gripping narrative survey of today's hard times; the 2013 National Book Award winner for nonfiction.",
          "price": "27",
          "publisher": "Farrar, Straus & Giroux",
          "subtitle": "An Inner History of the New America",
          "title": "The Unwinding"
        },
        {
          "author": "Margaret MacMillan",
          "description": "Why did the peace fail, a Canadian historian asks, and she offers superb portraits of the men who took Europe to war in the summer of 1914.",
          "price": "35",
          "publisher": "Random House",
          "subtitle": "The Road to 1914",
          "title": "The War That Ended Peace"
        },
        {
          "author": "Sonali Deraniyagala",
          "description": "Deraniyagala's unforgettable account of her struggle to carry on living after her husband, sons and parents were killed in the 2004 tsunami isn't only as unsparing as they come, but also defiantly imbued with light.",
          "price": "24",
          "publisher": "Knopf",
          "subtitle": null,
          "title": "Wave"
        },
        {
          "author": "Jon Mooallem",
          "description": "Mooallem explores the haphazard nature of our efforts to protect endangered species.",
          "price": "27.95",
          "publisher": "Penguin Press",
          "subtitle": "A Sometimes Dismaying, Weirdly Reassuring Story About Looking at People Looking at Animals in America",
          "title": "Wild Ones"
        },
        {
          "author": "Ian Buruma",
          "description": "This lively history shows how the Good War turned out badly for many people and splendidly for others less deserving.",
          "price": "29.95",
          "publisher": "Penguin Press",
          "subtitle": "A History of 1945",
          "title": "Year Zero"
        }
      ];
    }

    function getBookModels() {
      return _.map(getRawBookData(), function(d) { return new Book(d); });
    }

    var books = getBookModels();
    var index = new I.TextIndex({
      properties: [
        ["from.author()", "author"],
        ["from.publisher()", "publisher"],
        ["title.main()", "title"],
        ["title.sub()", "subtitle"],
        "description()",
        [function(o) { return o.price(); }, "price"]
      ]
    });
    index.indexObjects(books);

    it("single word works", function() {
      var tsa = I.TextSearch.parse("novel", true);
      expect(tsa.length).toEqual(1);
      expect(index.filter(tsa).length).toEqual(39);
      expect(index.filter("novel", true).length).toEqual(39);
    });

    it("multiple words works", function() {
      var tsa = I.TextSearch.parse("first novel", true);
      expect(tsa.length).toEqual(1);
      expect(index.filter(tsa).length).toEqual(4);
      expect(index.filter("first novel", true).length).toEqual(4);
    });

    it("identified search works", function() {
      var tsa = I.TextSearch.parse("subtitle:stories", true);
      expect(tsa.length).toEqual(1);
      expect(index.filter(tsa).length).toEqual(4);
      expect(index.filter("subtitle:stories", true).length).toEqual(4);
      expect(index.filter("sUbTiTlE:stories", true).length).toEqual(4);
    });

    it("multiple identified search works", function() {
      var tsa = I.TextSearch.parse("description=collection subtitle=stories", true);
      expect(tsa.length).toEqual(2);
      expect(index.filter(tsa).length).toEqual(2);
      expect(index.filter("description=collection subtitle=stories", true).length).toEqual(2);

      var tsa2 = I.TextSearch.parse("description:novel collection description=first", true);
      expect(tsa2.length).toEqual(2);
      expect(index.filter(tsa2).length).toEqual(5);
      expect(index.filter("description:novel collection description=first", true).length).toEqual(5);
    });
  });

  describe("Notifications", function() {
    xit("works");
  });

  describe("Ratio", function() {
    it("works", function() {
      var r0 = new I.Ratio(5, 8);
      var r1 = new I.Ratio(7, 15);
      var r3 = I.Ratio.copy(r0).straightAdd(r1);

      expect(r3.numerator).toEqual(12);
      expect(r3.denominator).toEqual(23);
      expect(r3.toString()).toEqual("12 / 23");
      expect(r3.toString("%2")).toEqual("52.17%");
      expect(r3.valueOf()).toBeCloseTo(0.5217, 4);
    });
  });

  describe("Table Scroll Manager", function() {
    xit("works");
  });

  describe("Table Sorter", function() {
    xit("works");
  });

  describe("View Model Base", function() {
    function MyClass(data, opts) {
      jsu.ViewModelBase.call(this, opts)
      var self = this;
    }
    jsu.util.inherit(jsu.ViewModelBase, MyClass);
    var mc = new MyClass({}, { dataAccess: { "endpoint": function() { return "hi!"; } } });
    it("works", function() {
      expect(mc.api("endpoint")).toEqual("hi!");
      expect(mc.private.root).toEqual(mc);
    });
  });
});
