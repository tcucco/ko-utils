describe("Knockout Utils", function() {
  "use strict";

  var I = jsu;
  var K = jsu.ko;

  describe("Ajax Status", function() {
    xit("works");
  });

  describe("General", function() {
    function Model(id, name, isSelected) {
      this.id = id;
      this.name = ko.observable(name);
      this.isSelected = ko.observable(isSelected);
      K.ToggleMethod(this);
      K.SetValueMethod(this);
      K.OppositeProperty(this, "notSelected", "isSelected");
    }

    it("Toggle Method", function() {
      var m = new Model(1, "Trey", true);
      expect(m.toggle).toBeDefined();
      expect(m.isSelected()).toBe(true);
      m.toggle("isSelected");
      expect(m.isSelected()).toBe(false);
      m.toggle("isSelected");
      expect(m.isSelected()).toBe(true);
    });

    it("Set Value Method", function() {
      var m = new Model(1, "Trey", true);
      expect(m.setValue).toBeDefined();
      expect(m.name()).toEqual("Trey");
      m.setValue("name", "Frank");
      expect(m.name()).toEqual("Frank");
      expect(m.isSelected()).toBe(true);
      m.setValue("isSelected", false);
      expect(m.isSelected()).toBe(false);
    });

    it("Opposite Property", function() {
      var m = new Model(1, "Trey", true);
      expect(m.notSelected).toBeDefined();
      expect(m.isSelected()).toBe(true);
      expect(m.isSelected()).not.toEqual(m.notSelected());
      m.toggle("isSelected");
      expect(m.isSelected()).toBe(false);
      expect(m.notSelected()).toBe(true);
    });

    it("getMappingPair", function() {
      var basis = { name: null, age: null, children: [] };
      var mappingPair = K.getMappingPair(basis);
      var fromJS = mappingPair.fromJS;
      var toJS = mappingPair.toJS;

      var orig = { name: "Julie", age: 30, color: "Blue", children: ["Asher", "Arwen"] };
      var kobj = fromJS(orig);
      expect(kobj.name.subscribe).toBeDefined();
      expect(kobj.age.subscribe).toBeDefined();
      expect(kobj.children.subscribe).toBeDefined();
      expect(kobj.children.push).toBeDefined();
      expect(kobj.color).not.toBeDefined();
      expect(toJS(kobj)).toEqual({ name: "Julie", age: 30, children: ["Asher", "Arwen"] });
      expect()
    });
  });

  describe("Serialized Property", function() {
    describe("Base", function() {
      var SP = K.SerializedProperty;

      it("underlying name works", function() {
        function M0() {
          SP(this, "name");
        }
        var m0 = new M0();
        expect(m0.name).toBeDefined();
        expect(m0._name).toBeDefined();

        function M1() {
          SP(this, "name", { underlyingName: "privateName" });
        }
        var m1 = new M1();
        expect(m1.name).toBeDefined();
        expect(m1._name).not.toBeDefined();
        expect(m1.privateName).toBeDefined();

        function M2() {
          SP(this, "name", { transformName: function(n) { return n + "_"; } });
        }
        var m2 = new M2();
        expect(m2.name).toBeDefined();
        expect(m2._name).not.toBeDefined();
        expect(m2.name_).toBeDefined();
      });

      it("(de)serialize works", function() {
        function M() {
          SP(this, "name", {
            serialize: function(n) { return "--" + n + "--"; },
            deserialize: function(n) { return n.slice(2, -2); }
          });
        }
        var m = new M();
        m.name("trey");
        expect(m.name()).toEqual("trey");
        expect(m._name()).toEqual("--trey--");
      });

      it("initial value works", function() {
        function M0(name) {
          SP(this, "name", {
            initialValue: name
          });
        }
        function M1() {
          SP(this, "name")
        }

        var m0 = new M0("hello");
        var m1 = new M1();
        expect(m0.name()).toEqual("hello");
        expect(m1.name()).toBe(null);
      });

      it("skip initial serialization works", function() {
        function M(name) {
          SP(this, "name", {
            serialize: function(n) { return "--" + n + "--"; },
            deserialize: function(n) { return n.slice(2, -2); },
            initialValue: name,
            skipInitialSerialization: true
          });
        }
        var m = new M("--trey--");
        expect(m._name()).toEqual("--trey--");
        expect(m.name()).toEqual("trey");
      });

      it("observers work", function() {
        var willChangeCalled = false;
        var didChangeCalled = false;
        function reset() { willChangeCalled = didChangeCalled = false; }

        function M() {
          SP(this, "age", {
            serialize: function(n) { return I.util.isPresent(n) ? n.toString() : null; },
            deserialize: function(n) { return parseInt(n); },
            willChange: function(nv, ov) {
              willChangeCalled = true;
              var n = parseInt(nv);
              if (isNaN(n) || n < 0 || n > 120) {
                return false;
              }
            },
            didChange: function(nv, ov) {
              didChangeCalled = true;
            }
          });
        }

        var values = [
          [30, true],
          ["50", true],
          [-1, false],
          [180, false],
          ["what the", false]
        ];
        var m = new M();

        _.each(values, function(v) {
          var nv = v[0];
          var didChange = v[1];
          reset();
          expect(willChangeCalled).toBe(false);
          expect(didChangeCalled).toBe(false);
          m.age(nv);
          expect(willChangeCalled).toBe(true);
          expect(didChangeCalled).toBe(didChange);
          if (didChange) {
            expect(m.age() == nv).toBe(true);
          }
        });
      });

      it("local storage works", function() {
        var lskey = "mIsTrue";
        function M() {
          SP(this, "isTrue", {
            localStorageKey: lskey,
            localStorageSerializer: function(v) { return v ? "true" : "false"; },
            localStorageDeserializer: function(v) { return v === "true"; },
            deserialize: function(v) { return v ? true : false; }
          });
        }

        I.util.validateLocalStorage(new Date().valueOf());
        var m = new M();
        expect(m.isTrue()).toBe(false);
        expect(I.util.localStorageRead(lskey)).toBe(undefined);
        m.isTrue(true);
        expect(I.util.localStorageRead(lskey)).toEqual("true");
        m.isTrue(false);
        expect(I.util.localStorageRead(lskey)).toEqual("false");
        m.isTrue(true);

        var m2 = new M();
        expect(m2.isTrue()).toBe(true);
      });
    });

    describe("Integer Property", function() {
      it("works", function() {
        var o = {};
        K.IntegerProperty(o, "value", { defaultValue: 0 });
        o.value(1);
        expect(o.value()).toEqual(1);
        o.value("1");
        expect(o.value()).toEqual(1);
        o.value(1.0);
        expect(o.value()).toEqual(1);
        o.value("abc");
        expect(o.value()).toEqual(0);
      });
    });

    describe("Float Property", function() {
      it("works", function() {
        var o = {};
        K.FloatProperty(o, "value", { defaultValue: 0.0 });
        o.value(2.5);
        expect(o.value()).toEqual(2.5);
        o.value("2.5");
        expect(o.value()).toEqual(2.5);
        o.value("abc");
        expect(o.value()).toEqual(0.0);
      });
    });

    describe("Integer Array Property", function() {
      it("works", function() {
        var o = {};
        K.IntegerArrayProperty(o, "values");
        K.IntegerArrayProperty(o, "values2", { sort: true });
        o.values(["1", "2", 3, 4.0, "5.0"]);
        expect(I.util.compareArrays(o.values(), [1, 2, 3, 4, 5])).toBe(true);
        o.values2([5, 4, "3", "2.0", 1.0]);
        expect(I.util.compareArrays(o.values2(), [1, 2, 3, 4, 5])).toBe(true);
      });
    });

    describe("Rounded Float Property", function() {
      it("works", function() {
        var o = {};
        K.RoundedFloatProperty(o, "value", { scale: 1 });
        o.value(2.5);
        expect(o.value()).toEqual("2.5");
        o.value(2.56);
        expect(o.value()).toEqual("2.6");
      });
    });

    describe("Stepped Float Property", function() {
      it("works", function() {
        var o = {};
        K.SteppedFloatProperty(o, "value", { step: 0.25, scale: 2, stripTrailingZeros: true });
        o.value(2.4);
        expect(o.value()).toEqual("2.5");
        o.value(2.3);
        expect(o.value()).toEqual("2.25");
      });
    });

    describe("Time Property", function() {
      xit("works");
    });
  });

  describe("Status Message", function() {
    var timeout = 2500;
    function Model() {
      K.StatusMessage(this, K.StatusMessage.all, K.StatusMessage.success, timeout)
    };
    var props = ["infoMessage", "warningMessage", "successMessage", "errorMessage"];
    var fxns = ["clearInfoMessage", "clearWarningMessage", "clearSuccessMessage", "clearErrorMessage"];

    beforeEach(function() {
      jasmine.clock().install();
    });

    afterEach(function() {
      jasmine.clock().uninstall();
    });

    it("creates properties and functions", function() {
      var m = new Model();
      expect(m.setMessage).toBeDefined();
      expect(m.clearMessage).toBeDefined();
      expect(m.statusMessageType).toBeDefined();
      expect(m.statusMessage).toBeDefined();
    });

    it("timeout works", function() {
      var m = new Model();
      var msg = "Hello there!";
      m.setMessage("Success", msg);
      expect(m.statusMessage()).toEqual(msg);
      setTimeout(timeout + 500, function() {
        expect(m.statusMessage()).toBe(null);
      });
    });
  });

  describe("Track Changes", function() {
    function Model(id, name, children) {
      this.id = ko.observable(null);
      this.name = ko.observable(name);
      this.children = ko.observableArray(children || []);
    }
    Model.prototype.saved = function(data) {
      for (var k in data) {
        this[k](data[k]);
      }
    }

    function getModel(name, children) {
      var m = new Model(1, name, children);
      K.TrackChanges(m, ["name", "children"]);
      return m;
    }

    it("creates appropriate methods and properties", function() {
      var m = getModel("Trey", ["Asher", "Arwen"]);
      expect(m.changedPropertyCount).toBeDefined();
      expect(m.hasChanges).toBeDefined();
      expect(m.changedPropertyCount()).toEqual(0);
      expect(m.revertChanges).toBeDefined();
      expect(m.saveChanges).toBeDefined();
      expect(m.hasChanges()).toBe(false);
      expect(m.name_changed).toBeDefined();
      expect(m.name_changed()).toBe(false);
      expect(m.children_changed()).toBeDefined();
      expect(m.children_changed()).toBe(false);
    });

    it("works for scalars", function() {
      var m = getModel("Trey", ["Asher", "Arwen"]);

      m.name("Frank the 3rd");
      expect(m.changedPropertyCount()).toEqual(1);
      expect(m.hasChanges()).toBe(true);
      expect(m.name_changed()).toBe(true);

      m.name("Trey");
      expect(m.changedPropertyCount()).toEqual(0);
      expect(m.hasChanges()).toBe(false);
      expect(m.name_changed()).toBe(false);
    });

    it("works for arrays", function() {
      var m = getModel("Trey", ["Asher", "Arwen"]);

      m.children.push("Thing 1");
      expect(m.changedPropertyCount()).toEqual(1);
      expect(m.hasChanges()).toBe(true);
      expect(m.children_changed()).toBe(true);
      m.children.pop();
      expect(m.changedPropertyCount()).toEqual(0);
      expect(m.hasChanges()).toBe(false);
      expect(m.children_changed()).toBe(false);
    });

    it("revert works", function() {
      var m = getModel("Trey", ["Asher", "Arwen"]);

      m.name("Frank the 3rd");
      m.children.push("Thing 2");
      expect(m.hasChanges()).toBe(true);
      expect(m.changedPropertyCount()).toEqual(2);
      m.revertChanges();
      expect(m.hasChanges()).toBe(false);
      expect(m.name()).toEqual("Trey");
      expect(I.util.compareArrays(m.children(), ["Asher", "Arwen"])).toBe(true);
    });

    it("save changes works", function() {
      var m1 = getModel("Trey", ["Asher", "Arwen"]);

      m1.name("Frank the 3rd");
      expect(m1.hasChanges()).toBe(true);
      m1.saveChanges({ id: 1 });
      expect(m1.id()).toEqual(1);
      expect(m1.hasChanges()).toBe(false);
    });

    it("add/remove property works", function() {
      var m1 = getModel("Trey", ["Asher", "Arwen"]);
      m1.age = ko.observable(32);

      m1.addChangeTracking("age");
      expect(m1.age_changed).toBeDefined();
      expect(m1.age_changed()).toBe(false);
      expect(m1.hasChanges()).toBe(false);

      m1.age(33);
      expect(m1.age_changed()).toBe(true);
      expect(m1.hasChanges()).toBe(true);

      m1.revertChanges();
      expect(m1.hasChanges()).toBe(false);
      expect(m1.age()).toEqual(32);

      m1.age(34);
      expect(m1.hasChanges()).toBe(true);
      m1.removeChangeTracking("age");
      expect(m1.age_changed).not.toBeDefined();
      expect(m1.hasChanges()).toBe(false);
    });
  });

  describe("Expandable", function() {
    it("works as expected", function() {
      var m1 = new jsu.ko.Expandable({ name: "Trey", age: 32 });

      expect(m1.name).toBeDefined();
      expect(m1.age).toBeDefined();
      expect(m1.name_changed).toBeDefined();
      expect(m1.age_changed).toBeDefined();

      m1.addAttribute("last_name");
      expect(m1.last_name).toBeDefined();
      expect(m1.last_name_changed).toBeDefined();

      expect(m1.hasChanges()).toBe(false);
      m1.last_name("Cucco");
      expect(m1.hasChanges()).toBe(true);

      expect(m1.toJS()).toEqual({
        name: "Trey",
        age: 32,
        last_name: "Cucco"
      });

      m1.removeAttribute("last_name");
      expect(m1.last_name).not.toBeDefined();
      expect(m1.last_name_changed).not.toBeDefined();
      expect(m1.hasChanges()).toBe(false);
    });
  });

  describe("Track Errors", function() {
    function Model(id, firstName, lastName, age) {
      this.id = ko.observable(id);
      this.firstName = ko.observable(firstName);
      this.lastName = ko.observable(lastName);
      this.age = ko.observable(age);
    }

    function getModel(firstName, lastName, age, multiple) {
      var m = new Model(1, firstName, lastName, age);
      K.TrackErrors(m, ["firstName", "lastName", "age"], null, multiple);
      return m;
    }

    it("creates appropriate methods and properties", function() {
      var m = getModel("Trey", "Cucco", 31);
      expect(m.errorPropertyCount).toBeDefined();
      expect(m.hasErrors).toBeDefined();
      expect(m.clearErrors).toBeDefined();
      expect(m.addError).toBeDefined();
      expect(m.mapErrors).toBeDefined();
      expect(m.listErrors).toBeDefined();

      expect(m.errorPropertyCount()).toEqual(0);
      expect(m.hasErrors()).toBe(false);
      expect(I.util.compareArrays(m.listErrors(), [])).toBe(true);
    });

    it("tracks singleton errors", function() {
      var m = getModel(null, "Cucco", 31);
      m.addError("firstName", "Can't be blank");
      expect(m.hasErrors()).toBe(true);
      expect(m.errorPropertyCount()).toEqual(1);
      expect(m.firstName_error()).toEqual("Can't be blank");
      var errorList = m.listErrors();
      expect(errorList.length).toEqual(1);
      expect(I.util.compareObjects(errorList[0], { propertyName: "firstName", errorMessage: "Can't be blank" })).toBe(true);
    });

    it("tracks multiple errors", function() {
      var m = getModel(null, "Cucco", 31, true);
      m.addError("firstName", "Can't be blank");
      m.addError("firstName", "Must be at least 2 characters long");
      expect(m.hasErrors()).toBe(true);
      expect(m.errorPropertyCount()).toEqual(1);
      expect(I.util.compareArrays(m.firstName_error(), ["Can't be blank", "Must be at least 2 characters long"])).toBe(true);
      var errorList = m.listErrors();
      expect(errorList.length).toEqual(2);
      var e0 = errorList[0];
      var e1 = errorList[1];
      expect(I.util.compareObjects(e0, { propertyName: "firstName", errorMessage: "Can't be blank" })).toBe(true);
      expect(I.util.compareObjects(e1, { propertyName: "firstName", errorMessage: "Must be at least 2 characters long" })).toBe(true);
    });

    it("clears errors", function() {
      var m = getModel("Trey", "Cucco", 31);
      m.addError("firstName", "No reaseon");
      expect(m.hasErrors()).toBe(true);
      m.clearErrors();
      expect(m.hasErrors()).toBe(false);
      expect(m.firstName_error()).toBe(null);
      m.addError("firstName", "No reason");
      m.addError("age", "Again, no reason");
      expect(m.hasErrors()).toBe(true);
      m.clearErrors("firstName");
      expect(m.hasErrors()).toBe(true);
      expect(m.firstName_error()).toBe(null);
      m.clearErrors("age");
      expect(m.hasErrors()).toBe(false);
    });

    it("maps errors", function() {
      var m1 = getModel("Trey", "Cucco", 31, false);
      var m2 = getModel("Trey", "Cucco", 31, true);
      var errorObject = {
        "firstName": ["Can't be blank", "Must have characters"],
        "age": ["Can't be negative"]
      };

      m1.mapErrors(errorObject);
      expect(m1.hasErrors()).toBe(true);
      expect(m1.errorPropertyCount()).toEqual(2);
      expect(m1.firstName_error()).toEqual("Must have characters");
      expect(m1.age_error()).toEqual("Can't be negative");

      m2.mapErrors(errorObject);
      expect(m2.hasErrors()).toBe(true);
      expect(m2.errorPropertyCount()).toEqual(2);
      expect(I.util.compareArrays(m2.firstName_error(), errorObject.firstName)).toBe(true);
    });
  });
});
