console.clear();

describe("General Utilities", function() {
  "use strict";

  var I = jsu;
  var U = I.util;

  function Parent(id, lastName, firstName) {
    this.id = id;
    this.firstName = firstName;
    this.lastName = lastName;
    this.children = [];
  }
  Parent.prototype.copy = function() {
    return new Parent(this.id, this.lastName, this.firstName);
  }

  function Child(id, parentId, lastName, firstName) {
    this.id = ko.observable(id);
    this.parentId = ko.observable(parentId);
    this.firstName = ko.observable(firstName);
    this.lastName = ko.observable(lastName);
    this.parent = ko.observable(null);
    this.siblings = ko.observableArray([]);
  }
  Child.prototype.copy = function() {
    return new Child(this.id(), this.parentId(), this.lastName(), this.firstName());
  }

  var parents = [
    new Parent(1, "Anderson", "Antoinetta"),
    new Parent(3, "Baker", "Wayna"),
    new Parent(5, "Bishop", "Angelyca"),
    new Parent(7, "Bryan", "Jamayka"),
    new Parent(9, "Chase", "Carlasia"),
    new Parent(10, "Cooper", "Jisele"),
    new Parent(8, "Carlson", "Montiera"),
    new Parent(6, "Brown", "Teaghan"),
    new Parent(4, "Bennett", "Wendella"),
    new Parent(2, "Armstrong", "Delilia")
  ];
  var children = [
    new Child(1, 1, "Cox", "Hawanatu"),
    new Child(3, 2, "Dickinson", "Innaya"),
    new Child(5, 3, "Frost", "Leeani"),
    new Child(7, 4, "Hartman", "Guin"),
    new Child(9, 5, "Henry", "Quiency"),
    new Child(11, 6, "Jones", "Nicolus"),
    new Child(13, 7, "Phillips", "Yuan"),
    new Child(15, 9, "Randolph", "Tyheim"),
    new Child(14, 8, "Powell", "Sothea"),
    new Child(12, 6, "Morgan", "Joeanthony"),
    new Child(16, 9, "Ruis", "Bankston"),
    new Child(10, 6, "Hughes", "Teng"),
    new Child(17, 10, "Smith", "Saimon"),
    new Child(8, 4, "Henderson", "Dorren"),
    new Child(18, 10, "Spencer", "Nyier"),
    new Child(6, 3, "Harrison", "Orvall"),
    new Child(19, 10, "Walton", "Daler"),
    new Child(4, 3, "Freeman", "Gwenaelle"),
    new Child(20, 10, "Warner", "Nishal"),
    new Child(2, 2, "Day", "Krischelle")
  ];

  describe("Array Functions", function() {
    var U = jsu.util;

    it("Array insert", function() {
      function test(in_, item, out, idx, pivot, where) {
        expect(U.arrayInsert(in_, item, pivot, where)).toEqual(idx);
        expect(in_).toEqual(out);
      }

      // Empty array
      test([], 9, [9], 0);
      test([], 9, [9], 0, null, "before");
      test([], 9, [9], 0, null, "after");

      // One item
      test([1], 9, [9, 1], 0, 1);
      test([1], 9, [1, 9], 1, 1, "after");

      // Multiple items
      test([1, 2, 3], 9, [9, 1, 2, 3], 0, 1);
      test([1, 2, 3], 9, [1, 9, 2, 3], 1, 1, "after");
      test([1, 2, 3], 9, [1, 9, 2, 3], 1, 2);
      test([1, 2, 3], 9, [1, 2, 9, 3], 2, 2, "after");
      test([1, 2, 3], 9, [1, 2, 9, 3], 2, 3);
      test([1, 2, 3], 9, [1, 2, 3, 9], 3, 3, "after");

      // Missing item
      test([1, 2], 9, [9, 1, 2], 0, 3);
      test([1, 2], 9, [1, 2, 9], 2, 3, "after");

      // Removing duplicate
      test([1, 9], 9, [9, 1], 0, 1, "before");

      // Observable arrays work correctly.
      var oa = ko.observableArray([1, 2, 3]);
      expect(U.arrayInsert(oa, 9, 1, "after")).toEqual(1);
      expect(oa()).toEqual([1, 9, 2, 3]);
      expect(U.arrayInsert(oa, 9, 3, "after")).toEqual(3);
      expect(oa()).toEqual([1, 2, 3, 9]);
    });

    it("Array remove", function() {
      function test(in_, item, out, idx) {
        expect(U.arrayRemove(in_, item)).toEqual(idx);
        expect(in_).toEqual(out);
      }

      test([], 9, [], -1);
      test([9], 9, [], 0);
      test([9, 1], 9, [1], 0);
      test([1, 9], 9, [1], 1);
      test([1, 9, 2], 9, [1, 2], 1);
    });

    it("Array extend", function() {
      expect(U.arrayExtend([1, 2], [3, 4, 5])).toEqual([1, 2, 3, 4, 5]);
      expect(U.arrayExtend([1, 2], [3, 4, 5], 1)).toEqual([1, 3, 4, 5, 2]);
    });

    it("Get New Ordinal", function() {
      function ni(o) { return { ordinal: o }; };
      function go(o) { return o.ordinal; }

      var a = _.map(_.range(10), ni);

      expect(U.getNewOrdinal([], 0, go)).toEqual(0);
      expect(U.getNewOrdinal(a, 0, go)).toEqual(-1);
      expect(U.getNewOrdinal(a, 10, go)).toEqual(10);
      expect(U.getNewOrdinal(a, 9, go)).toEqual(8.5);
      expect(U.getNewOrdinal(a, 1, go)).toEqual(0.5);
    });
  });

  describe("Comparison Builder", function() {
    it("Multi sort ascending", function() {
      var cmp = U.getComparer({ field: ["lastName", "firstName"] });
      var lparents = parents.slice();
      lparents.sort(cmp);
      for (var i = 1; i < lparents.length; ++i) {
        var cp = lparents[i];
        var pp = lparents[i - 1];
        expect(pp.lastName < cp.lastName || pp.lastName == cp.lastName && pp.firstName <= cp.firstName).toBe(true);
      }
    });

    it("Multi sort different directions", function() {
      var cmp = U.getComparer({ field: ["-lastName", "firstName"] });
      var lparents = parents.slice();
      lparents.sort(cmp);
      for (var i = 1; i < lparents.length; ++i) {
        var cp = lparents[i];
        var pp = lparents[i - 1];
        expect(pp.lastName > cp.lastName || pp.lastName == cp.lastName && pp.firstName <= cp.firstName).toBe(true);
      }
    });

    it("Sort on functions", function() {
      var cmp = U.getComparer("lastName()");
      var lchildren = children.slice();
      lchildren.sort(cmp);
      for (var i = 1; i < lchildren.length; ++i) {
        var cc = lchildren[i];
        var pc = lchildren[i - 1];
        expect(pc.lastName() <= cc.lastName()).toBe(true);
      }
    });
  });

  describe("Delegate", function() {
    it("works", function() {
      var v = false;
      var a = {
        fx: function() { v = true; }
      };
      var b = {};
      U.delegate(b, a, "fx");

      expect(b.fx).toBeDefined();
      b.fx();
      expect(v).toBe(true);
    });
  });

  describe("Deserializing", function() {
    it("works", function() {
      var csvData = {
        keys: ["id", "parentId", "lastName", "firstName"],
        values: _.map(children, function(c) { return [c.id(), c.parentId(), c.lastName(), c.firstName()]; })
      };
      var objs = U.deserialize(csvData.keys, csvData.values);
      var i = 10;
      var c = children[i];

      expect(objs.length).toEqual(children.length);
      expect(objs[i]).toEqual({ id: c.id(), parentId: c.parentId(), firstName: c.firstName(), lastName: c.lastName() });
    });
  });

  describe("Drag Toggle", function() {
    xit("works");
  });

  describe("Event Registration", function() {
    xit("works");
  });

  describe("General", function() {
    describe("trimString", function() {
      it("Left trims strings", function() { expect(U.trimString("  abc def")).toEqual("abc def"); });
      it("Right trims strings", function() { expect(U.trimString("abc def  ")).toEqual("abc def"); });
      it("Full trims strings", function() { expect(U.trimString("  abc def  ")).toEqual("abc def"); });
    });

    describe("parseQueryString", function() {
      var qs = "name=Trey&age=30&children=Asher&children=Arwen&spouse=Julie";
      it("Parses properly", function() {
        expect(U.parseQueryString(qs)).toEqual({
          name: "Trey",
          age: "30",
          children: ["Asher", "Arwen"],
          spouse: "Julie"
        });
      });
    });

    describe("parseCookies", function() {
      xit("works");
    });

    describe("setCookie", function() {
      xit("works");
    });

    describe("deleteCookie", function() {
      xit("works");
    });

    describe("extractXhrError", function() {
      var _xhr = {
        status: 400,
        contentType: "application/json",
        getResponseHeader: function(hn) {
          if (hn == "content-type") {
            return this.contentType;
          } else {
            throw "Unknown header: " + hn;
          }
        },
        responseText: "This is the response text",
        responseJSON: { message: "This is the JSON error message" }
      }

      it("Gives error response when unable to contact server", function() {
        var xhr = _.extend({}, _xhr);
        xhr.status = 0;
        expect(U.extractXhrError(xhr, 0, null)).toMatch(/unable to contact the server/i);
      });

      it("Gets JSON message property", function() {
        var xhr = _.extend({}, _xhr);
        xhr.status = 400;
        expect(U.extractXhrError(xhr, 400, "Bad request")).toEqual(xhr.responseJSON.message);
      });

      it("Gets error message when no JSON message", function() {
        var xhr = _.extend({}, _xhr);
        xhr.responseJSON = { };
        expect(U.extractXhrError(xhr, 400, "Bad request")).toEqual("Bad request");
      });

      it("Gets error message when html content type", function() {
        var xhr = _.extend({}, _xhr);
        xhr.contentType = "text/html";
        expect(U.extractXhrError(xhr, 400, "Bad request")).toEqual("Bad request");
      });

      it("Gets response text for plain text", function() {
        var xhr = _.extend({}, _xhr);
        xhr.contentType = "text/plain";
        expect(U.extractXhrError(xhr, 400, "Bad request")).toEqual(xhr.responseText);
      });
    });

    describe("inherit", function() {
      function A() { };
      A.prototype.sayHi = function() { };
      function B() { };

      it("Inheritance works", function() {
        U.inherit(A, B);
        var b = new B();
        expect(b.sayHi).toBeDefined();
      })
    });

    describe("makeBroadcaster", function() {
      it("Should create methods", function() {
        var o = {};
        U.makeBroadcaster(o);
        expect(o.on).toBeDefined();
        expect(o.off).toBeDefined();
        expect(o.trigger).toBeDefined();
      });

      it("Should work", function() {
        var o = {};
        U.makeBroadcaster(o);
        var triggered = 0;
        var sup = o.on("increment", function() { triggered += 1; });
        o.trigger("increment");
        expect(triggered).toEqual(1);
        o.off(sup);
        o.trigger("increment");
        expect(triggered).toEqual(1);
      });
    });

    describe("getNS", function() {
      var r = { child: {} };

      it("Should fetch existing", function() {
        expect(U.getNS(r, "child")).toBe(r.child);
      });

      it("Should create missing", function() {
        U.getNS(r, "child", "grandchild", "greatgrandchild");
        expect(r.child.grandchild).toBeDefined();
        expect(r.child.grandchild.greatgrandchild).toBeDefined();
      });
    });

    describe("getProperty", function() {
      var a = [1, 2, 3];
      var o = {
        p0: {
          q0: {
            r: function() { return { a: a }; }
          }
        },
        p1: [4, 5, 6]
      };

      it("Should get simple properties", function() {
        expect(U.getProperty(o, "p0.q0")).toBe(o.p0.q0);
      });

      it("Should get function properties", function() {
        expect(U.getProperty(o, "p0.q0.r().a")).toBe(a);
      });
    });

    describe("isBlank", function() {
      var blanks = [null, "", "   "];
      var notBlanks = ["a", " a", "a "];

      _.each(blanks, function(v) {
        it((_.isString(v) ? ("'" + v + "'") : v) + " should be blank", function() { expect(U.isBlank(v)).toBe(true); });
      });
      _.each(notBlanks, function(v) {
        it((_.isString(v) ? ("'" + v + "'") : v) + " should not be blank", function() { expect(U.isBlank(v)).toBe(false); });
      });
    });

    describe("isPresent", function() {
      var notPresent = [null, undefined];
      var present = ["", false, {}, []];

      _.each(notPresent, function(v) {
        it((_.isString(v) ? ("'" + v + "'") : v) + " should not be present", function() { expect(U.isPresent(v)).toBe(false); });
      });
      _.each(present, function(v) {
        it((_.isString(v) ? ("'" + v + "'") : v) + " should be present", function() { expect(U.isPresent(v)).toBe(true); });
      });
    });

    describe("join", function() {
      function a(cb) { cb(); }
      function b(cb) { cb(); }

      it("works", function() {
        var triggered = false;
        expect(triggered).toBe(false);
        U.join([a, b], function() { triggered = true; });
        expect(triggered).toBe(true);
      })
    });

    describe("queue", function() {
      beforeEach(function() {
        jasmine.clock().install();
      });

      afterEach(function() {
        jasmine.clock().uninstall();
      });

      it("works", function() {
        var triggered = false;
        expect(triggered).toBe(false);
        U.queue(function() { triggered = true; });
        U.queue(function() { expect(triggered).toBe(true); });
      });
    });

    describe("localStorage", function() {
      it("works", function() {
        var d = new Date();
        var s = d.toString();
        var n = d.valueOf();
        U.localStorageWrite("key", s);
        expect(U.localStorageRead("key")).toEqual(s);
        U.validateLocalStorage(n);
        expect(U.localStorageRead("key")).not.toBeDefined();
      });
    });

    describe("compareArrays", function() {
      it("self comparison works", function() {
        var a = [1, 2, 3];
        expect(U.compareArrays(a, a)).toBe(true);
      });

      it("different comparison works", function() {
        var a = [1, 2, 3];
        var b = [1, 2, 3];
        var c = [3, 2, 1];
        expect(U.compareArrays(a, b)).toBe(true);
        expect(U.compareArrays(b, a)).toBe(true);
        expect(U.compareArrays(a, c)).not.toBe(true);
        expect(U.compareArrays(c, a)).not.toBe(true);
      });

      it("strict works", function() {
        var a = [1, 2, 3];
        var b = ["1", "2", "3"];
        expect(U.compareArrays(a, b, true)).toBe(false);
        expect(U.compareArrays(a, b, false)).toBe(true);
        expect(U.compareArrays(a, b)).toBe(true);
      });

      it("order idependent works", function() {
        var a = [1, 2, 3];
        var b = [3, 1, 2];
        expect(U.compareArrays(a, b, true)).toBe(false);
        expect(U.compareArrays(a, b, true, true)).toBe(true);
      });
    });

    describe("compareObjects", function() {
      it("self comparison works", function() {
        var o = { a: 1, b: 2 };
        expect(U.compareObjects(o, o)).toBe(true);
      });

      it("different comaprison works", function() {
        var o1 = { a: 1, b: 2 };
        var o2 = { b: 2, a: 1 };
        var o3 = { a: 1, c: 3 };
        var o4 = { a: 1, b: 2, c: 3 };

        expect(U.compareObjects(o1, o2)).toBe(true);
        expect(U.compareObjects(o2, o1)).toBe(true);
        expect(U.compareObjects(o1, o3)).not.toBe(true);
        expect(U.compareObjects(o1, o4)).not.toBe(true);
      });

      it("sub objects works", function() {
        var o1 = { a: { b: 1, c: 2 }, d: 3 };
        var o2 = { d: 3, a: { b: 1, c: 2 } };
        var o3 = { a: { b: 2, c: 2 }, d: 3 };
        expect(U.compareObjects(o1, o2)).toBe(true);
        expect(U.compareObjects(o2, o1)).toBe(true);
        expect(U.compareObjects(o1, o3)).not.toBe(true);
      });

      it("sub arrays works", function() {
        var o1 = { a: 1, b: [1, 2, 3] };
        var o2 = { a: 1, b: [1, 2, 3] };
        var o3 = { a: 1, b: [3, 2, 1] };
        expect(U.compareObjects(o1, o2)).toBe(true);
        expect(U.compareObjects(o2, o1)).toBe(true);
        expect(U.compareObjects(o1, o3)).not.toBe(true);
        expect(U.compareObjects(o1, o3, false, true)).toBe(true);
      });

      it("strict works", function() {
        var o1 = { a: 1, b: "abc" };
        var o2 = { a: "1", b: "abc" };
        var o3 = { a: 1.0, b: "abc" };
        expect(U.compareObjects(o1, o2)).toBe(true);
        expect(U.compareObjects(o2, o1)).toBe(true);
        expect(U.compareObjects(o1, o3)).toBe(true);
        expect(U.compareObjects(o1, o2, true)).not.toBe(true);
      });

      it("works on native values", function() {
        expect(U.compareObjects(1, 1)).toBe(true);
        expect(U.compareObjects(1, 1.0)).toBe(true);
        expect(U.compareObjects(1, "1")).toBe(true);
        expect(U.compareObjects(1, 1.0, true)).toBe(true);
        expect(U.compareObjects(1, "1", true)).toBe(false);
        expect(U.compareObjects("Trey", "Trey")).toBe(true);
        expect(U.compareObjects([1, 2, 3], [1, 2, 3])).toBe(true);
        expect(U.compareObjects([1, 2, "3"], [3, "2", 1], false, true)).toBe(true);
        expect(U.compareObjects(null, null)).toBe(true);
        expect(U.compareObjects(undefined, undefined)).toBe(true);
      });
    });

    describe("abbreviate", function() {
      xit("works");
    });
  });

  describe("Indexing and Joining", function() {
    it("indexing works", function() {
      var index = U.indexObjects(children, "id()");
      _.each(children, function(c) {
        expect(c).toBe(index[c.id()]);
      });
    });

    it("selecting works", function() {
      var objs = [
        { id: 1, name: "One" },
        { id: 2, name: "Two" },
        { id: 3, name: "Three" }
      ];
      var selection = U.selectObjects(objs, [1, 3], "id");
      var cmp = objs.slice();
      cmp.splice(1, 1);
      expect(selection).toEqual(cmp);
    });

    it("joining works", function() {
      var lparents = _.map(parents, function(p) { return p.copy(); });
      var lchildren = _.map(children, function(c) { return c.copy(); });

      U.joinObjects({
        parents: lparents,
        children: lchildren,
        getIdOfParent: "id",
        getParentId: "parentId()",
        assignChild: "children[]",
        assignParent: "parent()="
      });

      U.joinObjects({
        parents: lchildren,
        children: lchildren,
        getIdOfParent: "parentId()",
        getParentId: "parentId()",
        assignChild: "siblings()[]",
        assignParent: "siblings()[]"
      });

      _.each(lparents, function(p) {
        expect(p.children.length).not.toEqual(0);
        _.each(p.children, function(c) {
          expect(c.parentId()).toEqual(p.id);
          expect(c.parent()).toBe(p);
        });
      });

      _.each(lchildren, function(c) {
        _.each(c.siblings(), function(s) {
          expect(s.parentId()).toEqual(c.parentId());
        });
      });
    });

    it("join through works", function() {
      var lparents = _.map(parents, function(p) { return p.copy(); });
      var lchildren = _.map(children, function(c) { return c.copy(); });
      var joiners = _.map(children, function(c) { return { parentId: c.parentId(), childId: c.id() }; });

      U.joinObjectsThrough({
        parents: lparents,
        children: lchildren,
        joiners: joiners,
        getIdOfParent: "id",
        getIdOfChild: "id()",
        getParentId: "parentId",
        getChildId: "childId",
        assignChild: "children[]",
        assignParent: "parent()="
      });

      _.each(lparents, function(p) {
        expect(p.children.length).not.toEqual(0);
        _.each(p.children, function(c) {
          expect(c.parentId()).toEqual(p.id);
          expect(c.parent()).toBe(p);
        });
      });
    });
  });

  describe("Random ID", function() {
    it("random id works", function() {
      for (var i = 0; i < 100; ++i) {
        var len = 1 + Math.ceil(Math.random() * 20);
        var base = 1 + Math.ceil(Math.random() * 63);
        var rid = U.getRandomId(len, base);
        expect(rid.length).toEqual(len);
        var re = new RegExp("^[" + U.getRegexSafeText(U.getRandomIdSpace(base)) + "]+$");
        expect(re.test(rid)).toBe(true);
      }
    });

    var uuidRe = /^[a-z0-9]{8}(-[a-z0-9]{4}){3}-[a-z0-9]{12}$/i;
    var randRe1 = /^.{14}4.{3}-[89ab].{16}$/i;

    it ("uuid works", function() {
      for (var i = 0; i < 100; ++i) {
        var uuid = U.getRandomUUID();
        expect(uuidRe.test(uuid)).toBe(true);
        expect(randRe1.test(uuid)).toBe(true);
      }
    });
  });

  describe("Regex Builder", function() {
    var res = U.getRegexReservedChars();
    var re = new RegExp("^[" + U.getRegexSafeText(res) + "]+$");

    it("works", function() {
      expect(re.test(res)).toBe(true);
    });
  });

  describe("Viewport Observing", function() {
    xit("works");
  });
});
