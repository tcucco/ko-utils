/*

jsu version 2.1.0
Copyright (c) 2015, Trey Cucco
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.

    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.

    * Neither the name of the project nor the names of its contributors may
      be used to endorse or promote products derived from this software
      without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

// Set up namespaces.

var jsu = {};
jsu.DEBUG = false;
// DEPENDENCY: underscore
// DEPENDENCY: knockout

(function(ns) {
  function BatchEditor(args) {
    args = _.extend({}, batchEditorDefaultArgs, args);

    var self = this;
    var selectedItemsMap = {};
    var batchProperties = {};

    function updateInclusion(item, list, e) {
      if (!item) {
        clearItems();
      } else {
        if (e) {
          if (e.shiftKey) {
            if (self.selectedItem()) {
              var items = itemsInRange(list, self.selectedItem(), item);
              for (var i = 0; i < items.length; ++i) {
                addItem(items[i]);
              }
            } else {
              resetWith(item);
            }
          } else if (e.ctrlKey || e.metaKey) {
            if (isSelected(item)) {
              removeItem(item);
            } else {
              addItem(item);
            }
          } else {
            resetWith(item);
          }
        } else {
          resetWith(item);
        }
      }
      updatePublicMap();
    }

    function removeItem(item) {
      var i = self.selectedItems.remove(item);
      var id = args.getId(item);
      delete selectedItemsMap[id];
    }

    function resetWith(item) {
      clearItems();
      addItem(item);
    }

    function addItem(item) {
      if (!isSelected(item)) {
        selectedItemsMap[args.getId(item)] = true;
        self.selectedItems.unshift(item);
        updatePropertiesWithItem(item);
      }
    }

    function isSelected(item) {
      return args.getId(item) in selectedItemsMap;
    }

    function clearItems() {
      self.selectedItems([]);
      selectedItemsMap = {};
      resetItemProperties();
    }

    function resetItemProperties() {
      for (var i = 0; i < args.trackProperties.length; ++i) {
        var f = args.trackProperties[i];
        var v;

        if (f.type == "array") {
          v = [];
        } else {
          v = null;
        }

        if (!batchProperties[f.id]) {
          batchProperties[f.id] = new BatchEditProperty(v);
        } else {
          batchProperties[f.id].reset();
        }
      }

      for (var i = 0; i < self.selectedItems().length; ++i) {
        var item = self.selectedItems()[i];
        updatePropertiesWithItem(item);
      }
    }

    function updatePropertiesWithItem(item) {
      for (var i = 0; i < args.trackProperties.length; ++i) {
        var property = args.trackProperties[i];
        updatePropertyWithItem(property, item);
      }
    }

    function updatePropertyWithItem(property, item) {
      var reset = self.selectedItems().length == 1;

      if (property.type == "array") {
        var include = _.map(property.getValue(item), property.getId);
        if (reset) {
          batchProperties[property.id].value(include);
        } else {
          batchProperties[property.id].value(_.intersection(batchProperties[property.id].value(), include));
        }
      } else {
        var v = property.getValue(item);
        if (!batchProperties[property.id].value() == v) {
          batchProperties[property.id].reset(false);
        }
      }
    }

    function updatePublicMap() {
      self.selectedItemsMap(selectedItemsMap);
    }

    self.selectedItems = ko.observableArray([]);
    self.selectedItem = ko.computed(function() {
      switch (self.selectedItems().lengt) {
        case 0:
          return null;
        case 1:
          return self.selectedItems()[0];
        default:
          return args.singleSelectionOnMultiple ? self.selectedItems()[0] : null;
      }
    });
    self.hasSelection = ko.computed(function() { return self.selectedItems().length; });
    self.isSingle = ko.computed(function() { return self.selectedItems().length == 1; });
    self.selectedItemsMap = ko.observable({});

    self.updateInclusion = updateInclusion;
    self.removeItem = function(i) {
      removeItem(i);
      updatePublicMap();
    }
    self.resetWith = function(i) {
      resetWith(i);
      updatePublicMap();
    }
    self.addItem = function(i) {
      addItem(i);
      updatePublicMap();
    }
    self.isSelected = isSelected;
    self.clear = function() {
      clearItems();
      updatePublicMap();
    }
    self.batchProperties = function() { return batchProperties; };
  }

  var batchEditorDefaultArgs = {
    getId: function(v) { return v; },
    singleSelectionOnMultiple: true,
    trackProperties: []
  }

  function BatchEditProperty(value, defaultValue) {
    var self = this;

    self.value = ko.observable(value);
    self.isActive = ko.observable(true);

    self.reset = function(isActive) {
      self.value(defaultValue !== undefined ? defaultValue : value);
      self.isActive(isActive);
    }
  }

  function itemsInRange(array, a, b) {
    var i = _.indexOf(array, a);
    var j = _.indexOf(array, b);

    if (i == -1) {
      if (j == -1) {
        return [];
      } else {
        return array.slice(0, j + 1);
      }
    } else {
      if (j == -1) {
        return array.slice(0, i + 1);
      } else {
        return array.slice(Math.min(i, j), Math.max(i, j) + 1);
      }
    }
  }

  // Export
  ns.BatchEditor = BatchEditor;
})(jsu);
// DEPENDENCY: underscore

(function(ns) {
  "use strict";

  function shiftDate(dt, delta) {
    dt.add(delta);
    if ("quarters" in delta) {
      var months = delta.quarters * 3;
      dt.add("months", months);
    }
    return dt;
  }

  function rangeIncludes(dr, d) {
    var lb = dr[0].valueOf();
    var ub = dr[1].valueOf();
    var v = d.valueOf();
    return v >= lb && v <= ub;
  }

  // if what should be either "date" or "moment". Default is "date"
  function getDate(dt, what) {
    // NOTE: Commented out lines below to make sure a new object is always returned.
    if (what == "moment") {
      // if (moment.isMoment(dt)) { return dt; }
      return moment(dt);
    } else {
      // if (dt instanceof Date) { return dt; }
      if (moment.isMoment(dt)) { return dt.toDate(); }
      return moment(dt).toDate();
    }
  }

  // obj must be either a Date object, or a moment object.
  // fmt must be one of: ["date", "moment", "iso"] or a valid moment format string. Default "date"
  function formatDate(obj, fmt) {
    if (fmt && fmt != "date") {
      var mom = null;
      var converted = false;
      if (moment.isMoment(obj)) {
        mom = obj;
      } else {
        mom = moment(obj);
        converted = true;
      }
      switch (fmt) {
        case "moment":
          return converted ? mom : moment(mom);
        case "iso":
          return mom.format("YYYY-MM-DD");
        case "quarter-array":
          return [mom.year(), getQuarter(mom)];
        case "quarter-pretty":
          return "Q" + getQuarter(mom) + " '" + mom.year().toString().substring(2);
        case "value":
          return mom.valueOf();
        default:
          return mom.format(fmt);
      }
    } else {
      if (moment.isMoment(obj)) { return obj.toDate(); }
      return obj;
    }
  }

  // Returns 1-based quarter of moment object:
  // [jan, feb, mar] -> 1
  // [apr, may, jun] -> 2
  // [jul, aug, sep] -> 3
  // [oct, nov, dec] -> 4
  function getQuarter(mom) { return Math.ceil((mom.month() + 1) / 3); }

  // Finds dates and generates date ranges relative to a given week start day.
  function DateFinder(weekStartsOnDay, weekBasedBounds) {
    if (weekStartsOnDay < 0 || weekStartsOnDay > 6) {
      throw new Error("weekStartsOnDay must be in [0, 6]");
    }
    this.weekStartsOnDay = weekStartsOnDay;
    this.weekBasedBounds = weekBasedBounds !== undefined ? weekBasedBounds : true;
  }

  var cls = DateFinder;

  // arguments:
  // - dt - A value that will return a valid value from a call of moment(dt)
  // - unit - What to get the start of: ["week", "month", "quarter", "year"]
  // - fmt - The format for output, passed directly to formatDate
  // - delta - hash of units and values to shift date by.
  cls.prototype.getStartOf = function(dt, unit, fmt, opts) {
    if (!dt) { throw new Error("Invalid date"); }
    dt = getDate(dt, "moment");
    unit = unit || "week";
    fmt = fmt || "date";

    if (opts && opts.delta) { shiftDate(dt, opts.delta); }

    var sou = null;
    var fd = null;

    if (this.weekBasedBounds) {
      fd = moment(this._getStartOfWeek(dt));
    } else {
      fd = dt;
    }

    switch(unit) {
      case "week":
        sou = this._getStartOfWeek(fd);
        break;
      case "month":
        sou = fd.startOf("month");
        break;
      case "quarter":
        sou = moment([fd.year(), (getQuarter(fd) - 1) * 3]);
        break;
      case "year":
        sou = fd.startOf("year");
        break;
      default:
        throw new Error("Unknown unit: " + unit);
    }

    if (this.weekBasedBounds && !this._isStartOfWeek(sou)) {
      sou = moment(this._getStartOfWeek(sou)).add(7, "days");
    }

    return formatDate(sou, fmt);
  }

  cls.prototype.getEndOf = function(dt, unit, fmt, opts) {
    if (!dt) { throw new Error("Invalid date"); }
    dt = getDate(dt, "moment");
    unit = unit || "week";
    fmt = fmt || "date";

    if (opts && opts.delta) { shiftDate(dt, opts.delta); }

    var eou = null;

    switch(unit) {
      case "week":
        eou = this._getEndOfWeek(dt);
        break;
      case "month":
        eou = moment(dt).add(1, "months").startOf("month").subtract(1, "days");
        break;
      case "quarter":
        eou = moment([dt.year(), (getQuarter(dt) -1) * 3]).add(3, "months").startOf("month").subtract(1, "days");
        break;
      case "year":
        eou = moment([dt.year(), 11, 31]);
        break;
      default:
        throw new Error("Unknown unit: " + unit);
    }

    if (this.weekBasedBounds && unit != "week") {
      eou = this._getEndOfWeek(eou);
    }

    return formatDate(eou, fmt);
  }

  cls.prototype.getDateRanges = function(args) {
    args = _.extend({}, getDateRangesDefaultArgs, args);
    if (!args.fromDate) { throw new Error("Invalid from date"); }
    if (!args.toDate) { throw new Error("Invalid to date"); }

    var fd = this.weekBasedBounds ? this.getStartOf(args.fromDate, args.unit, "moment") : getDate(args.fromDate, "moment");
    var td = this.weekBasedBounds ? this.getEndOf(args.toDate, args.unit, "moment") : getDate(args.toDate, "moment");

    // var fdDebug = "From Date: " + fd.toString() + " / " + fd.valueOf();
    // var tdDebug = "  To Date: " + td.toString() + " / " + td.valueOf();

    // if (!td.isValid() || !fd.isValid() || td.valueOf() <= fd.valueOf()) {
    //   var txt = "There was an error with the date range provided.\n  " + fdDebug + "\n  " + tdDebug;
    //   throw txt;
    //   // return [];
    // }

    var currentRange = [moment(fd), this.getEndOf(fd, args.unit, "moment")];
    var ranges = [currentRange];

    // while (!(td.valueOf() < currentRange[0].valueOf() || rangeIncludes(currentRange, td))) {
    while (td.valueOf() > currentRange[0].valueOf() && !rangeIncludes(currentRange, td)) {
      if (td.valueOf() < currentRange[0].valueOf()) {
        break;
      }
      if (rangeIncludes(currentRange, td)) {
        break;
      }

      var nlb = moment(currentRange[1]).add(1, "days");
      var nub = this.getEndOf(nlb, args.unit, "moment");
      currentRange = [nlb, nub];
      ranges.push(currentRange);
    }

    if (args.format != "moment") {
      for (var i = 0; i < ranges.length; ++i) {
        var range = ranges[i];
        range[0] = formatDate(range[0], args.format);
        range[1] = formatDate(range[1], args.format);
      }
    }

    if (args.onlyRangeStart) {
      return _.map(ranges, function(r) { return r[0]; });
    } else {
      return ranges;
    }
  }

  var getDateRangesDefaultArgs = {
    fromDate: null,
    toDate: null,
    unit: "week",
    format: "date",
    onlyRangeStart: false
  };

  cls.prototype._getStartOfWeek = function(dt) {
    dt = getDate(dt, "date");
    var day = dt.getDay();
    var date = dt.getDate();
    var newDt = new Date(dt.valueOf());
    var diff = (day - this.weekStartsOnDay) % 7;
    diff = diff < 0 ? 7 + diff : diff;
    newDt.setDate(date - diff);
    return newDt;
  }

  cls.prototype._getEndOfWeek = function(dt) {
    var s = this._getStartOfWeek(dt);
    s.setDate(s.getDate() + 6);
    return s;
  }

  cls.prototype._isStartOfWeek = function(dt) {
    var m = null;
    if (dt instanceof Date) {
      m = dt.getDay();
    } else {
      if (moment.isMoment(dt)) {
        m = dt.day();
      } else {
        m = moment(dt).day();
      }
    }
    return m == this.weekStartsOnDay;
  }

  cls.prototype.formatDate = formatDate;

  // Export
  ns.DateFinder = DateFinder;
})(jsu);
(function(ns) {
  "use strict";

  function DelayedActor(action, interval) {
    this.action = action;
    this.interval = interval;
    this.needsAction = false;
    this.timeout = null;
  }

  DelayedActor.prototype.act = function() {
    clearTimeout(this.timeout);
    var da = this;
    this.timeout = setTimeout(function() { da.actNow(); }, this.interval);
    this.needsAction = true;
  }

  DelayedActor.prototype.actNow = function(args, force) {
    clearTimeout(this.timeout);
    if (force || this.needsAction) {
      this.action(args);
      this.needsAction = false;
    }
  }

  DelayedActor.prototype.cancel = function() {
    clearTimeout(this.timeout);
  }

  ns.DelayedActor = DelayedActor;
})(jsu);
// DEPENDENCY: jQuery
// DEPENDENCY: utils/event-registration

(function(ns) {
  "use strict";

  function DragDropManager(args) {
    var self = this;

    self.movementThreshold = args.movementThreshold;
    self.flyBack = args.flyBack;

    self.isMouseDown = false;
    self.isDragging = false;
    self.dragOrigin = null;
    self.getDragData = null;
    self.getDragVisual = null;
    self.onDragCancelled = null;
    self.dragData = null;
    self.dragVisual = null;

    jsu.util.registerEvent(document, "mouseup", function(e) { self.globalMouseUp(e); });
    jsu.util.registerEvent(document, "mousemove", function(e) { self.globalMouseMove(e); });
  }

  DragDropManager.prototype.dropCleanup = function() {
    var self = this;
    self.isMouseDown = false;
    self.isDragging = false;
    self.dragOrigin = null;
    self.getDragData = null;
    self.getDragVisual = null;
    self.onDragCancelled = null;
    self.dragData = null;
    if (self.dragVisual) {
      self.dragVisual.remove();
    }
    self.dragVisual = null;
  };

  DragDropManager.prototype.dragCancelled = function() {
    var self = this;
    if (self.flyBack && self.dragVisual) {
      self.dragVisual.animate({ left: self.dragOrigin[0], top: self.dragOrigin[1] }, 400, "swing", function() {
        if (self.onDragCancelled) { self.onDragCancelled(self.dragData); }
        self.dropCleanup();
      });
    } else {
      if (self.onDragCancelled) { self.onDragCancelled(self.dragData); }
      self.dropCleanup();
    }
  }

  DragDropManager.prototype.globalMouseUp = function(e) {
    var self = this;
    if (self.isDragging) {
      self.dragCancelled();
    } else {
      self.dropCleanup();
    }
  };

  DragDropManager.prototype.globalMouseMove = function(e) {
    var self = this;
    if (self.isMouseDown && !self.isDragging && distance(self.dragOrigin, [e.pageX, e.pageY]) >= self.movementThreshold) {
      self.isDragging = true;
      self.dragData = self.getDragData();
      if (self.getDragVisual) {
        self.dragVisual = $(self.getDragVisual(self.dragData, e));
        self.dragVisual.appendTo(document.body);
        self.dragVisual.css({ top: e.pageY + 5, left: e.pageX + 5 });
      }
      self.getDragData = null;
      self.getDragVisual = null;
    } else if (self.dragVisual) {
      self.dragVisual.css({ top: e.pageY + 5, left: e.pageX + 5 });
    }
  };

  DragDropManager.prototype.registerDropElement = function(element, scope, processDrop, acceptsDrop) {
    var self = this;
    var handler = function(e) {
      if (self.isDragging) {
        e.preventDefault();
        var successful = false;

        try {
          if (!acceptsDrop || acceptsDrop(scope, self.dragData)) {
            processDrop(scope, self.dragData);
            successful = true;
            e.stopPropagation();
          } else if (acceptsDrop && !acceptsDrop(scope, self.dragData)) {
            self.dragCancelled();
          }
        } finally {
          if (!successful) {
            // if (self.onDragCancelled) { self.onDragCancelled(); }
            self.dragCancelled();
            e.stopPropagation();
          } else {
            self.dropCleanup();
          }
        }
      }
    };
    jsu.util.registerEvent(element, "mouseup", handler);
  };

  DragDropManager.prototype.registerDragElement = function(element, getDragData, onDragCancelled, getDragVisual) {
    var self = this;
    var handler = function(e) {
      if (!DragDropManager.elementCanDrag(e.target.nodeName)) { return; }
      e.preventDefault();
      e.stopPropagation();
      self.isMouseDown = true;
      self.getDragData = getDragData;
      self.getDragVisual = getDragVisual;
      self.onDragCancelled = onDragCancelled;
      self.dragOrigin = [e.pageX, e.pageY];
    };
    jsu.util.registerEvent(element, "mousedown", handler);
  };

  DragDropManager.elementCanDrag = function(nodeName) {
    switch(nodeName) {
      case "INPUT":
      case "SELECT":
      case "TEXTAREA":
      case "BUTTON":
      case "OPTGROUP":
      case "OPTION":
      case "DATALIST":
      case "KEYGEN":
      case "OUTPUT":
        return false;
      default:
        return true;
    }
  }

  function distance(p0, p1) {
    return Math.sqrt(Math.pow(p0[0] - p1[0], 2) + Math.pow(p0[1] - p1[1], 2));
  }

  // Export
  ns.DragDropManager = DragDropManager;
})(jsu);
// DEPENDENCY: underscore

(function(ns) {
  "use strict";

  function EventAggregator() {
    var self = this;

    var specificSubscriptions = {};
    var globalSubscriptions = {};
    var keys = {};
    var id = 0;

    function newSubscriptionKey() {
      return id++;
    }

    self.subscribe = function() {
      switch (arguments.length) {
      case 1:
        if (!_.isFunction(arguments[0])) throw new Error("EventAggregator.subscribe called with one parameter expects a function");
        return subscribeToAll(arguments[0]);
        break;
      case 2:
        if (!_.isString(arguments[0])) throw new Error("EventAggregator.subscribe called with two parameters expects a string as the first parameter");
        if (!_.isFunction(arguments[1])) throw new Error("EventAggregator.subscribe called with two parameters expects a function as the second parameter");
        return subscribeToMessage(arguments[0], arguments[1]);
        break;
      default:
        throw new Error("EventAggregator.subscribe expects either a single function parameter, or a string and a function parameter");
      }
    }

    self.unsubscribe = function(key) {
      var message = keys[key];

      if (message === null) {
        unsubscribeFromAll(key);
      } else {
        unsubscribeFromMessage(message, key);
      }
      return self;
    }

    self.broadcast = function(sender, message, data) {
      if (specificSubscriptions[message]) {
        for (var callback in specificSubscriptions[message]) {
          specificSubscriptions[message][callback](sender, message, data);
        }
      }
      for (var callback in globalSubscriptions) {
        globalSubscriptions[callback](sender, message, data);
      }
      return self;
    }

    function subscribeToMessage(message, callback) {
      if (!specificSubscriptions[message]) {
        specificSubscriptions[message] = {};
      }
      var key = newSubscriptionKey();
      specificSubscriptions[message][key] = callback;
      keys[key] = message;
      return key;
    }

    function subscribeToAll(callback) {
      var key = newSubscriptionKey();
      globalSubscriptions[key] = callback;
      keys[key] = null;
      return key;
    }

    function unsubscribeFromMessage(message, subscriptionKey) {
      if (specificSubscriptions[message] && specificSubscriptions[message][subscriptionKey]) {
        delete specificSubscriptions[message][subscriptionKey];
        delete keys[subscriptionKey];
      }
    }

    function unsubscribeFromAll(subscriptionKey) {
      if (globalSubscriptions[subscriptionKey]) {
        delete globalSubscriptions[subscriptionKey];
        delete keys[subscriptionKey];
      }
    }
  };

  // Export
  ns.EventAggregator = EventAggregator;
})(jsu);
jsu.expression = jsu.expression || {};

(function(ns) {
  "use strict";

  var Associativity = {
    left: 1,
    right: 2
  };

  function setParseId(cons, value) {
    cons.prototype.parseId = cons.parseId = value;
  }

  function OperatorToken(symbol, precedence, associativity, cardinality, execute, isShortCircuit) {
    this.symbol = symbol;
    this.precedence = precedence;
    this.associativity = associativity;
    this.cardinality = cardinality;
    this.execute = execute;
    this.isShortCircuit = isShortCircuit;
  }

  function applyOr(evaluator, lht, rht) {
    var lh = evaluator.getValue(lht);

    if (lh) { return true; }

    var rh = evaluator.getValue(rht);

    return rh;
  }

  function applyAnd(evaluator, lht, rht) {
    var lh = evaluator.getValue(lht);

    if (!lh) { return false; }

    var rh = evaluator.getValue(rht);

    return rh;
  }

  function saveDiv(n, d, fallback) {
    if (n === null || n === undefined || d === null || d === undefined || d === 0) {
      return fallback;
    } else {
      return n / d;
    }
  }

  setParseId(OperatorToken, "operator");

  OperatorToken.prototype.toString = function() { return this.symbol; }

  OperatorToken.or                 = new OperatorToken("|",  1, Associativity.left, 2, applyOr, true);

  OperatorToken.and                = new OperatorToken("&",  2, Associativity.left, 2, applyAnd, true);

  OperatorToken.add                = new OperatorToken("+",  3, Associativity.left, 2, function(a, b) { return a + b; });
  OperatorToken.subtract           = new OperatorToken("-",  3, Associativity.left, 2, function(a, b) { return a - b; });

  OperatorToken.equal              = new OperatorToken("=",  3, Associativity.left, 2, function(a, b) { return a == b; });
  OperatorToken.greaterThan        = new OperatorToken(">",  3, Associativity.left, 2, function(a, b) { return a > b; });
  OperatorToken.greaterThanOrEqual = new OperatorToken(">=", 3, Associativity.left, 2, function(a, b) { return a >= b; })
  OperatorToken.lessThan           = new OperatorToken("<",  3, Associativity.left, 2, function(a, b) { return a < b; });;
  OperatorToken.lessThanOrEqual    = new OperatorToken("<=", 3, Associativity.left, 2, function(a, b) { return a <= b; });
  OperatorToken.notEqual           = new OperatorToken("!=", 3, Associativity.left, 2, function(a, b) { return a != b; });

  OperatorToken.multiply           = new OperatorToken("*",  4, Associativity.left, 2, function(a, b) { return a * b; });
  OperatorToken.divide             = new OperatorToken("/",  4, Associativity.left, 2, function(a, b) { return a / b; });
  OperatorToken.safeDivide         = new OperatorToken("//", 4, Associativity.left, 2, function(a, b) { return saveDiv(a, b, 0); });

  OperatorToken.not                = new OperatorToken("!",  5, Associativity.right, 1, function(a) { return !a; });

  OperatorToken.power              = new OperatorToken("^",  6, Associativity.right, 2, function(a, b) { return Math.pow(a, b); });
  // Specially handled operators
  OperatorToken.negate             = new OperatorToken("-",  4, Associativity.right, 1, function(a) { return -a; });
  OperatorToken.identity           = new OperatorToken("+",  4, Associativity.right, 1, function(a) { return a; });

  OperatorToken.operators = [
    OperatorToken.add,
    OperatorToken.and,
    OperatorToken.divide,
    OperatorToken.safeDivide,
    OperatorToken.equal,
    OperatorToken.greaterThan,
    OperatorToken.greaterThanOrEqual,
    OperatorToken.lessThan,
    OperatorToken.lessThanOrEqual,
    OperatorToken.multiply,
    OperatorToken.not,
    OperatorToken.notEqual,
    OperatorToken.or,
    OperatorToken.power,
    OperatorToken.subtract
  ].sort(function(a, b) { return b.symbol.length - a.symbol.length });


  function VariableToken(name) {
    this.name = name;
  }

  setParseId(VariableToken, "variable");

  VariableToken.prototype.toString = function() { return this.name; }


  function SubexpressionToken(input) {
    this.input = input;
  }

  setParseId(SubexpressionToken, "subexpression");

  SubexpressionToken.prototype.expression = function() {
    if (!this._expression) {
      this._expression = this.input.slice(1, -1);
    }
    return this._expression;
  }

  SubexpressionToken.prototype.toString = function() { return this.expression(); }


  // For wrapping native values: numbers, strings, booleans
  function WrapperToken(value) {
    this.value = value;
  }

  setParseId(WrapperToken, "wrapper");


  function FunctionToken(input, evaluateArgs) {
    this.input = input;
    if (evaluateArgs) {
      this.argumentTokens();
    }
  }

  setParseId(FunctionToken, "function");

  FunctionToken.prototype.name = function() {
    if (!this._name) {
      var op = this.input.indexOf("(");
      this._name = this.input.slice(0, op).replace(/\s+$/, "");
    }
    return this._name;
  }

  FunctionToken.prototype.argumentTokens = function() {
    if (!this._argumentTokens) {
      var op = this.input.indexOf("(");
      var argsInput = this.input.slice(op + 1, -1);
      var startPosition = 0;
      var args = [];

      while (startPosition < argsInput.length) {
        var tokenizer = new Tokenizer(argsInput, { haltOn: ',', startPosition: startPosition });
        var tokens = [];

        while (!tokenizer.done) {
          var t = tokenizer.getNextToken();

          if (t !== undefined) {
            tokens.push(t);
          }
        }

        args.push(tokens);
        startPosition = tokenizer.currentPosition + 1;
      }

      this._argumentTokens = args;
    }
    return this._argumentTokens;
  }

  FunctionToken.prototype.toString = function() { return this.input; }


  function Tokenizer(input, opts) {
    opts = opts || {};
    this.input = input;
    this.haltOn = opts.haltOn;
    this.currentPosition = opts.startPosition || 0;
    this.done = !(input && input.length);
  }

  Tokenizer.prototype.advance = function(n) {
    this.currentPosition += n || 1;
    if (this.currentPosition >= this.input.length) {
      this.done = true;
    }
  }

  Tokenizer.prototype.currentCharacter = function() { return this.input.charAt(this.currentPosition); }

  Tokenizer.prototype.relativeCharacter = function(n) {
    if (n === undefined) {
      n = 0;
    }
    return this.input.charAt(this.currentPosition + n);
  }

  Tokenizer.prototype.getNextToken = function() {
    var cc = this.currentCharacter();

    while (Tokenizer.isWhiteSpace(cc) && !this.done) {
      this.advance();
      cc = this.currentCharacter();
    }

    if (cc == this.haltOn) {
      this.done = true;
    }

    var token = undefined;

    // NOTE: Order here is important.
    // First check functions, then operators.
    if (this.done) { /* pass */ }
    else if (this.operatorIsNext()) { token = this.getOperatorToken(); }
    else if (this.subexpressionIsNext()) { token = this.getSubexpressionToken(); }
    else if (this.stringIsNext()) { token = this.getStringToken(); }
    else if (this.regExpIsNext()) { token = this.getRegExpToken(); }
    else if (this.numberIsNext()) { token = this.getNumberToken(); }
    else if (this.booleanIsNext()) { token = this.getBooleanToken(); }
    else if (this.functionIsNext()) { token = this.getFunctionToken(); }
    else if (this.variableIsNext()) { token = this.getVariableToken(); }
    else if (this.whitespaceVariableIsNext()) { token = this.getWhitespaceVariable(); }
    else { throw new Error("Unable to parse character '" + cc + "' at position " + this.currentPosition); }

    return token;
  }

  Tokenizer.prototype.operatorIsNext = function() {
    // NOTE: It's important that the operators are sorted by symbol length
    //       descending.
    for (var i = 0; i < OperatorToken.operators.length; ++i) {
      var symbol = OperatorToken.operators[i].symbol;

      if (this.input.indexOf(symbol, this.currentPosition) == this.currentPosition) {
        return true;
      }
    }

    return false;
  }

  Tokenizer.prototype.getOperatorToken = function() {
    var op = null;

    // NOTE: It's important that the operators are sorted by symbol length
    //       descending.
    for (var i = 0; i < OperatorToken.operators.length; ++i) {
      var symbol = OperatorToken.operators[i].symbol;

      if (this.input.indexOf(symbol, this.currentPosition) == this.currentPosition) {
        op = OperatorToken.operators[i];
        break;
      }
    }

    if (op == null) {
      throw new Error("Unrecognized operator '" + c + "' at position " + this.currentPosition);
    }

    this.advance(op.symbol.length);

    return op;
  }

  Tokenizer.prototype.subexpressionIsNext = function() { return this.currentCharacter() == "("; }

  Tokenizer.prototype.getSubexpressionToken = function() {
    var fp = this.currentPosition;
    var depth = 1;
    this.advance();

    while(!this.done && depth > 0) {
      var cc = this.currentCharacter();

      if (cc == "(") {
        depth += 1;
      } else if (cc == ")") {
        depth -= 1;
      }

      this.advance();
    }

    if (this.input.charAt(this.currentPosition - 1) != ")") {
      throw "Unbalanced parentheses starting at position " + fp;
    }

    return new SubexpressionToken(this.input.slice(fp, this.currentPosition));
  }

  Tokenizer.prototype.stringIsNext = function() {
    var c = this.currentCharacter();
    return c == "'" || c == "\"";
  }

  Tokenizer.prototype.getStringToken = function() {
    var fp = this.currentPosition;
    var delimiter = this.currentCharacter();
    var escaped = false;
    var rv = "";

    this.advance();
    var cc = this.currentCharacter();

    while (!this.done && (cc != delimiter || escaped)) {
      if (escaped) {
        rv += cc;
        escaped = false;
      } else if (cc == "\\") {
        escaped = true;
      } else {
        rv += cc;
      }

      this.advance();
      cc = this.currentCharacter();
    }

    if (this.done) {
      throw new Error("Unterminated string starting at position " + fp);
    }

    this.advance();

    return new WrapperToken(rv);
  }

  Tokenizer.prototype.regExpIsNext = function() {
    return this.input.indexOf("re/", this.currentPosition) == this.currentPosition;
  }

  Tokenizer.prototype.getRegExpToken = function() {
    var fp = this.currentPosition;
    var delimiter = "/";
    var escaped = false;
    var rva = [];

    this.advance(3);
    var cc = this.currentCharacter();

    while(!this.done && (cc != delimiter || escaped)) {
      if (cc == "\\") {
        escaped = !escaped;
      } else if (cc == delimiter) {
        if (escaped) {
          rva.pop();
        }
      } else {
        escaped = false;
      }

      rva.push(cc);
      this.advance();
      cc = this.currentCharacter();
    }

    if (this.done) {
      throw new Error("Unterminated regex starting at position " + fp);
    }

    this.advance();

    return new WrapperToken(new RegExp(rva.join("")));
  }

  Tokenizer.prototype.numberIsNext = function() { return Tokenizer.numberStartRe.test(this.currentCharacter()); }

  Tokenizer.prototype.getNumberToken = function() {
    var decimalFound = false;
    var fp = this.currentPosition;
    var cc = this.currentCharacter();

    while(!this.done) {
      if (cc == ".") {
        if (decimalFound) {
          throw new Error("Malformed number starting at position " + fp);
        } else {
          decimalFound = true;
        }
      } else if (!Tokenizer.numberRe.test(cc)) {
        break;
      }

      this.advance();
      var cc = this.currentCharacter();
    }

    var s = this.input.slice(fp, this.currentPosition);

    return new WrapperToken(decimalFound ? parseFloat(s) : parseInt(s));
  }

  Tokenizer.prototype.booleanIsNext = function() {
    return /^(true|false)($|\W)/.test(this.input.slice(this.currentPosition));
  }

  Tokenizer.prototype.getBooleanToken = function() {
    var cc = this.currentCharacter();

    if (cc == "t") {
      this.advance(4);
      return new WrapperToken(true);
    } else {
      this.advance(5);
      return new WrapperToken(false);
    }
  }

  Tokenizer.prototype.functionIsNext = function() {
    return /^[a-z_]\w*\s*\(/i.test(this.input.slice(this.currentPosition));
  }

  Tokenizer.prototype.getFunctionToken = function() {
    var fp = this.currentPosition;
    var argumentsFound = false;
    var depth = 0;

    this.advance();

    while (!this.done && (!argumentsFound || depth != 0)) {
      var cc = this.currentCharacter();

      if (cc == "(") {
        argumentsFound = true;
        depth += 1;
      } else if (cc == ")") {
        if (argumentsFound) {
          depth -= 1;
        } else {
          throw new Error("Malformed function expression starting at position " + fp);
        }
      }

      this.advance();
    }

    if (this.input.charAt(this.currentPosition - 1) != ")") {
      throw new Error("Malformed function expression starting at position " + fp);
    }

    return new FunctionToken(this.input.slice(fp, this.currentPosition), true);
  }

  Tokenizer.prototype.variableIsNext = function() { return Tokenizer.variableStartRe.test(this.currentCharacter()); }

  Tokenizer.prototype.getVariableToken = function() {
    var fp = this.currentPosition;

    this.advance();

    var cc = this.currentCharacter();

    while (!this.done && Tokenizer.variableRe.test(cc)) {
      this.advance();
      cc = this.currentCharacter();
    }

    return new VariableToken(this.input.slice(fp, this.currentPosition));
  }

  Tokenizer.prototype.whitespaceVariableIsNext = function() { return this.currentCharacter() == "["; }

  Tokenizer.prototype.getWhitespaceVariable = function() {
    var fp = this.currentPosition;

    this.advance();

    while(!this.done && this.currentCharacter() != "]") { this.advance(); }

    if (this.done) {
      throw new Error("Unterminated whitespace variable starting at position " + fp);
    }

    this.advance();

    return new VariableToken(this.input.slice(fp + 1, this.currentPosition - 1));
  }

  Tokenizer.isWhiteSpace = function(c) { return Tokenizer.whiteSpaceRe.test(c); }

  Tokenizer.whiteSpaceRe = /\s/;
  Tokenizer.numberStartRe = /\d|\./;
  Tokenizer.numberRe = /\d/;
  Tokenizer.variableStartRe = /[a-z_\$]/i;
  Tokenizer.variableRe = /\w|\$/;


  function Parser() { }

  Parser.prototype.stringToQueue = function(input) {
    var tokenizer = new Tokenizer(input);
    var tokens = [];
    var lastToken = null;

    while (!tokenizer.done) {
      var token = tokenizer.getNextToken();

      // Properly handle signed variables, function calls and numbers. If the
      // last token is null, or was an operator, and the current token is + or
      // -, then we should push unary identity or negation into the queue.
      if (token.parseId == OperatorToken.parseId && (lastToken == null || lastToken.parseId == OperatorToken.parseId)) {
        if (token == OperatorToken.add) {
          token = OperatorToken.identity;
        } else if (token == OperatorToken.subtract) {
          token = OperatorToken.negate;
        }
      }

      lastToken = token;

      tokens.push(token);
    }

    return this.tokensToQueue(tokens);
  }

  Parser.prototype.tokensToQueue = function(tokens) {
    var operators = [];
    var output = [];
    var currentOperator = null;

    for (var i = 0; i < tokens.length; ++i) {
      var token = tokens[i];

      switch(token.parseId) {
        case SubexpressionToken.parseId:
          output.push.apply(output, this.stringToQueue(token.expression()));
          break;
        case WrapperToken.parseId:
        case VariableToken.parseId:
        case FunctionToken.parseId:
          output.push(token);
          break;
        case OperatorToken.parseId:
          if (currentOperator && (currentOperator.precedence > token.precedence || currentOperator.precedence == token.precedence && currentOperator.associativity == Associativity.left)) {
            output.push(operators.pop());
          }
          currentOperator = token;
          operators.push(token);
          break;
        default:
          throw new Error("Unrecognized token: " + token + " (" + token.parseId + ")");
      }
    }

    while(operators.length) {
      output.push(operators.pop());
    }

    return output;
  }


  function Evaluator(scope, debug) {
    this.scope = scope || new EvaluatorScope();
    this.debug = debug;
    this.values = [];
  }

  Evaluator.prototype.evaluateString = function(input) {
    var queue = new Parser().stringToQueue(input);
    return this.evaluateQueue(queue);
  }

  Evaluator.prototype.evaluateQueue = function(queue) {
    this.values = [];
    for (var i = 0; i < queue.length; ++i) {
      var token = queue[i];

      switch(token.parseId) {
        case WrapperToken.parseId:
        case VariableToken.parseId:
        case FunctionToken.parseId:
          this.values.push(token);
          break;
        case OperatorToken.parseId:
          this.applyOperator(token);
          break;
        default:
          throw new Error("Unrecognized token: " + token + " (" + token.parseId + ")");
      }
    }

    if (this.values.length != 1) {
      throw new Error("Invalid formula");
    }

    return this.getValue(this.values[0]);
  }

  Evaluator.prototype.applyOperator = function(operator) {
    if (this.values.length < operator.cardinality) {
      throw new Error("Operator '" + operator.symbol + "' expects " + operator.cardinality + " operands");
    }

    var rht = this.values.pop();
    var result = null;

    if (operator.cardinality == 1) {
      var rhs = this.getValue(rht);
      result = operator.execute(rhs);
      if (this.debug) { console.log(operator + " " + rhs + " = " + result); }
    } else {
      var result = null;

      if (operator.isShortCircuit) {
        var lht = this.values.pop();
        result = operator.execute(this, lht, rht);
      } else {
        var rhs = this.getValue(rht);
        var lhs = this.getValue(this.values.pop());
        result = operator.execute(lhs, rhs);
      }

      if (this.debug) { console.log(lhs + " " + operator + " " + rhs + " = " + result); }
    }

    this.values.push(new WrapperToken(result));
  }

  Evaluator.prototype.getValue = function(token) {
    switch(token.parseId) {
      case WrapperToken.parseId:
        return token.value;
      case VariableToken.parseId:
        return this.evaluateVariable(token);
      case FunctionToken.parseId:
        return this.evaluateFunction(token);
      default:
        "Unrecognized token: " + token + " (" + token.parseId + ")";
    }
  }

  Evaluator.prototype.evaluateVariable = function(variable) {
    var v = this.scope.getVariable(variable.name);

    if (v === undefined) { throw new Error("Undefined variable: " + variable.name); }

    return v;
  }

  Evaluator.prototype.evaluateFunction = function(functionExpression) {
    var fx = this.scope.getFunction(functionExpression.name());

    if (fx === undefined) { throw new Error("Undefined function: " + functionExpression.name()); }

    var argsTokens = functionExpression.argumentTokens();
    var args = [];

    if (Evaluator.hasSpecialProperty(fx, "scopeAware")) {
      args.push(this.scope);
    }

    if (Evaluator.hasSpecialProperty(fx, "unevaluatedArguments")) {
      for (var i = 0; i < argsTokens.length; ++i) {
        args.push(argsTokens[i]);
      }
    } else {
      for (var i = 0; i < argsTokens.length; ++i) {
        args.push(Evaluator.mapFunctionArg(this.scope, argsTokens[i]));
      }
    }

    return fx.apply(this, args);
  }

  Evaluator.hasSpecialProperty = function(fx, propertyName) {
    if (!fx.specialProperties) { return false; }
    if (propertyName in Evaluator.specialProperties) {
      return (fx.specialProperties & Evaluator.specialProperties[propertyName]) != 0
    } else {
      throw new Error("Unknown special property: " + propertyName);
    }
  }

  Evaluator.addSpecialProperties = function(fx) {
    var props = 0;
    for (var i = 1; i < arguments.length; ++i) {
      var n = arguments[i];
      if (n in Evaluator.specialProperties) {
        props = props | Evaluator.specialProperties[n];
      } else {
        throw new Error("Unknown special property: " + n);
      }
    }
    if (!fx.specialProperties) {
      fx.specialProperties = props;
    } else {
      fx.specialProperties = fx.specialProperties | props;
    }
    return fx;
  }

  Evaluator.mapFunctionArg = function(scope, tokens, mapToken) {
    var parser = new Parser();
    var editedTokens;

    if (mapToken) {
      editedTokens = [];
      for (var i = 0; i < tokens.length; ++i) {
        editedTokens.push(mapToken(tokens[i]));
      }
    } else {
      editedTokens = tokens;
    }

    var queue = parser.tokensToQueue(editedTokens);
    var evaluator = new Evaluator(scope);

    return evaluator.evaluateQueue(queue);
  }

  Evaluator.specialProperties = {
    none: 0x00,
    unevaluatedArguments: 0x01,
    scopeAware: 0x02,
    all: 0xff
  };


  function EvaluatorScope(global) {
    this.scope = [];

    if (global) { this.scope.push(global); }
  }

  EvaluatorScope.prototype.pushScope = function() {
    this.scope.push.apply(this.scope, arguments);
    return this;
  }

  EvaluatorScope.prototype.popScope = function() { return this.scope.pop(); }

  EvaluatorScope.prototype.each = function(fx) {
    for (var i = 0; i < this.scope.length; ++i) {
      fx(this.scope[i]);
    }
  }

  EvaluatorScope.prototype.getVariable = function(name) {
    for (var i = this.scope.length - 1; i >= 0; --i) {
      if (this.scope[i].getVariable) {
        var v = this.scope[i].getVariable(name);
        if (v !== undefined) {
          return v;
        }
      }
    }
    return undefined;
  }

  EvaluatorScope.prototype.getFunction = function(name) {
    for (var i = this.scope.length - 1; i >= 0; --i) {
      if (this.scope[i].getFunction) {
        var f = this.scope[i].getFunction(name);
        if (f !== undefined) {
          return f;
        }
      }
    }
    return undefined;
  }

  EvaluatorScope.mapToGetter = function(map, caseSensitive) {
    caseSensitive = caseSensitive === undefined ? true : caseSensitive;
    map = map || {};
    if (caseSensitive) {
      return function(name) { return map[name]; };
    } else {
      return function(name) { return map[name.toLowerCase()]; };
    }
  }


  function Dependency(args) {
    args = args || {};
    this.getName = args.getName;
    this.getFormula = args.getFormula;
    this.getTokenQueue = args.getTokenQueue;
    this.setTokenQueue = args.setTokenQueue;
  }

  // Get a dictionary whose keys are items that form circular references.
  Dependency.prototype.findCircularReferences = function(list) {
    var variables = this.prepareItems(list);
    var references = {};
    var circular = null;

    for (var k in variables) {
      references[k] = this.getVariables(variables[k]);
    }

    for (var k in variables) {
      if (this._hasCircularReferences(k, references)) {
        if (!circular) { circular = {}; }
        circular[k] = true;
      }
    }

    return circular;
  }

  // Return list in an order that ensures each items dependants are resolved before
  // it is calculated.
  Dependency.prototype.order = function(list) {
    var variables = this.prepareItems(list);
    var shallowReferences = {};
    var allReferences = {};

    for (var k in variables) {
      shallowReferences[k] = this.getVariables(variables[k]);
    }

    for (var k in variables) {
      allReferences[k] = this._getAllVariables(k, shallowReferences, true);
      if (k in allReferences[k]) {
        throw new Error("Circular reference found for " + k);
      }
    }

    var inserted = {};
    var insertedCount = 0;
    var ordered = [];
    var __looped = 0;

    while (insertedCount != list.length) {
      for (var i = 0; i < list.length; ++i) {
        var vname = this.getName(list[i]);
        var missingReferences = false;

        if (vname in inserted) { continue; }

        for (var rname in allReferences[vname]) {
          if (rname in variables && !(rname in inserted)) {
            missingReferences = true;
          }
        }

        if (!missingReferences) {
          inserted[vname] = true;
          insertedCount += 1;
          ordered.push(variables[vname]);
        }
      }

      if (++__looped > 1000) {
        throw new Error("Infinite loop?");
      }
    }

    return ordered;
  }

  // Get a map of all variables named in list that item depends on for resolution.
  Dependency.prototype.getAllVariables = function(list, item) {
    var variables = this.prepareItems(list);
    var shallowReferences = {};

    for (var k in variables) {
      shallowReferences[k] = this.getVariables(variables[k]);
    }

    return this._getAllVariables(this.getName(item), shallowReferences, true);
  }

  // Get the map of variables that are referenced directly by item.
  Dependency.prototype.getVariables = function(item) {
    var variables = {};
    var queue = this.getTokenQueue(item);

    for (var i = 0; i < queue.length; ++i) {
      this._getReferencedItems(queue[i], variables, VariableToken.parseId);
    }

    return variables;
  }

  Dependency.prototype.getFunctions = function(item) {
    this.prepareItems([item]);
    var fxs = {};
    var queue = this.getTokenQueue(item);

    for (var i = 0; i < queue.length; ++i) {
      this._getReferencedItems(queue[i], fxs, FunctionToken.parseId, "name()");
    }

    return fxs;
  }

  Dependency.prototype.prepareItems = function(list) {
    var vars = {};
    var parser = new Parser();

    for (var i = 0; i < list.length; ++i) {
      var item = list[i];
      vars[this.getName(item)] = item;
      if (!this.getTokenQueue(item)) {
        var queue = [];
        try {
          var queue = parser.stringToQueue(this.getFormula(item));
        } catch (ex) {
          queue = [];
        }
        this.setTokenQueue(item, queue);
      }
    }

    return vars;
  }

  Dependency.prototype._getReferencedItems = function(token, items, parseId, getName) {
    if (getName === undefined) {
      getName = "name";
    }
    if (token.parseId == parseId) {
      items[jsu.util.indexOrCall(token, getName)] = true;
    }
    if (token.parseId == FunctionToken.parseId) {
      var functionArgs = token.argumentTokens();
      for (var i = 0; i < functionArgs.length; ++i) {
        var argTokens = functionArgs[i];
        for (var j = 0; j < argTokens.length; ++j) {
          this._getReferencedItems(argTokens[j], items, parseId, getName);
        }
      }
    }
  }

  // Get the map of all variables referenced by name in allRefs
  Dependency.prototype._getAllVariables = function(name, allRefs, breakOnCircular) {
    var refs = {};
    var checked = {};
    var newRefFound = true;

    for (var k in allRefs[name]) {
      refs[k] = true;
    }

    // Prevent infinite loops
    checked[name] = true;

    while (newRefFound) {
      var newRefs = {};
      newRefFound = false;

      for (var ref in refs) {
        if (ref in checked) { continue; }

        if (ref in allRefs) {
          for (var newRef in allRefs[ref]) {
            if (!(newRef in refs)) {
              newRefs[newRef] = true;
              newRefFound = true;
            }
            if (newRef == name && breakOnCircular) {
              newRefFound = false;
              break;
            }
          }
        }
      }

      for (var newRef in newRefs) {
        refs[newRef] = true;
      }
    }

    return refs;
  }

  Dependency.prototype._hasCircularReferences = function(name, allRefs) {
    return (name in this._getAllVariables(name, allRefs, true));
  }


  // Export
  ns.OperatorToken = OperatorToken;
  ns.VariableToken = VariableToken;
  ns.SubexpressionToken = SubexpressionToken;
  ns.FunctionToken = FunctionToken;
  ns.Tokenizer = Tokenizer;
  ns.Parser = Parser;
  ns.Evaluator = Evaluator;
  ns.EvaluatorScope = EvaluatorScope;
  ns.Dependency = Dependency;
})(jsu.expression);
// DEPENDENCY: moment
// DEPENDENCY: underscore

(function(ns) {
  "use strict";

  var predefinedDateFormats = {
    "ISO": "YYYY-MM-DD",
    "US-Long": "MM/DD/YYYY",
    "US-Short": "MM/DD/YY",
    "US": "MM/DD/YYYY",
    "UK-Long": "DD/MM/YYYY",
    "UK-Short": "DD/MM/YY",
    "UK": "DD/MM/YYYY",
  };
  var predefinedTimeFormats = {
    "24 HR-Long": "HH:mm:ss",
    "24 HR-Short": "HH:mm",
    "24 HR": "HH:mm",
    "12 HR-Long": "h:mm:ss a",
    "12 HR-Short": "h:mm a",
    "12 HR": "h:mm a",
  };
  var formatNumberDefaultOpts = {
    decimalPlaces: -1,
    thousandsSeparator: ",",
    decimalSeparator: ".",
    nullString: "N/A",
    decimalMode: "fixed",
    abbreviate: false,
    prefix: "",
    suffix: "",
    signed: false
  };
  var truncateRepeatingDecimalsRegex = /^(\d*?)(\d+?)\2+$/;
  // NOTE: The trailing \d? will allow this to truncate numbers such as 1.666666667
  var truncateRepeatingDecimalsGreedyRegex = /^(\d*?)(\d+?)\2+\d?$/;
  var stripTrailingZerosRegex = /^(.+?)0*$/;
  var numberBreakpoints = [[1e12, "T"], [1e9, "B"], [1e6, "M"], [1e3, "K"]];
  var stringReplacementRegex = /\{(\d+)\}/g;

  function formatDate(dt, ofmt, ifmt) {
    if (!ofmt) {
      ofmt = predefinedDateFormats["ISO"];
    } else if (ofmt in predefinedDateFormats) {
      ofmt = predefinedDateFormats[ofmt];
    }

    if (ifmt && ifmt in predefinedDateFormats) {
      ifmt = predefinedDateFormats[ifmt];
    }

    // return moment.utc(dt, ifmt).format(ofmt);
    return moment(dt, ifmt).format(ofmt);
  }

  function formatTime(dt, ofmt, ifmt) {
    if (!ofmt) {
      ofmt = predefinedTimeFormats["24 HR"];
    } else if (ofmt in predefinedTimeFormats) {
      ofmt = predefinedTimeFormats[ofmt];
    }

    if (ifmt && ifmt in predefinedTimeFormats) {
      ifmt = predefinedTimeFormats[ifmt];
    }

    // return moment.utc(dt, ifmt).format(ofmt);
    return moment(dt, ifmt).format(ofmt);
  }

  function formatDateTime(dt, dfmt, tfmt, joiner, ifmt) {
    if (joiner === undefined) { joiner == " " };
    return formatDate(dt, dfmt, ifmt) + joiner + formatTime(dt, tfmt, ifmt);
  }

  function formatNumber(n, opts) {
    opts = _.extend({}, formatNumberDefaultOpts, opts);
    var suffix = null;

    if (!_.isNumber(n)) { return opts.nullString; }

    if (opts.abbreviate) {
      for (var i = 0; i < numberBreakpoints.length; ++i) {
        var bp = numberBreakpoints[i];
        if (n >= bp[0]) {
          n = n / bp[0];
          suffix = bp[1];
        }
      }
    }

    var numberString = opts.decimalPlaces >= 0 ? n.toFixed(opts.decimalPlaces) : n.toString();
    var parts = numberString.split(".");

    var whole = parts[0]
    var fractional = parts[1];

    if (n < 0) {
      whole = whole.slice(1);
    }

    if (opts.thousandsSeparator && whole.length > 3) {
      var separatorCount = Math.floor((whole.length - 1) / 3);

      for (var i = 0; i < separatorCount; ++i) {
        var pivot = whole.length - 3 * (i + 1) - i;
        whole = whole.slice(0, pivot) + opts.thousandsSeparator + whole.slice(pivot);
      }
    }

    // if (n < 0) {
    //   whole = "-" + whole;
    // }

    var theNumber = null;

    if (fractional !== undefined) {
      switch (opts.decimalMode) {
      case "trimzeros":
        fractional = fractional.replace(stripTrailingZerosRegex, "$1");
        break;
      case "trimrepeating+":
        fractional = fractional.replace(truncateRepeatingDecimalsGreedyRegex, "$1$2");
        break;
      case "trimrepeating":
        fractional = fractional.replace(truncateRepeatingDecimalsRegex, "$1$2");
        break;
      case "fixed":
      default:
        break;
      }

      theNumber = whole + opts.decimalSeparator + fractional;
    } else {
      theNumber = whole;
    }

    if (suffix) {
      theNumber += suffix;
    }

    var sign = n < 0 ? "-" : opts.signed ? "+" : "";

    return sign + opts.prefix + theNumber + opts.suffix;
  }

  function formatString(str) {
    var re = new RegExp(stringReplacementRegex);
    var parts = [];
    var fromIndex = 0;
    var m = re.exec(str);

    while (m) {
      parts.push(str.slice(fromIndex, m.index));
      var argIndex = parseInt(m[1]);
      var arg = arguments[argIndex + 1];
      if (arg === undefined) {
        throw new Error("Invalid index: " + argIndex + ". In format string " + str);
      }
      parts.push((arg !== null && arg !== undefined) ? arg.toString() : arg);
      fromIndex = re.lastIndex;
      m = re.exec(str);
    }

    parts.push(str.slice(fromIndex));

    return parts.join("");
  }

  function humanizeNumber(n) {
    var n = parseInt(n);
    var mh = n % 100;
    if (mh >= 11 && mh <= 13) { return n + "th"; }
    switch(n % 10) {
      case 1: return n + "st";
      case 2: return n + "nd";
      case 3: return n + "rd";
      default: return n + "th";
    }
  }

  function Formatter(opts) {
    opts = opts || {};
    this.opts = {};
    _.extend(this.opts, formatterDefaultOpts, opts);
    this.opts.numberFormat = _.extend({}, formatterDefaultOpts.numberFormat, opts.numberFormat);
  }

  Formatter.date = formatDate;
  Formatter.time = formatTime;
  Formatter.dateTime = formatDateTime;
  Formatter.number = formatNumber;
  Formatter.string = formatString;
  Formatter.humanizeNumber = humanizeNumber;

  Formatter.prototype.dateFormat = function() { return predefinedDateFormats[this.opts.dateFormat] || this.opts.dateFormat; }
  Formatter.prototype.timeFormat = function() { return predefinedTimeFormats[this.opts.timeFormat] || this.opts.timeFormat; }
  Formatter.prototype.dateTimeFormat = function() { return this.dateFormat() + this.opts.timestampJoiner + this.timeFormat(); }
  Formatter.prototype.date = function(dt, ofmt, ifmt) { return formatDate(dt, ofmt || this.dateFormat(), ifmt); }
  Formatter.prototype.time = function(dt, ofmt, ifmt) { return formatTime(dt, ofmt || this.timeFormat(), ifmt); }
  Formatter.prototype.dateTime = function(dt, dfmt, tfmt, joiner, ifmt) {
    return formatDateTime(dt, dfmt || this.opts.dateFormat, tfmt || this.opts.timeFormat, joiner || this.opts.timestampJoiner, ifmt);
  }
  Formatter.prototype.number = function(n, opts) { return formatNumber(n, _.extend({}, this.opts.numberFormat, opts)); }
  Formatter.prototype.humanizeNumber = humanizeNumber;
  Formatter.prototype.integer = function(n, opts) { return formatNumber(n, _.extend({}, this.opts.numberFormat, opts, { decimalPlaces: 0 })); }
  Formatter.prototype.money = function(n, opts) {
    return formatNumber(n, _.extend({}, this.opts.numberFormat, {
      decimalPlaces: 2,
      prefix: this.opts.numberFormat.currencySymbol,
      decimalMode: "fixed"
    }, opts));
  }
  Formatter.prototype.compactNumber = function(n, opts) {
    return formatNumber(n, _.extend({}, this.opts.numberFormat, {
      decimalPlaces: -1,
      decimalMode: "trimrepeating+"
    }, opts));
  }
  Formatter.prototype.abbrNumber = function(n, opts) {
    return formatNumber(n, _.extend({}, this.opts.numberFormat, {
      decimalPlaces: 2,
      decimalMode: "trimzeros",
      abbreviate: true
    }, opts));
  }
  Formatter.prototype.string = formatString;
  Formatter.dateFormats = _.keys(predefinedDateFormats).sort();
  Formatter.timeFormats = _.keys(predefinedTimeFormats).sort();

  var formatterDefaultOpts = {
    dateFormat: "ISO",
    timeFormat: "24 HR",
    timestampJoiner: " ",
    numberFormat: _.extend({ currencySymbol: "$" }, formatNumberDefaultOpts)
  };

  ns.Formatter = Formatter;
})(jsu);
// DEPENDENCY: underscore
// DEPENDENCY: utils/general
// DEPENDENCY: utils/regex-builder

(function(ns) {
  "use strict";

  function TextIndex(args) {
    this.properties = _.map(args.properties, TextProperty.parse);
    this.indices = [];
  }

  TextIndex.prototype.createIndex = function(obj) {
    return new ObjectIndex(this, obj);
  };

  TextIndex.prototype.indexObjects = function(objs, extend) {
    var self = this;
    var indices = _.map(objs, function(p) { return self.createIndexPair(p); });
    if (extend) {
      this.indices.push.apply(this.indices, indices)
    } else {
      this.indices = indices;
    }
  };

  TextIndex.prototype.removeFromIndex = function(obj) {
    var index = null;
    for (var i = 0; i < this.indices.length; ++i) {
      var pair = this.indices[i];
      if (pair.subject === obj) {
        index = i;
        break;
      }
    }
    if (index !== null) {
      this.indices.splice(index, 1);
    }
  };

  TextIndex.prototype.updateIndex = function(obj) {
    for (var i = 0; i < this.indices.length; ++i) {
      var pair = this.indices[i];
      if (pair.subject === obj) {
        pair.index.update(pair.subject);
        return;
      }
    }
    this.indices.push(this.createIndexPair(obj));
  };

  TextIndex.prototype.reindex = function() {
    _.each(this.indices, function(i) {
      i.index.update(i.subject);
    });
  };

  TextIndex.prototype.createIndexPair = function(obj) {
    return { subject: obj, index: this.createIndex(obj) };
  };

  TextIndex.prototype.filter = function(textSearches, autoWildcard) {
    // textSearches should be either an array of TextSearch objects (created manually or from
    // TextSearch.parse) or a string that will be sent to TextSearch.parse.

    if (_.isString(textSearches)) {
      textSearches = TextSearch.parse(textSearches, autoWildcard);
    }

    return _.map(
      _.filter(
        this.indices,
        function(pair) { return pair.index.test(textSearches); }
      ),
      function(p) { return p.subject; }
    );
  };


  function ObjectIndex(textIndexDefinition, obj) {
    this.textIndexDefinition = textIndexDefinition;
    this.index = {};
    this.allWords = [];

    if (obj) {
      this.update(obj);
    }
  }

  ObjectIndex.prototype.update = function(obj) {
    var props = this.textIndexDefinition.properties;
    this.index = {};
    for (var i = 0; i < props.length; ++i) {
      var property = props[i];
      var words = property.getWords(obj);
      if (words !== null) {
        this.index[property.identifier] = words;
      }
    }
    this.allWords = _.uniq(_.flatten(_.values(this.index)).sort(), true);
  }

  ObjectIndex.prototype.test = function(textSearches) {
    if (textSearches.length == 1 && textSearches[0].identifier == null) {
      return textSearches[0].test(this.allWords);
    } else {
      for (var tsIndex = 0; tsIndex < textSearches.length; ++tsIndex) {
        var ts = textSearches[tsIndex];

        if (!ts.identifier in this.index) {
          throw "Invalid identifier: " + ts.identifier;
        }

        var words = this.index[ts.identifier];

        if (!ts.test(words)) { return false; }
      }

      return true;
    }
  }


  function Property(name, unwrap) {
    // A property defines how to get a value from an object.

    this.name = name;
    this.unwrap = unwrap;
  }

  Property.prototype.getValue = function(obj) {
    var v = obj[this.name];
    return this.unwrap ? v() : v;
  }

  Property.parse = function(name) {
    if (name.slice(-2) == "()") {
      return new Property(name.slice(0, -2), true);
    } else {
      return new Property(name, false);
    }
  };


  function PropertyChain(properties) {
    // A PropertyChain defines how to get a (potentially) nested value from an object.
    // properties can be either:
    // - a function, which will be called directly on objects to get their value
    // - an array of Property objects.

    this.properties = properties;
  }

  PropertyChain.prototype.getValue = function(obj) {
    if (_.isFunction(this.properties)) {
      return this.properties(obj);
    } else {
      var co = obj;
      for (var i = 0; i < this.properties.length; ++i) {
        var property = this.properties[i];
        co = property.getValue(co);
        if (!jsu.util.isPresent(co)) {
          break;
        }
      }
      return co;
    }
  }

  PropertyChain.parse = function(name) {
    if (_.isFunction(name)) {
      return new PropertyChain(name);
    } else {
      return new PropertyChain(_.map(name.split("."), Property.parse));
    }
  }


  function TextProperty(identifier, propertyChain) {
    // A TextProperty is an identifier and propertyChain pair, defining what name particular values
    // are being indexed under, and how to get the data for that index.

    this.identifier = identifier.toLowerCase();
    this.propertyChain = propertyChain;
  }

  TextProperty.prototype.getWords = function(obj) {
    var v = this.propertyChain.getValue(obj);

    if (v === undefined || v === null) {
      return null;
    }

    if (_.isArray(v)) {
      return _.uniq(_.flatten(_.map(v, normalizeText)).sort(), true);
    } else {
      return normalizeText(v, true);
    }
  }

  TextProperty.parse = function(def) {
    // def can be either an array of:
    // - [pc, id]
    //   where pc is a valid argument for PropertyChain.parse and id is the index identifier
    // - propertyPath
    //   where propertyPath is a valid string for PropertyChain.parse, and identifier will be the
    //   the name of propertyChain.properties[0]. This makes it simple to index a property on a
    //   model by just giving it's name. E.G. index model.description by just passing in
    //   'description'

    var identifier;
    var propertyChain;

    if (_.isArray(def)) {
      propertyChain = PropertyChain.parse(def[0]);
      identifier = def[1];
    } else {
      propertyChain = PropertyChain.parse(def);
      identifier = propertyChain.properties[0].name;
    }

    return new TextProperty(identifier, propertyChain);
  }


  function TextSearch(identifier, words, mode) {
    // A TextSearch defines how to search a text index.
    // - identifier: if given, the specific field to search on
    // - words: an array of strings or regexes to search with
    // - mode: any or all. How many of the words must match.
    // TODO: Change mode to be a number? <= 0 means all, positive integer is count? Can't be more
    //       than length of words?

    this.identifier = identifier;
    this.words = words;
    this.mode = mode || "all";
  }

  TextSearch.prototype.test = function(words) {
    if (!words || words.length == 0) { return false; }

    if (this.mode == "all") {
      for (var i = 0; i < this.words.length; ++i) {
        if (!TextSearch.contains(words, this.words[i])) {
          return false;
        }
      }
      return true;
    } else {
      for (var i = 0; i < this.words.length; ++i) {
        if (TextSearch.contains(words, this.words[i])) {
          return true;
        }
      }
      return false;
    }
  }

  TextSearch.parse = function(def, autoWildcard) {
    // Parse a string into one or more TextSearch objects. If the string is of the form:
    // identifier (: or =) words ... where identifier contains no spaces, there's optional spaces
    // around the method (: or =), and words is one or more space-separated word. The operators
    // determine the search mode. = means all words must match, : means any words can match.
    // If the def string does not match that general pattern, then a single TextSearch object is
    // created with no identifier, and a mode of 'all'.

    var textSearches = [];
    var match;

    while (match = paramSearchRe.exec(def)) {
      var identifier = match[1].toLowerCase();
      var mode = match[2] == "=" ? "all" : "any";
      var words = TextSearch.textToWords(match[3], autoWildcard);
      textSearches.push(new TextSearch(identifier, words, mode));
    }

    if (textSearches.length == 0) {
      textSearches.push(new TextSearch(null, TextSearch.textToWords(def, autoWildcard), "all"));
    }

    return textSearches;
  }

  TextSearch.textToWords = function(words, autoWildcard) {
    var normalizedWords = normalizeText(words, true);

    if (autoWildcard) {
      normalizedWords = _.map(normalizedWords, function(w) { return "*" + w + "*"; });
    }

    return _.map(normalizedWords, TextSearch.textToWord);
  }

  TextSearch.textToWord = function(word) {
    return word.indexOf("*") == -1 ? word : getWildcardRegex(word);
  }

  TextSearch.contains = function(words, word) {
    if (word instanceof RegExp) {
      for (var i = 0; i < words.length; ++i) {
        if (word.test(words[i])) {
          return true;
        }
      }
      return false;
    } else {
      return _.indexOf(words, word, true) != -1;
    }
  }


  function normalizeText(t, uniq) {
    var rawWords = textContentsRe.exec(t.toString())[0].split(textSplitRe);
    var words = _.filter(rawWords, normalizeWord);

    if (uniq) {
      return _.uniq(words.sort(), true);
    }
    return words;
  }

  function normalizeWord(w) {
    return w.toLowerCase().replace(removeFromWordsRe, "");
  }

  function getWildcardRegex(word) {
    return new RegExp("^" + jsu.util.getRegexSafeText(word, "*").replace(/\*+/g, ".*") + "$", "i");
  }

  var paramSearchRe =  /(\w+)\s*(=|\:)(.*?)(?=(?:\s*\w+\s*(?:=|\:))|$)/ig;
  var textContentsRe = /^\s*(.*?)\s*$/;
  var textSplitRe = /[\s\/\-&]+/g;
  var removeFromWordsRe = /['"]|^[,\.\(\)]|[,\.\(\)]$/g;

  // Export
  ns.TextIndex = TextIndex;
  ns.TextSearch = TextSearch;
})(jsu);
(function(ns) {
  "use strict";

  // nodeType is one of:
  // "i": only matches when event.target is an input type
  // "o": only matches when event.target is an output type (any non-input type)
  // "a": (default) doesn't check nodeType
  function KeyChecker(keyDef, nodeType) {
    this.keyDef = keyDef;
    this.parsedKeyDef = KeyChecker.parseKeyDefinition(keyDef);
    this.nodeType = nodeType || "a";
  }

  KeyChecker.prototype.matches = function(e) {
    return nodeTypeMatches(this.nodeType, e.target.nodeName, e.target) &&
      KeyChecker.matches(this.parsedKeyDef, e);
  }

  KeyChecker.parseKeyDefinition = function(keyDef) {
    var groups = keyDef.split("+");
    for (var i = 0; i < groups.length; ++i) {
      groups[i] = groups[i].split("|");
    }
    return groups;
  }

  KeyChecker.matchesKeyDefinition = function(kdef, e) {
    return KeyChecker.matches(KeyChecker.parseKeyDefinition(kdef), e);
  }

  KeyChecker.matches = function(pkdef, e) {
    var allMatch = false;

    for (var gi = 0; gi < pkdef.length; ++gi) {
      var group = pkdef[gi];
      var groupMatches = false;
      for (var ci = 0; ci < group.length; ++ci) {
        if (groupMatches) { break; }
        switch (group[ci]) {
          case "shift":
            groupMatches = e.shiftKey;
            break;
          case "control":
            groupMatches = e.ctrlKey;
            break;
          case "meta":
            groupMatches = e.metaKey;
            break;
          case "alt":
            groupMatches = e.altKey;
            break;
          default:
            groupMatches = (e.keyCode === keyMap[group[ci]]);
            break;
        }
      }
      if (!groupMatches) {
        return false;
      }
    }

    return true;
  }

  function nodeTypeMatches(typeCode, nodeName, node) {
    if (typeCode == "a") {
      return true;
    } else if (typeCode == "i") {
      return isInput(nodeName, node);
    } else {
      return !isInput(nodeName, node);
    }
  }

  function isInput(nodeName, node) {
    switch(nodeName) {
      case "INPUT":
      case "SELECT":
      case "TEXTAREA":
      case "BUTTON":
      case "OPTGROUP":
      case "OPTION":
      // case "DATALIST":
      // case "KEYGEN":
      // case "OUTPUT":
        return true;
      default:
        return node.contentEditable === "true";
    }
  }

  var keyMap = {
    "backspace": 8,
    "tab": 9,
    "enter": 13,
    "shift": 16,
    "control": 17,
    "alt": 18,
    "capslock-on": 20,
    "escape": 27,
    "space": 32,
    "left": 37,
    "up": 38,
    "right": 39,
    "down": 40,
    "0": 48,
    "1": 49,
    "2": 50,
    "3": 51,
    "4": 52,
    "5": 53,
    "6": 54,
    "7": 55,
    "8": 56,
    "9": 57,
    "a": 65,
    "b": 66,
    "c": 67,
    "d": 68,
    "e": 69,
    "f": 70,
    "g": 71,
    "h": 72,
    "i": 73,
    "j": 74,
    "k": 75,
    "l": 76,
    "m": 77,
    "n": 78,
    "o": 79,
    "p": 80,
    "q": 81,
    "r": 82,
    "s": 83,
    "t": 84,
    "u": 85,
    "v": 86,
    "w": 87,
    "x": 88,
    "y": 89,
    "z": 90,
    "meta-left": 91,
    "meta-right": 93,
    "f1": 112,
    "f2": 113,
    "f3": 114,
    "f4": 115,
    "f5": 116,
    "f6": 117,
    "f7": 118,
    "f8": 119,
    "f9": 120,
    "f10": 121,
    "f11": 122,
    "f12": 123,
    ";": 186,
    "+": 187,
    ",": 188,
    "-": 189,
    ".": 190,
    "/": 191,
    "`": 192,
    "[": 219,
    "\\": 220,
    "]": 221,
    "'": 222
  };

  ns.KeyChecker = KeyChecker;
})(jsu);
// DEPENDENCY: underscore
// DEPENDENCY: jQuery

(function(ns) {
  var webkitPermissions = ["granted", "default", "denied", "unavailable"];

  // This class provides a common interface to webkit notifications and
  // standardized notifications. It also fails silently in browsers without
  // notifications
  function Notification(title, opts) {
    if (Notification.permission() != "granted") { return; }
    opts = _.extend({}, { icon: "", body: "", tag: "" }, opts);

    if (window.webkitNotifications) {
      var notification = window.webkitNotifications.createNotification(opts.icon, title, opts.body);
      notification.show();
    } else if (window.Notification) {
      new window.Notification(title, opts);
    }
  }

  Notification.permission = function() {
    if (window.webkitNotifications) {
      return webkitPermissions[window.webkitNotifications.checkPermission()];
    } else if (window.Notification) {
      return window.Notification.permission;
    } else {
      return "unavailable";
    }
  }

  Notification.requestPermission = function(callback) {
    if (Notification.permission() == "default") {
      if (window.webkitNotifications) {
        window.webkitNotifications.requestPermission(callback);
      } else if (window.Notification) {
        window.Notification.requestPermission(callback);
      } else {
        callback();
      }
    }
  }

  ns.Notification = Notification;


  // This class regularly checks for notifications at a given API endpoint, and
  // shows any that are found. This class will do nothing if notifications
  // aren't available, or permission is not granted.
  function NotificationChecker(args) {
    var self = this;

    self.getUri = args.getUri;
    self.delay = args.delay || 60 * 1000;
    self.immediate = args.immediate === false ? false : true;
    self.successCallback = args.successCallback;
    self.icon = args.icon || undefined;

    if (Notification.permission() != "granted") {
      return;
    }

    if (self.immediate) {
      self.checkForNotifications();
    }

    self.intervalHandle = setInterval(function() { self.checkForNotifications() }, self.delay);
  }

  NotificationChecker.prototype.checkForNotifications = function() {
    var self = this;

    $.ajax(self.getUri(), {
      success: function(data) {
        self.showNotification(_.map(data.notifications, _.identity));
        if (self.successCallback) { self.successCallback(data); }
      },
      error: function(xhr, status, error) {
        clearInterval(self.intervalHandle);
      }
    });
  };

  NotificationChecker.prototype.showNotification = function(nlist) {
    if (!(nlist && nlist.length)) { return; }

    var self = this;
    var n = nlist.shift();

    NotificationChecker.showNotification(n.subject, { body: n.body, icon: self.icon }, function() { self.showNotification(nlist); });
  };

  NotificationChecker.showNotification = function(subject, opts, callback) {
    new Notification(subject, opts);
    if (callback) { callback(); }
  };

  // Export
  ns.Notification = Notification;
  ns.NotificationChecker = NotificationChecker;
})(jsu);
(function(ns) {
  "use strict";

  function Ratio(n, d) {
    var self = this;

    self.numerator = n || 0;
    self.denominator = d || 0;
  }

  Ratio.prototype.straightAdd = function(ratio) {
    var self = this;

    self.numerator += ratio.numerator;
    self.denominator += ratio.denominator;

    return self;
  }

  Ratio.prototype.valueOf = function() { return this.denominator ? this.numerator / this.denominator : null; }

  Ratio.prototype.toString = function(fmt) {
    if (!fmt || fmt == "/") {
      return this.numerator + " / " + this.denominator;
    } else if (fmt[0] == "%") {
      var r = this.numerator / this.denominator * 100;

      if (fmt.length > 1) {
        var f = parseInt(fmt.substring(1)) || 0;
        return r.toFixed(f) + "%";
      } else {
        return r.toString() + "%";
      }
    }
  }

  Ratio.copy = function(r) { return new Ratio(r.numerator, r.denominator); }

  Ratio.straightAdd = function(a, b) { return Ratio.copy(a).add(b); }

  // Export
  ns.Ratio = Ratio;
})(jsu);
// DEPENDENCY: underscore

(function(ns) {
  "use strict";

  function getX(p) { return p[0]; }
  function getY(p) { return p[1]; }

  function sum(data, extract) {
    extract = extract || _.identity;

    return _.reduce(data, function(t, d) { return t + extract(d); }, 0);
  }

  function average(data, extract, sum_) {
    if (sum_ === undefined) {
      sum_ = sum(data, extract);
    }
    var avg = sum_ / data.length;
    return avg;
  }

  function varianceSum(data, extract, avg) {
    if (avg === undefined) {
      avg = average(data, extract);
    }
    return sum(_.map(data, function(d) { return Math.pow(extract(d) - avg, 2); }), _.identity);
  }

  function variancePopulation(data, extract, avg) {
    return varianceSum(data, extract, avg) / data.length;
  }

  function varianceSample(data, extract, avg) {
    return varianceSum(data, extract, avg) / (data.length - 1);
  }

  function standardDeviationPopulation(data, extract, avg) {
    return Math.sqrt(variancePopulation(data, extract, avg));
  }

  function standardDeviationSample(data, extract, avg) {
    return Math.sqrt(varianceSample(data, extract, avg));
  }

  function linearRegression(data, getX, getY) {
    var sumX = sum(data, getX);
    var sumY = sum(data, getY);
    var sumXY = sum(data, function(p) { return getX(p) * getY(p); });
    var sumX2 = sum(data, function(p) { return Math.pow(getX(p), 2); });
    var n = data.length;
    var slope = (n * sumXY - sumX * sumY) / (n * sumX2 - Math.pow(sumX, 2));
    var intercept = (sumY - slope * sumX) / n;

    return [intercept, slope];
  }

  // TODO: This should probably be moved out to applications that need it.
  // function rollingAveragePoints(points, width, nullValue) {
  //   if (nullValue === undefined) { nullValue = null; }
  //
  //   var newPoints = [];
  //   var avgGroup = [];
  //
  //   for (var i = 0; i < points.length; ++i) {
  //     avgGroup.push(points[i]);
  //     var y = nullValue;
  //     if (i >= width) {
  //       y = average(avgGroup, 1);
  //       avgGroup.shift();
  //     }
  //     newPoints.push([points[i][0], y]);
  //   }
  //
  //   return newPoints;
  // }

  // TODO: getX / getY?
  function getRollingAveragePoints(points, width, nullValue, type) {
    if (nullValue === undefined) { nullValue = null; }
    if (type === undefined) { type = "centered"; }

    var rollingAveragePoints = [];
    var lradius = null;
    var rradius = null;

    switch (type) {
    case "trailing":
      lradius = width - 1;
      rradius = 1;
      break;
    case "leading":
      lradius = 1;
      rradius = width - 1;
      break;
    case "centered":
    default:
      lradius = Math.floor(width / 2);
      rradius = lradius;
      break;
    }

    for (var i = 0; i < points.length; ++i) {
      var sum = 0;
      var count = 0;

      for (var j = Math.max(i - lradius, 0); j < Math.min(i + rradius, points.length); ++j) {
        var v = points[j][1];
        if (jsu.util.isPresent(v)) {
          sum += v;
          count += 1;
        }
      }

      rollingAveragePoints.push([points[i][0], count === 0 ? nullValue : sum / count]);
    }

    return rollingAveragePoints;
  }

  ns.stats = {
    getX: getX,
    getY: getY,
    sum: sum,
    average: average,
    varianceSum: varianceSum,
    variancePopulation: variancePopulation,
    varianceSample: varianceSample,
    standardDeviationPopulation: standardDeviationPopulation,
    standardDeviationSample: standardDeviationSample,
    linearRegression: linearRegression,
    getRollingAveragePoints: getRollingAveragePoints
  };
})(jsu);
// DEPENDENCY: jQuery
// DEPENDENCY: utils/general

(function(ns) {
  "use strict";

  function TableScrollManager(selector, args) {
    this.selector = selector;
    this.args = $.extend({}, TableScrollManager.defaultArgs, args);
    this.elem = null; //$(selector);
    this.lastWidth = null;
    this.lastHeight = null;
    this.lastLeft = null;
    this.lastTop = null;
    this.elements = null;
    this.hidden = false;

    this.resetElement();

    if (jsu.util.makeBroadcaster) {
      var tsm = this;
      jsu.util.makeBroadcaster(tsm);
      tsm.headerClicked = function(e) { TableScrollManager.headerClicked(tsm, e); }
    }

    if (this.args.defaultOn) {
      this.turnOn();
    }
  }

  TableScrollManager.prototype.resetElement = function() {
    this.elem = $(this.selector);
    return this.elem;
  }

  TableScrollManager.headerClicked = function(tableScrollManager, e) {
    if (e.isPropagationStopped()) { return; }
    if (!(e.target && e.target.nodeName == "TH")) { return; }
    var div = getAncestor(e.target, 4);
    if (!div) { return; }
    if (div.className.indexOf(tableScrollManager.args.horizontalTableContainerClass) >= 0) {
      var coords = TableResizer.getCellCoords(e.target);
      var ofst = 0;
      if (tableScrollManager.args.firstCol) {
        ofst = 1;
      }
      tableScrollManager.trigger("columnHeaderClicked", { eventObject: e, column: coords.x - ofst });
    } else if (div.className.indexOf(tableScrollManager.args.verticalTableContainerClass) >= 0) {
      var coords = TableResizer.getCellCoords(e.target);
      tableScrollManager.trigger("rowHeaderClicked", { eventObject: e, row: coords.y });
    } else if (div.className.indexOf(tableScrollManager.args.coverTableContainerClass) >= 0) {
      tableScrollManager.trigger("allHeadersClicked", { eventObject: e });
    }
  }

  TableScrollManager.prototype.getDimensions = function() {
    var o = this.elem.offset();

    return {
      width: this.elem.width(),
      height: this.elem.height(),
      top: o ? o.top : 0,
      left: o ? o.left : 0
    };
  }

  TableScrollManager.prototype.dimensionsChanged = function(d) {
    d = d || this.getDimensions();

    return this.lastWidth != d.width || this.lastHeight != d.height || this.lastTop != d.top || this.lastLeft != d.left;
  }

  TableScrollManager.prototype.updateDimensions = function(d) {
    d = d || this.getDimensions();

    this.lastWidth = d.width;
    this.lastHeight = d.height;
    this.lastLeft = d.left;
    this.lastTop = d.top;
  }

  TableScrollManager.prototype.update = function(force, opts) {
    var d = this.getDimensions();

    if (force || this.dimensionsChanged(d)) {
      this.turnOff();
      this.resetElement();
      this.elements = TableScrollManager.makeTableScrollable(this.elem, this.args, opts);
      var tsm = this;
      this.each(function(e) { e.on("mouseup", tsm.headerClicked); });
      this.updateDimensions(d);
      this.hidden = false;
    }
  }

  TableScrollManager.prototype.isOn = function() { return this.elements !== null; }

  TableScrollManager.prototype.turnOn = function() { this.update(true); }

  TableScrollManager.prototype.turnOff = function() {
    if (this.isOn()) {
      var tsm = this;
      this.each(function(e) {
        $(e).off("click", tsm.headerClicked);
        $(e).remove(); });
      this.elements = null;
    }
  }

  TableScrollManager.prototype.toggle = function() {
    if (this.isOn()) {
      this.turnOff();
    } else {
      this.turnOn();
    }
  }

  TableScrollManager.prototype.hide = function() {
    if (this.isOn() && !this.hidden) {
      this.each(function(e) { $(e).css("visibility", "hidden"); });
      this.hidden = true;
    }
  }

  TableScrollManager.prototype.show = function() {
    if (this.isOn() && this.hidden) {
      this.each(function(e) { $(e).css("visibility", "visible"); });
      this.hidden = false;
    }
  }

  TableScrollManager.prototype.toggleVisibility = function() {
    if (this.hidden) {
      this.show();
    } else {
      this.hide();
    }
  }

  TableScrollManager.prototype.each = function(a) {
    for (var k in this.elements) {
      a(this.elements[k], k);
    }
  }

  TableScrollManager.prototype.getColumnWidths = function() {
    if (this.elements.firstRowContainer) {
      var ths = $(this.elements.firstRowContainer).find("thead th");
      var widths = [];
      for (var i = 0; i < ths.length; ++i) {
        widths.push($(ths[i]).css("width"));
      }
      return widths;
    }
    return null;
  }

  // TableScrollManager.prototype.setColumnWidths = function(columnWidths) {
  //   if (this.elements.firstRowContainer) {
  //     var headerThs = $(this.elements.firstRowContainer).find("thead th");
  //     var bodyThs = $(this.elem).find("thead th");
  //     for (var i = 0; i < headerThs.length && i < columnWidths.length; ++i) {
  //       if (!columnWidths[i]) { continue; }
  //       TableScrollManager.setSize(headerThs[i], "width", columnWidths[i]);
  //     }
  //     TableScrollManager.matchSizes(headerThs, bodyThs, this.args.boxSizing, TableScrollManager.dimension.width);
  //   }
  // }

  TableScrollManager.makeTableScrollable = function(selector, args, opts) {
    args = $.extend({}, TableScrollManager.makeTableScrollable.defaultArgs, args);

    var $sel = $(selector);
    var tbl = $sel.children("table");
    var firstRowContainer = null;
    var firstColContainer = null;
    var coverContainer = null;
    var o = $sel.offset();
    var elements = {};
    var pos, top, left, append;

    if (!o) { return; }

    if (args.attachToBody) {
      pos = "fixed";
      top = o.top + "px";
      left = o.left + "px";
      append = "body";
    } else {
      pos = "absolute";
      top = "0px";
      left = "0px";
      append = $sel.parent();
    }


    if (args.firstRow) {
      var firstRow = TableScrollManager.copyFirstRow(tbl);

      firstRowContainer = $("<div></div>").css({
        // "position": "fixed",
        // "top": o.top + "px",
        // "left": o.left + "px",
        "position": pos,
        "top": top,
        "left": left,
        "width": $sel.width() + "px",
        "overflow-x": "hidden"
      })
        .addClass(args.horizontalTableContainerClass)
        .append(firstRow)
        //.appendTo("body");
        .appendTo(append);
      elements.firstRowContainer = firstRowContainer;
    }

    if (args.firstCol) {
      var firstCol = TableScrollManager.copyFirstCol(tbl);
      firstColContainer = $("<div></div>").css({
          // "position": "fixed",
          // "top": o.top + "px",
          // "left": o.left + "px",
          "position": pos,
          "top": top,
          "left": left,
          "height": $sel.height() + "px",
          "overflow-y": "hidden"
        })
          .addClass(args.verticalTableContainerClass)
          .append(firstCol)
          //.appendTo("body");
          .appendTo(append);
      elements.firstColContainer = firstColContainer;
    }

    if (args.firstCol && args.firstRow) {
      var cover = TableScrollManager.getCover(tbl);
      coverContainer = $("<div></div>").css({
        // "position": "fixed",
        // "top": o.top + "px",
        // "left": o.left + "px"
        "position": pos,
        "top": top,
        "left": left,
      })
        .addClass(args.coverTableContainerClass)
        .append(cover)
        //.appendTo("body");
        .appendTo(append);
      elements.coverContainer = coverContainer;
    }

    TableScrollManager.sync($sel, firstRowContainer, firstColContainer, coverContainer, args, opts);

    if (args.resizable) {
      if (args.firstRow) {
        new TableResizer(firstRowContainer, tbl, TableResizer.direction.horizontal, args);
      }
      if (args.firstCol) {
        new TableResizer(firstColContainer, tbl, TableResizer.direction.vertical, args);
      }
    }

    return elements;
  }

  TableScrollManager.makeTableScrollable.defaultArgs = {
    firstRow: true,
    firstCol: true,
    horizontalTableContainerClass: "horizontal-scroll-container",
    verticalTableContainerClass: "vertical-scroll-container",
    coverTableContainerClass: "cover-container",
    boxSizing: "border",
    resizable: true,
    attachToBody: true
  };

  TableScrollManager.matchScroll = function(orig, horiz, verti) {
    if (horiz) { TableScrollManager.matchProp(orig, horiz, "scrollLeft"); }
    if (verti) { TableScrollManager.matchProp(orig, verti, "scrollTop"); }
  }

  TableScrollManager.sync = function(orig, horiz, verti, cover, args, opts) {
    var ignoreOrig = false;
    var ignoreProxies = false;

    TableScrollManager.matchScroll(orig, horiz, verti);

    orig.on("scroll", function(e) {
      if (ignoreOrig) { return; }

      ignoreProxies = true;
      TableScrollManager.matchScroll(orig, horiz, verti);
      ignoreProxies = false;
    });

    if (horiz) {
      horiz.on("scroll", function(e) {
        if (ignoreProxies) { return; }

        ignoreOrig = true;
        TableScrollManager.matchProp(horiz, orig, "scrollLeft");
        ignoreOrig = false;
      });

      var ofr = orig.find("thead th");
      var pfr = horiz.find("thead th");

      if (opts && opts.columnWidths) {
        var cw = opts.columnWidths;
        for (var i = 0; i < ofr.length && i < cw.length; ++i) {
          if (!cw[i]) { continue; }
          TableScrollManager.setSize(ofr[i], "width", cw[i]);
        }
      }

      TableScrollManager.matchSizes(ofr, pfr, args.boxSizing, TableScrollManager.dimension.both);
    }

    if (verti) {
      verti.on("scroll", function(e) {
        if (ignoreProxies) { return; }

        ignoreOrig = true;
        TableScrollManager.matchProp(verti, orig, "scrollTop");
        ignoreOrig = false;
      });

      var ohc = orig.find("thead tr:first-child th:first-child")[0];
      var phc = verti.find("thead tr:first-child th:first-child")[0];

      TableScrollManager.matchSize(ohc, phc, args.boxSizing, TableScrollManager.dimension.both);

      var ofc = orig.find("tbody th");
      var pfc = verti.find("tbody th");

      TableScrollManager.matchSizes(ofc, pfc, args.boxSizing, TableScrollManager.dimension.both);
    }

    if (cover) {
      var ohc = orig.find("thead tr:first-child th:first-child")[0];
      var phc = cover.find("thead tr:first-child th:first-child")[0];

      TableScrollManager.matchSize(ohc, phc, args.boxSizing, TableScrollManager.dimension.both);
    }
  }

  TableScrollManager.matchSizes = function(from, to, sizing, dim, offset) {
    for (var i = 0; i < from.length; ++i) {
      TableScrollManager.matchSize(from[i], to[i], sizing, dim, offset);
    }
  }

  TableScrollManager.setSize = function(elem, dim, v) {
    var css = {};
    css["min-" + dim] = v;
    css["max-" + dim] = v;
    css[dim] = v;

    $(elem).css(css);
  }

  TableScrollManager.matchSize = function(from, to, sizing, dim, offset) {
    dim = dim || TableScrollManager.dimension.both;
    offset = offset || {};

    if ((dim & TableScrollManager.dimension.height) != 0) {
      var h = TableScrollManager.getSize(from, "height", sizing, offset) + "px";
      TableScrollManager.setSize(to, "height", h);
    }

    if ((dim & TableScrollManager.dimension.width) != 0) {
      var w = TableScrollManager.getSize(from, "width", sizing, offset) + "px";
      TableScrollManager.setSize(to, "width", w);
    }
  }

  TableScrollManager.getSize = function(elem, dim, sizing, offset) {
    if (dim == "width") {
      if (sizing == "border") {
        return elem.offsetWidth + (offset.width || 0);
      } else {
        return $(elem).width() + (offset.width || 0);
      }
    } else {
      if (sizing == "border") {
        return elem.offsetHeight + (offset.height || 0);
      } else {
        return $(elem).height() + (offset.height || 0);
      }
    }
  }

  TableScrollManager.matchProp = function(from, to, fprop, tprop) {
    tprop = tprop || fprop;
    if (to[tprop]() != from[fprop]()) {
      to[tprop](from[fprop]());
    }
  }

  TableScrollManager.copyFirstRow = function(tbl) {
    var $tbl = $(tbl);

    return $("<table></table>")
      .attr("class", $tbl.attr("class"))
      .css("width", $tbl.width())
      .append($tbl.children("thead").clone());
  }

  TableScrollManager.copyFirstCol = function(tbl) {
    var $tbl = $(tbl);
    var thead = $("<thead></thead>").append($("<tr></tr>").append(
      $tbl.find("thead tr:first-child th:first-child").clone()));
    var tbody = $("<tbody></tbody>");

    $tbl.children("tbody").children("tr").each(function() {
      tbody.append($("<tr></tr>").append($(this).children("th").clone()));
    });

    return $("<table></table>")
      .attr("class", $tbl.attr("class"))
      .append(thead)
      .append(tbody);
  }

  TableScrollManager.getCover = function(tbl) {
    var $tbl = $(tbl);
    var thead = $("<thead></thead>").append($("<tr></tr>").append($tbl.find("Thead tr:first-child th:first-child").clone()));
    return $("<table></table>")
      .attr("class", $tbl.attr("class"))
      .append(thead);
  }

  TableScrollManager.defaultArgs = $.extend({}, TableScrollManager.makeTableScrollable.defaultArgs, {
    defaultOn: true
  });

  TableScrollManager.dimension = {
    none: 0,
    width: 1,
    height: 2,
    both: 3
  };

  function getAncestor(e, n) {
    n = Math.abs(parseInt(n) || 1);
    var i = 0;
    var c = e;
    while (c && i++ < n) {
      c = c.parentNode;
    }
    return c;
  }



  function TableResizer(container, target, direction, opts) {
    var self = this;

    this.opts = $.extend({}, TableResizer.defaultOpts, opts);
    this.container = $(container);
    this.target = $(target);
    this.direction = direction;
    this.zone = TableResizer.direction.none;
    this.isMouseDown = false;
    this.lastCoords = null;
    this.resizeTarget = null;
    this.hasResized = false;

    this.container.on("mousedown", function(e) { self.onMouseDown(e); });
    this.container.on("mouseup", function(e) { self.onMouseUp(e); });
    this.container.on("mousemove", function(e) { self.onMouseMove(e); });

    if (!TableResizer.on && jsu.util.makeBroadcaster) { jsu.util.makeBroadcaster(TableResizer); }
    if (this.direction != TableResizer.direction.both && this.opts.maintainOppositeAxis) {
      TableResizer.on("resized", function(name, args, sender) { self.otherDirectionResized(args); });
    }
  }

  TableResizer.prototype.onMouseDown = function(e) {
    this.isMouseDown = true;
    this.lastCoords = { x: e.pageX, y: e.pageY };
    this.hasResized = false;
  }

  TableResizer.prototype.onMouseUp = function(e) {
    this.isMouseDown = false;
    this.lastCoords = null;
    if (this.hasResized) {
      this.hasResized = false;
      e.stopPropagation();
      return false;
    }
  }

  TableResizer.prototype.onMouseMove = function(e) {
    if (this.isMouseDown) {
      var coords = { x: e.pageX, y: e.pageY };
      var cellCoords = TableResizer.getCellCoords(this.resizeTarget[0]);
      var proxy = this.resizeTarget[0];
      var targetCell = this.target && TableResizer.getCell(this.target.children("tbody")[0], cellCoords);
      var offset = { width: coords.x - this.lastCoords.x, height: coords.y - this.lastCoords.y };
      var resizedOn = TableResizer.direction.none;

      if ((this.direction & TableResizer.direction.horizontal) != 0 && coords.x != this.lastCoords.x) {
        TableScrollManager.matchSize(proxy, proxy, this.opts.boxSizing, TableScrollManager.dimension.width, offset);
        if (targetCell) {
          TableScrollManager.matchSize(proxy, targetCell, this.opts.boxSizing, TableScrollManager.dimension.width);
        }
        resizedOn = resizedOn | TableResizer.direction.horizontal;
      }

      if ((this.direction & TableResizer.direction.vertical) != 0 && coords.y != this.lastCoords.y) {
        TableScrollManager.matchSize(proxy, proxy, this.opts.boxSizing, TableScrollManager.dimension.height, offset);
        if (targetCell) {
          TableScrollManager.matchSize(proxy, targetCell, this.opts.boxSizing, TableScrollManager.dimension.height);
        }
        resizedOn = resizedOn | TableResizer.direction.vertical;
      }

      this.lastCoords = coords;
      if (resizedOn != TableResizer.direction.none && TableResizer.trigger) {
        this.hasResized = true;
        TableResizer.trigger("resized", { direction: resizedOn });
      }
    } else {
      if (e.target.nodeName != "TH") { return; }

      var lastZone = this.zone;
      this.zone = this.direction & this.getActiveZone(e);
      this.resizeTarget = $(e.target);

      if (lastZone != this.zone) {
        this.setCursor();
      }
    }
  }

  TableResizer.prototype.setCursor = function() {
    var cursor = null;

    switch(this.zone) {
      case TableResizer.direction.none:
        cursor = "auto";
        break;
      case TableResizer.direction.vertical:
        cursor = "ns-resize";
        break;
      case TableResizer.direction.horizontal:
        cursor = "ew-resize";
        break;
      case TableResizer.direction.both:
        cursor = "nwse-resize";
        break;
    }

    this.container.css({ "cursor": cursor });
  }

  TableResizer.prototype.getActiveZone = function(e) {
    var zone = TableResizer.direction.none;
    var box = TableResizer.getBoundingBox(e.target);

    if (e.pageX >= box.left + box.width - this.opts.zoneSize) {
      zone = zone | TableResizer.direction.horizontal;
    }

    if (e.pageY >= box.top + box.height - this.opts.zoneSize) {
      zone = zone | TableResizer.direction.vertical;
    }

    return zone;
  }

  TableResizer.prototype.otherDirectionResized = function(args) {
    if ((args.direction & this.direction) == 0) {
      var selector;
      var dim;
      if (this.direction == TableResizer.direction.horizontal) {
        selector = "thead th";
        dim = TableScrollManager.dimension.width;
      } else {
        selector = "tbody th";
        dim = TableScrollManager.dimension.height;
      }
      var from = this.target.find(selector);
      var to = this.container.find(selector);
      TableScrollManager.matchSizes(from, to, "border", dim);
    }
  }

  TableResizer.getBoundingBox = function(elem) {
    var o = $(elem).offset();

    return {
      left: o.left,
      top: o.top,
      width: elem.offsetWidth,
      height: elem.offsetHeight
    };
  }

  TableResizer.getCellCoords = function(c) {
    var x = 0;
    var y = 0;

    while (c.previousSibling) {
      c = c.previousSibling;
      if (c.nodeName == "TD" || c.nodeName == "TH") {
        x += 1;
      }
    }

    c = c.parentNode;

    while (c.previousSibling) {
      c = c.previousSibling;
      if (c.nodeName == "TR") {
        y += 1;
      }
    }

    return { x: x, y: y };
  }

  TableResizer.getCell = function(cont, coords) {
    var x = 0;
    var y = 0;
    var tr = cont.firstChild;

    while (tr) {
      if (tr.nodeName == "TR") {
        if (y++ == coords.y) {
          break;
        }
      }
      tr = tr.nextSibling;
    }

    if (!tr) { return null; }

    var cl = tr.firstChild;

    while (cl) {
      if (cl.nodeName == "TD" || cl.nodeName == "TH") {
        if (x++ == coords.x) {
          break;
        }
      }
      cl = cl.nextSibling;
    }

    return cl;
  }

  TableResizer.defaultOpts = {
    zoneSize: 5,
    maintainOppositeAxis: true
  };

  TableResizer.direction = {
    none: 0,
    horizontal: 1,
    vertical: 2,
    both: 3
  };

  ns.TableScrollManager = TableScrollManager;
  ns.TableResizer = TableResizer;
})(jsu);
// DEPENDENCY: underscore
// DEPENDENCY: knockout

(function(ns) {
  "use strict";

  function TableSorter(columnDefs, opts) {
    opts = _.extend({}, tableSorterDefaultArgs, opts);
    var self = this;
    var sorts = [];

    self.sorts = ko.observable({});

    self.compare = function(l, r) {
      for (var i = 0; i < sorts.length; ++i) {
        var c = columnDefs[sorts[i].columnName].compare(l, r);
        if (c != 0) {
          return sorts[i].direction * c;
        }
      }
      return 0;
    }

    self.addSort = function(columnName) {
      if (!(columnName in columnDefs)) {
        throw "Unknown column name: " + columnName;
      }

      var direction = opts.firstDirection;

      for (var i = 0; i < sorts.length; ++i) {
        if (sorts[i].columnName == columnName) {
          direction = -sorts[i].direction;
          sorts.splice(i, 1);
          break;
        }
      }

      sorts.unshift({ columnName: columnName, direction: direction });

      if (sorts.length > opts.maxSorts) {
        sorts.pop();
      }

      var sortsObj = {};
      for (var i = 0; i < sorts.length; ++i) {
        sortsObj[sorts[i].columnName] = sorts[i].direction;
        if (!opts.showAllSorts) { break; }
      }
      self.sorts(sortsObj);
    }
  }

  var tableSorterDefaultArgs = {
    maxSorts: 3,
    firstDirection: 1,
    showAllSorts: false
  }

  // Export
  ns.TableSorter = TableSorter;
})(jsu);
(function(ns) {
  "use strict";

  function ViewModelBase(opts) {
    if (this === window || this === undefined) {
      throw new Error("Error creating object");
    }

    opts = _.extend({ }, defaultOpts, opts)

    this.private = {
      root: opts.root,
      parent: opts.parent,
      eaSubscriptions: [],
      koSubscriptions: [],
      eventHandlers: [],
      viewModelPropertyNames: []
    };

    this.isLoading = ko.observable("all");

    if (opts.root) {
      // If this is not the root, don't do anything.
    } else {
      var self = this;
      // var setup = _opts.setupRoot || defaultSetupRoot;
      // opts = _.extend({ }, rootDefaultOpts, opts);

      // setup(self, opts);
      self.private.root = self;
      self._eventAggregator = new jsu.EventAggregator();
      self._formatter = new jsu.Formatter();
      self._dataAccess = opts.dataAccess;
      jsu.ko.StatusMessage(self, opts.statusMessageTimeouts);
      self.viewportWidth = ko.observable(jsu.util.notifyOnWindowSizeChanged(function(nv, ov, sz) { root.viewportWidth(nv); }));
      self.windowOrientation = ko.observable(jsu.util.notifyOnWindowOrientationChanged(function(nv) { root.windowOrientation(nv); }));
    }
  }

  var defaultOpts = { };

  var rootDefaultOpts = {
    statusMessageTimeouts: { "success": 5000, "info": 5000 }
  };

  ViewModelBase.prototype.open = function(callback) {
    var self = this;
    this.isLoading("all");
    this.load(function(arg) {
      if (self.isLoading() === "all") {
        self.isLoading(null);
      }
      if (callback) {
        callback(arg);
      }
    });
  }

  ViewModelBase.prototype.close = function(callback, skipSave) {
    var self = this;

    function _callback(arg) {
      if (self.private.eaSubscriptions) {
        _.forEach(self.private.eaSubscriptions, function(k) { self.private.root._eventAggregator.unsubscribe(k); });
      }

      if (self.private.koSubscriptions) {
        _.forEach(self.private.koSubscriptions, function(s) { s.dispose(); });
      }

      if (self.private.eventHandlers) {
        _.forEach(self.private.eventHandlers, function(s) { $(s[0]).off(s[1], s[2]); });
      }

      if (self.private.viewModelPropertyNames) {
        _.forEach(self.private.viewModelPropertyNames, function(pn) { self.closeViewModel(pn, null, true); });
      }

      self.clearMessage();
      self.finalize();

      // self.private.root = null;
      // self.private.parent = null;

      if (callback) {
        callback(arg);
      }
    };

    if (this.hasChanges() && !skipSave) {
      this.save(_callback);
    } else {
      _callback(this);
    }
  }

  ViewModelBase.prototype.getViewModelOpts = function() {
    var opts = {
      root: this.private.root,
      parent: this
    };
    return $.extend(opts, this.getCustomViewModelOpts());
  }

  ViewModelBase.prototype.openViewModel = function(propertyName, viewModelConstructor, callback, opts, create) {
    if (!propertyName) {
      var vm = new viewModelConstructor($.extend(this.getViewModelOpts(), opts));
      vm.open(callback);
      return vm;
    } else {
      if (!this[propertyName] && create) {
        this[propertyName] = ko.observable(null);
      }
      if (!this[propertyName]()) {
        this[propertyName](new viewModelConstructor($.extend(this.getViewModelOpts(), opts)));
        this.private.viewModelPropertyNames.push(propertyName);
      }
      this[propertyName]().open(callback);
      return this[propertyName]();
    }
  }

  ViewModelBase.prototype.closeViewModel = function(propertyName, callback, skipSave) {
    if (this[propertyName]()) {
      this[propertyName]().close(callback, skipSave);
      this[propertyName](null);
    }
  }

  ViewModelBase.prototype.koSubscribe = function(kob, fx) {
    this.private.koSubscriptions.push(kob.subscribe(fx));
  }

  ViewModelBase.prototype.eaSubscribe = function() {
    this.private.eaSubscriptions.push(this.private.root._eventAggregator.subscribe.apply(this.private.root._eventAggregator, arguments));
  }

  ViewModelBase.prototype.registerEventHandler = function(element, eventName, handler) {
    $(element).on(eventName, handler);
    this.private.eventHandlers.push([element, eventName, handler]);
  }

  ViewModelBase.prototype.broadcast = function(message, data) {
    this.private.root._eventAggregator.broadcast(this, message, data);
  }

  ViewModelBase.prototype.api = function(method, args) {
    var da = this.private.root._dataAccess;
    if (!(method in da)) {
      throw new Error(jsu.Formatter.string("API method {0} not found", method));
    }
    return da[method](args);
  }

  ViewModelBase.prototype.eventAggregator = function() { return this.private.root._eventAggregator; }

  ViewModelBase.prototype.formatter = function() { return this.private.root._formatter; }

  ViewModelBase.prototype.setMessage = function(type, message, timeout) { return this.private.root.setMessage(type, message, timeout); }

  ViewModelBase.prototype.clearMessage = function() { return this.private.root.clearMessage(); }

  // NOTE: These methods are expected to be overridden in most cases.
  ViewModelBase.prototype.hasChanges = function() { return false; }
  ViewModelBase.prototype.isActive = function() { return true; }
  ViewModelBase.prototype.save = function(callback) {
    if (callback) {
      callback(this);
    }
  }
  ViewModelBase.prototype.load = function(callback) {
    if (callback) {
      callback(this);
    }
  }
  ViewModelBase.prototype.finalize = function() { }
  ViewModelBase.prototype.getCustomViewModelOpts = function() { return { }; }

  // Export
  // ns.generateViewModelBase = generateViewModelBase;
  ns.ViewModelBase = ViewModelBase;
})(jsu);
// Setup utility name spaces

jsu.util = jsu.util || {};
// A group of helpful array functions. Many of these are designed to work transparently with
// ko.ObservableArray or native arrays.

(function(ns) {
  "use strict";

  function indexOf(array, item) {
    // Allows us to use ko observable arrays.
    if ("indexOf" in array) {
      return array.indexOf(item);
    } else {
      return _.indexOf(array, item);
    }
  }

  function arrayLength(array) {
    // Allows us to use ko observable arrays
    return _.isFunction(array) ? array().length : array.length;
  }

  function arrayInsert(array, item, pivot, where) {
    if (where === undefined) { where = "before"; }

    var oi = indexOf(array, item);

    if (oi >= 0) { array.splice(oi, 1) };

    var pi = pivot === undefined ? -1 : indexOf(array, pivot);

    if (pi == -1) {
      if (where == "before") {
        array.unshift(item);
        return 0;
      } else {
        array.push(item);
        return arrayLength(array) - 1;
      }
    } else if (where == "before") {
      array.splice(pi, 0, item);
      return pi;
    } else {
      if (pi + 1 == arrayLength(array)) {
        array.push(item);
      } else {
        array.splice(pi + 1, 0, item);
      }
      return pi + 1;
    }
  }

  function _arrayRemove(array, item) {
    var i = indexOf(array, item);
    if (i != -1) {
      array.splice(i, 1);
    }
    return i;
  }

  // Remove an item from an array. If item is an array, then remove each of its members from the
  // target array.
  function arrayRemove(array, item) {
    if (_.isArray(item)) {
      return _.map(item, function(i) { return _arrayRemove(array, i); });
    } else {
      return _arrayRemove(array, item);
    }
  }

  // Add one array to another. If the `at` parameter is given, splice the values array into the
  // target list rather than just pushing to the end.
  function arrayExtend(to, values, at) {
    if (at === undefined) {
      to.push.apply(to, values);
    } else {
      to.splice.apply(to, [at, 0].concat(values));
    }
    return to;
  }

  // Replaces one item in an array with another.
  function arrayReplace(array, item, replaceWith) {
    var i = indexOf(array, item);
    if (i != -1) {
      array.splice(i, 1, replaceWith);
      return true;
    }
    return false;
  }

  // This method returns an ordinal for an item to be inserted in a list of ordinaled items.
  //
  // Suppose you have a list of objects of the from {name:value, ordinal:number}. Where the ordinal
  // value is a number that indicates the order the values should be presented. If you're adding a
  // new item to the list, you want its ordinal to be such that:
  // list[idx-1].ordinal < newOrdinal < list[idx+1].ordinal.
  //
  // This method handles that.
  function getNewOrdinal(list, atIndex, getOrdinal) {
    if (list.length == 0) {
      return 0;
    }

    if (atIndex == 0) {
      return getOrdinal(list[0]) - 1;
    }

    if (atIndex >= list.length) {
      return getOrdinal(list[list.length - 1]) + 1;
    }

    var pi = getOrdinal(list[atIndex - 1]);
    var ni = getOrdinal(list[atIndex]);
    return pi + (ni - pi) / 2;
  }

  ns.arrayInsert = arrayInsert;
  ns.arrayRemove = arrayRemove;
  ns.arrayExtend = arrayExtend;
  ns.arrayReplace = arrayReplace;
  ns.getNewOrdinal = getNewOrdinal;
})(jsu.util);
// Contains functionality to build comparison functions (for use in array.sort).

// DEPENDENCY: underscore

(function(ns) {
  "use strict";

  // args can either be:
  // {
  //   nullOrder: "first"|"last",
  //   field: string|array
  //   dir: 1|-1
  // }
  // or a string, which will be considered as the field in the above object.
  // If field is an array, it's treated like an object path, so ["a", "b"] will access obj.a.b in
  // the comparison, although nulls will be checked for, and if the path part ends with () then
  // it will be called like a function: ["a()", "b"] -> obj.a().b
  function getComparer(args) {
    if (_.isString(args)) {
      args = { field: args };
    }
    args = _.extend({}, getCmpDefaultArgs, args);
    var body;
    if (args.field && _.isArray(args.field)) {
      var _arg = _.extend({}, args);
      var cmps = [];
      for (var i = 0; i < args.field.length; ++i) {
        _arg.field = args.field[i];
        cmps.push(getCmp(_arg, i == 0));
      }
      body = cmps.join("\n");
    } else {
       body = getCmp(args, true);
    }

    body += "\nreturn 0;";

    return new Function("a", "b", body);
  }

  var cmpDebug = false;

  function getCmp(args, declare) {
    args = _.extend({}, args);
    var fieldDefs;

    if (args.field !== undefined && args.field !== null) {
      var chain = args.field.toString().split(".");
      fieldDefs = [];
      for (var i = 0; i < chain.length; ++i) {
        fieldDefs.push(parseFieldDef(chain[i]));
      }

      if (!args.dir) {
        args.dir = fieldDefs[0].dir;
      }
    }

    var extractionA = getExtraction("a", "av", fieldDefs);
    var extractionB = getExtraction("b", "bv", fieldDefs);

    var nullDir = args.nullOrder == "first" ? -1 : 1;
    var nullHandling = "if (av === null && bv === null) { return 0; } else { if (av === null) { return " + nullDir  + "; } if (bv === null) { return " + -nullDir + "; } }";
    var nonNullHandling = "if (av < bv) { return " + -args.dir + "; } if (bv < av) { return " + args.dir + "; }"

    var rv = "";

    if (declare) {
      rv = "var av, bv;\n";
    }

    if (cmpDebug) {
      var debug = "console.log(av + ' <=> ' + bv);"
      return rv + extractionA + "\n" + extractionB + "\n" + debug + "\n" + nullHandling + "\n" + nonNullHandling;
    } else {
      return rv + extractionA + "\n" + extractionB + "\n" + nullHandling + "\n" + nonNullHandling;
    }
  }

  function getExtraction(from, to, fieldDefs) {
    var l1 = to + " = "; // + from;
    var rhs = from;
    var pathParts = [];
    if (fieldDefs) {
      for (var i = 0; i < fieldDefs.length; ++i) {
        rhs += "['" + fieldDefs[i].field + "']";
        if (fieldDefs[i].unwrap) {
          rhs += "()";
        }
        pathParts.push(rhs);
      }
    }
    l1 = l1 + pathParts.join(" && ");
    var l2 = to + " = (" + to + " === null || " + to + " === undefined) ? null : " + to + ".valueOf();";
    return l1 + ";\n" + l2;
  }

  function parseFieldDef(f) {
    var rv = {
      dir: 1,
      field: null,
      unwrap: false
    };

    var dir = f[0];
    var substringStart = 0;
    var substringEnd = f.length;

    if (dir == "-") {
      substringStart = 1;
      rv.dir = -1;
    } else if (dir == "+") {
      substringStart = 1;
      rv.dir = 1;
    }

    if (f.slice(-2) == "()") {
      substringEnd -= 2;
      rv.unwrap = true;
    }

    rv.field = f.slice(substringStart, substringEnd);

    return rv;
  }

  var getCmpDefaultArgs = {
    nullOrder: "first",
    field: null,
    dir: null
  };

  // Export
  ns.getComparer = getComparer;
})(jsu.util);
// Contains a function for causing a method call on one object to be delegated to another object.

(function(ns) {
  "use strict";

  function _delegate(from, to, methodName) {
    from[methodName] = function() { return to[methodName].apply(to, arguments); };
  }

  function delegate() {
    var from = arguments[0]
    var to = arguments[1]
    for (var i = 2; i < arguments.length; ++i) {
      _delegate(from, to, arguments[i]);
    }
  }

  // Export
  ns.delegate = delegate;
})(jsu.util);
// Deserialize a list of JSON objects that were serialized into a CSV-like format.

(function(ns) {
  "use strict";

  function getDeserializer(keys) {
    var body = "var obj = {};\n"
    for (var i = 0; i < keys.length; ++i) {
      body += "obj['" + keys[i].replace("'", "\\'") + "'] = data[" + i + "];\n";
    }
    body += "return obj;";
    return new Function("data", body);
  }

  function deserialize(keys, data) {
    var deserializer = getDeserializer(keys);
    var objs = [];
    for (var i = 0; i < data.length; ++i) {
      objs.push(deserializer(data[i]));
    }
    return objs
  }

  // Export
  ns.getDeserializer = getDeserializer;
  ns.deserialize = deserialize;
})(jsu.util);
// DEPENDENCY: jQuery

(function(ns) {
  "use strict";

  function clearTextSelection() {
    if (window.getSelection) {
      window.getSelection().removeAllRanges();
    } else if (document.selection) {
      document.selection.empty();
    }
  }

  // This function makes it so you hold down the mouse button and drag it over checkboxes to toggle
  // them. It makes selecting or unselecting a series of checkboxes much quicker.
  function enableDragToggle(containerClass) {
    var mouseIsDown = false;
    var isInContainer = false;

    function isInToggleContainer(target) {
      var n = target;
      while (n) {
        if (n.className && n.className.indexOf(containerClass) != -1) {
          return true;
        }
        n = n.parentNode;
      }
      return false;
    }

    ns.registerEvent(document, "mousedown", function(e) {
      mouseIsDown = true;
      isInContainer = isInToggleContainer(e.target);
    });

    ns.registerEvent(document, "mousemove", function(e) {
      if(mouseIsDown && isInContainer) {
        clearTextSelection();
        e.preventDefault();
      }
    });

    ns.registerEvent(document, "mouseup", function(e) {
      mouseIsDown = false;
      isInContainer = false;
    });

    ns.registerEvent(document, "mouseover", function(e) {
      if (mouseIsDown) {
        isInContainer = isInContainer || isInToggleContainer(e.target);
        if (e.target.nodeName == "INPUT" && e.target.type == "checkbox" && isInContainer && !e.target.disabled) {
          e.target.checked = !e.target.checked;
          $(e.target).triggerHandler("click");
        }
      }
    });
  }

  // Export
  ns.enableDragToggle = enableDragToggle;
})(jsu.util);
// DEPENDENCY: jQuery

(function(ns) {
  "use strict";

  var registrations = [];

  // For registering events that will live until the window goes out of scope.
  // They will all be deregistered when the window unloads.
  function registerEvent(element, eventName, handler) {
    $(element).on(eventName, handler);
    registrations.push([element, eventName, handler]);
  }

  function deregisterEvents() {
    for (var i = 0; i < registrations.length; ++i) {
      var r = registrations[i];
      $(r[0]).off(r[1], r[2]);
    }
  }

  registerEvent(window, "unload", function() { deregisterEvents(); });

  // Export
  ns.registerEvent = registerEvent;
})(jsu.util);
(function(ns) {
  "use strict";

  function parseQueryString(qs, skipDecode) {
    // Parse a query string into an object. If a key occurs multiple times the
    // the value will be an array holding the values in the order the appear.

    var result = {};
    var qsArgs = qs.split("&");

    for (var i = 0; i < qsArgs.length; ++i) {
      var p = qsArgs[i].split("=");
      var k = p[0];
      var v = p.length > 1 ? p[1] : "";

      if (!skipDecode) { v = decodeURIComponent(v); }

      if (!(k in result)) {
        result[k] = v;
      }
      else if (result[k].push) {
        result[k].push(v);
      }
      else {
        result[k] = [result[k], v];
      }
    }

    return result;
  }

  function parseCookies(container) {
    // Parse cookies into an object. Container must be an object with a cookies
    // property, it defaults to document.

    var cookies = (container || document).cookie.split(";");
    var result = {};
    for (var i = 0; i < cookies.length; ++i) {
      var cookie = cookies[i].split("=");
      result[trimString(cookie[0])] = trimString(cookie[1]);
    }
    return result;
  }

  function setCookie(name, value, opts, container) {
    // Set a cookie's value. Also set expires, domain and path if they
    // specified in opts. Container must be an object with a cookies property,
    // it defaults to document.

    opts = opts || {};
    var c = name + "=" + value;
    if (opts.expires) { c += " expires=" + opts.expires.toString(); }
    if (opts.domain) { c += " domain=" + opts.domain.toString(); }
    if (opts.path) { c += " path=" + opts.path.toString(); }

    (container || document).cookie = c;
  }

  function deleteCookie(name, opts, container) {
    // Delete a cookie by setting its value to empty, and setting its expires
    // date to the unix epoch. Also set domain and path if specified in opts.
    // Container must be an object with a cookies property, it defaults to
    // document.

    opts = opts || {};
    var c = name + "=; expires=Thu, 01 Jan 1970 00:00:00 GMT;";
    if (opts.domain) { c += " domain=" + opts.domain.toString(); }
    if (opts.path) { c += " path=" + opts.path.toString(); }

    (container || document).cookie = c;
  }

  function extractXhrError(xhr, status, error) {
    // Given the jQuery ajax error callback args, extract a sensible error
    // message.

    var contentType = xhr.getResponseHeader("content-type");
    var errorMessage = null;

    if (xhr.status == 0) {
      errorMessage = "We were unable to contact the server. It appears that you are offline or your internet connection is down. Please make sure you are connected and try again.";
    } else if (/^text\/plain/i.test(contentType)) {
      errorMessage = xhr.responseText;
    } else if (/^text\/html/i.test(contentType)) {
      errorMessage = error;
    } else if (/^application\/json/i.test(contentType) && "message" in xhr.responseJSON) {
      errorMessage = xhr.responseJSON["message"];
    } else {
      errorMessage = error;
    }

    return errorMessage;
  }

  function inherit(superclass, subclass) {
    // Alter a constructor's prototype chain. Uses Object.create when
    // available, and an older method when not.

    if (Object.create) {
      subclass.prototype = Object.create(superclass.prototype);
    } else {
      var proto = new superclass();
      var toDelete = [];

      for (var pn in proto) {
          if (proto.hasOwnProperty(pn)) {
              toDelete.push(pn);
          }
      }
      for (var i = 0; i < toDelete.length; ++i) {
         delete proto[toDelete[i]];
      }
      subclass.prototype = proto;
      proto.constructor = subclass;
    }
  }

  function makeBroadcaster(obj) {
    // Add methods to an object to allow it to broadcast messages to
    // subscribers

    var nextId = 1;
    var registrants = { };

    obj.on = function(name, handler) {
      // Add a handler for a named action. Returns an id that can be used to
      // unsubscribe.

      var id = nextId++;
      registrants[id] = [name, handler];
      return id;
    };

    obj.off = function(id) {
      // Remove a handler using the id returned from the on function. If id is
      // undefined, clear all registrants.

      if (id === undefined) {
        registrants = { };
        return true;
      } else {
        if (id in registrants) {
          delete registrants[id];
          return true;
        }
        return false;
      }
    }

    obj.trigger = function(name, args, sender) {
      // Send a message to all subscriptions on name with the given args. If
      // sender is not specified, it defaults to obj.

      for (var id in registrants) {
        var n = registrants[id][0];
        var h = registrants[id][1];
        if (n == name) {
          h(name, args, sender || obj);
        }
      }
    }
  }

  function getNS() {
    // Walk through a hierarchy from root, given as the first argument, through
    // children specified in the remaining arguments. If a child is not found
    // in its parent, create it as an empty object.

    var root = arguments[0];
    if (arguments.length == 0) { return root; }
    var current = root;
    for (var i = 1; i < arguments.length; ++i) {
      var next = arguments[i].toString();
      if (!(next in current)) { current[next] = {}; }
      current = current[next];
    }
    return current;
  }

  function getProperty(root, chain) {
    // Get a deeply nested property identified by chain starting at root.
    // chain format is: id0.id1.(...).idn
    // where each id is the name of a property on the preceeding object. Each
    // id can optionally be suffixed with '()' to cause the node to be
    // called as a function before continuing down the chain.

    var n = root;
    var c = null;

    if (_.isArray(chain)) {
      c = chain;
    } else {
      c = chain.split(".");
    }

    for (var i = 0; i < c.length; ++i) {
      var p = c[i];

      if (p.slice(-2) == "()") {
        n = n[p.slice(0, -2)]();
      } else {
        n = n[p];
      }

      if (!isPresent(n)) {
        break;
      }
    }

    return n;
  }

  var trimStringRe = /^\s*|\s*$/g;
  function trimString(s) {
    // Remove trailing and leading spaces from a string.

    if (s) {
      return s.replace(trimStringRe, "");
    } else {
      return s;
    }
  }

  var isBlankRe = /^\s+$/;
  function isBlank(o) {
    // Returns true if a string is not present, has zero length, or is all
    // space characters.

    var notEmpty = o && o.length && !isBlankRe.test(o);
    return !notEmpty;
  }

  function _isPresent(v) {
    // Returns true if v is not null and not undefined.

    return v !== null && v !== undefined;
  }

  function isPresent() {
    for (var i = 0; i < arguments.length; ++i) {
      if (!_isPresent(arguments[i])) {
        return false;
      }
    }
    return true;
  }

  function join(actions, callback, afterIntermediate) {
    // Given a list of async actions that call a callback when done, call each
    // action and when all have completed call callback.

    var completed = 0;
    var required = actions.length;

    function _callback() {
      if (++completed == required) {
        callback();
      } else if (afterIntermediate) {
        afterIntermediate(completed - 1);
      }
    }

    for (var i = 0; i < actions.length; ++i) {
      actions[i](_callback);
    }
  }

  function serialize(actions, callback, afterIntermediate) {
    // Given a list of async actions that call a callback when done, call each
    // action and when its calls its callback, call the next item. When all
    // have completed, call callback.

    var i = 0;

    function callback_() {
      if (i == actions.length) {
        if (callback) {
          callback();
        }
      } else {
        if (afterIntermediate && i > 0) {
          afterIntermediate(i);
        }
        actions[i++](callback_);
      }
    }

    callback_();
  }

  function queue(action, timeout) {
    // Request an action to be queued up after all pending actions have
    // completed.

    return setTimeout(action, timeout || 0);
  }

  function compareArrays(al, ar, strict, orderIndependent) {
    // Compares the two arrays item-by-item. If strict is true, '===' is used, otherwise '==' is
    // used. If orderIndependent is true, copies of the arrays, sorted, are compared.
    //
    // NOTE: Only works for scalar values.

    if (!_.isArray(al) || !_.isArray(ar)) { return false; }
    if (al === ar) { return true; }
    if (al.length != ar.length) { return false; }

    if (orderIndependent) {
      al = al.slice();
      al.sort();
      ar = ar.slice();
      ar.sort();
    }

    for (var i = 0; i < al.length; ++i) {
      if (strict) {
        if (al[i] !== ar[i]) {
          return false;
        }
      } else {
        if (al[i] != ar[i]) {
          return false;
        }
      }
    }

    return true;
  }

  function compareObjects(ol, or, strict, orderIndependent) {
    // Compares to objects.
    //
    // If the objects are arrays, calls compareArrays. If the objects are scalars, compares them
    // using '==' if strict is true, or '===' if strict is not true. Otherwise each key/value in ol
    // is compared to each in or using compareObjects.

    if (!(jsu.util.isPresent(ol) && jsu.util.isPresent(or))) { return ol === or; }
    if (ol === or) { return true; }

    if (_.isArray(ol)) {
      return compareArrays(ol, or, strict, orderIndependent);
    } else if (_.isObject(ol)) {
      if (!_.isObject(or)) {
        return false;
      }
      if (_.keys(ol).length != _.keys(or).length) {
        return false;
      }

      for (var k in ol) {
        if (!(k in or)) {
          return false;
        }
        var vl = ol[k];
        var vr = or[k];

        if (!compareObjects(vl, vr, strict, orderIndependent)) {
          return false;
        }
      }
    } else {
      if (strict) {
        if (ol !== or) {
          return false;
        }
      } else {
        if (ol != or) {
          return false;
        }
      }
    }

    return true;
  }

  var localStorage = typeof window == "undefined" ? null : window.localStorage;

  function localStorageRead(key, deserialize) {
    // Get a value from local storage (if available), optionally deserializing
    // it with the given deserializer.

    if (!(key && localStorage)) {
      return undefined;
    }
    var v = localStorage.getItem(key);
    if (v === null) {
      return undefined;
    }
    return deserialize ? deserialize(v) : v;
  }

  function localStorageWrite(key, value, serialize) {
    // Write a value to local storage (if available), optionally serializing it
    // with the given serializer.

    if (key && localStorage) {
      if (value === undefined || value === null) {
        localStorage.removeItem(key)
      } else {
        localStorage.setItem(key, serialize ? serialize(value) : value);
      }
    }
  }

  function clearLocalStorage(key) {
    // Delete a key from local storage.

    if (key === undefined) {
      localStorage.clear();
    } else if (_.isArray(key)) {
      _.each(key, function(k) { localStorage.removeItem(k); });
    } else {
      localStorage.removeItem(key);
    }
  }

  function validateLocalStorage(localStorageVersion, versionKey) {
    // Check that the value in localStorage[versionKey] == localStorageVersion,
    // and if not, clear out all of local storage. This makes it easy to clear
    // local storage when you make large changes to how you store things.

    versionKey = versionKey || "localStorageVersion";
    var currentValue = localStorage.getItem(versionKey);
    if (currentValue != localStorageVersion) {
      localStorage.clear();
      localStorage.setItem(versionKey, localStorageVersion);
    }
  }

  var abbreviateRe = /(?:^|\s)(\w[A-Z0-9]*)/g;
  function abbreviate(s, sep) {
    // Abbreviates a string to its initials.

    var p = "";
    sep = (sep === undefined || sep === null) ? "" : sep;
    while (true) {
      var m = abbreviateRe.exec(s);
      if (m) {
        p += m[1] + sep;
      } else {
        break;
      }
    }
    return p;
  }

  // Export
  ns.parseQueryString = parseQueryString;
  ns.parseCookies = parseCookies;
  ns.setCookie = setCookie;
  ns.deleteCookie = deleteCookie;
  ns.extractXhrError = extractXhrError;
  ns.inherit = inherit;
  ns.makeBroadcaster = makeBroadcaster;
  ns.getNS = getNS;
  ns.getProperty = getProperty;
  ns.trimString = trimString;
  ns.isBlank = isBlank;
  ns.isPresent = isPresent;
  ns.join = join;
  ns.serialize = serialize;
  ns.queue = queue;
  ns.compareArrays = compareArrays;
  ns.compareObjects = compareObjects;
  ns.localStorageWrite = localStorageWrite;
  ns.localStorageRead = localStorageRead;
  ns.clearLocalStorage = clearLocalStorage;
  ns.validateLocalStorage = validateLocalStorage;
  ns.abbreviate = abbreviate;
})(jsu.util);
// DEPENDENCY: underscore

(function(ns) {
  "use strict";

  // If obj is a string:
  // - if obj ends with (), then return subj[obj]()
  // - otherwise return subj[obj]
  // Otherwise treat obj as a function and return obj(subj);
  function indexOrCall(subj, obj) {
    if (_.isString(obj)) {
      if (subj === null || subj === undefined) {
        return subj;
      }
      if (obj.substring(obj.length - 2) == "()") {
        return subj[obj.substring(0, obj.length - 2)]();
      } else {
        return subj[obj];
      }
    } else if (_.isNumber(obj)) {
      return subj[obj];
    } else {
      return obj(subj);
    }
  }

  function indexObjects(objects, getKey) {
    getKey = getKey || "id";
    return _.chain(objects).map(function(o) { return [indexOrCall(o, getKey), o]; }).object().value();
  }

  function selectObjects(objects, keys, getKey, definedOnly) {
    if (definedOnly === undefined) { definedOnly = true; }
    var idx = indexObjects(objects, getKey);
    var sel = _.map(keys, function(k) { return idx[k]; });
    if (definedOnly) {
      return _.filter(sel, function(s) { return s !== undefined; });
    } else {
      return sel;
    }
  }

  var howRe = /(.*?)(=|\[\]|\(\)=|\(\)\[\])?$/;

  function assignValue(assigner, assignee, how) {
    if (!how) { return; }
    if (_.isString(how)) {
      // Looks for one of: field=, field[], field()=, field()[]
      var matches = howRe.exec(how);
      var field = matches[1];
      var method = matches[2];

      switch(method) {
        case "=":
          assigner[field] = assignee;
          break;
        case "[]":
          if (!assigner[field]) { assigner[field] = []; }
          assigner[field].push(assignee);
          break;
        case "()=":
          assigner[field](assignee);
          break;
        case "()[]":
          if (!assigner[field]()) { assigner[field]([]); }
          assigner[field].push(assignee);
          break;
        default:
          throw new Error("Unknown assignment method: " + how);
      }
    } else {
      how(assigner, assignee);
    }
  }

  // args
  // - [REQ] parents
  // - [REQ] children
  // - [REQ] getIdOfParent: (parent) -> id
  // - [REQ] getParentId: (child) -> parentId
  // - assignChild: '=', '[]', '()=', '()[]', or function(parent, child)
  // - assignParent: '=', '[]', '()=', '()[]', or function(child, parent)
  // - strict: raise an error if parent not found.
  function joinObjects(args) {
    var parentMapping = indexObjects(args.parents, args.getIdOfParent);
    for (var i = 0; i < args.children.length; ++i) {
      var child = args.children[i];
      var parentId = indexOrCall(child, args.getParentId);
      if (parentId === undefined) { continue; }
      var parent = parentMapping[parentId];
      if (parent) {
        assignValue(parent, child, args.assignChild);
        assignValue(child, parent, args.assignParent);
      } else {
        if (parentId === null) {
          // Do nothing.
        } else if (args.strict) {
          throw new Error("Parent with id {0} not found".format(parentId));
        }
      }
    }
  }

  // args
  // - [REQ] parents
  // - [REQ] children
  // - [REQ] joiners
  // - [REQ] getIdOfParent
  // - [REQ] getIdOfChild
  // - [REQ] getParentId
  // - [REQ] getChildId
  // - assignChild: '=', '[]', '()=', '()[]', or function(parent, child)
  // - assignParent: '=', '[]', '()=', '()[]', or function(child, parent)
  function joinObjectsThrough(args) {
    var parentMapping = indexObjects(args.parents, args.getIdOfParent);
    var childMapping = indexObjects(args.children, args.getIdOfChild);
    for (var i = 0; i < args.joiners.length; ++i) {
      var joiner = args.joiners[i];
      var parentId = indexOrCall(joiner, args.getParentId);
      var childId = indexOrCall(joiner, args.getChildId);
      if (parentId === null || childId === null || parentId === undefined || childId === undefined) { continue; }
      var parent = parentMapping[parentId];
      var child = childMapping[childId];
      if (parent === null || parent === undefined || child === null || child === undefined) { continue; }
      assignValue(parent, child, args.assignChild);
      assignValue(child, parent, args.assignParent);
    }
  }

  // Export
  ns.indexOrCall = indexOrCall;
  ns.indexObjects = indexObjects;
  ns.assignValue = assignValue;
  ns.selectObjects = selectObjects;
  ns.joinObjects = joinObjects;
  ns.joinObjectsThrough = joinObjectsThrough
})(jsu.util);
(function(ns) {
  "use strict";

  var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz+/";

  function toChar(i) {
    if (i < 0) {
      throw new Error("Index must be in [0, " + (chars.length - 1) + "]");
    }
    if (i > chars.length - 1) {
      throw new Error("Cannot handle values greater than " + chars.length);
    }
    return chars.charAt(i);
  }

  function getRandomId(len, base) {
    base = Math.min(Math.max(Math.abs(base || 64), 2), 64);
    var codes = [];
    for (var i = 0; i < len; ++i) {
      codes.push(toChar(Math.floor(Math.random() * base)));
    }
    return codes.join("");
  }

  var uuidIds = "89AB";

  function getRandomUUID() {
    var randId = uuidIds.charAt(Math.floor(Math.random() * 4));
    return [
      getRandomId(8, 16),
      getRandomId(4, 16),
      "4" + getRandomId(3, 16),
      randId + getRandomId(3, 16),
      getRandomId(12, 16)
    ].join("-");
  }

  function getRandomIdSpace(base) {
    var base = base || 64;
    return chars.slice(0, base);
  }

  ns.getRandomId = getRandomId;
  ns.getRandomUUID = getRandomUUID;
  ns.getRandomIdSpace = getRandomIdSpace;
})(jsu.util);
(function(ns) {
  "use strict";

  var reservedChars = "\\^$*+?.()[]=!{},";

  // This function will escape all reserved regex characters except those (if any) passed into
  // okChars. This can be useful for building traditional wildcard search strings, for instance.
  function getRegexSafeText(text, okChars) {
    return text.replace(new RegExp("(\\" + reservedChars.replace(okChars || "", "").split("").join("|\\") + ")", "g"), "\\$1");
  }

  function getRegexReservedChars() {
    return reservedChars;
  }

  // Export
  ns.getRegexSafeText = getRegexSafeText;
  ns.getRegexReservedChars = getRegexReservedChars;
})(jsu.util);
(function(ns) {
  var defaultBreakpoints = [[768, "xs"], [992, "sm"], [1200, "md"], [Infinity, "lg"]];
  var hasRegisteredSize = false;

  function defaultGetWindowSize() {
    return window.innerWidth;
  }

  function getSizeName(breakpoints, length) {
    for (var i = 0; i < breakpoints.length; ++i) {
      if (length < breakpoints[i][0]) {
        return breakpoints[i][1];
      }
    }
    return undefined;
  }

  function getWindowSizeName(breakpoints, windowSize) {
    return getSizeName(breakpoints || defaultBreakpoints, windowSize || window.innerWidth);
  }

  function notifyOnWindowSizeChanged(callback, breakpoints, getWindowSize) {
    if (hasRegisteredSize) {
      throw "getWindowSizeName has already been registered";
    }
    breakpoints = breakpoints || defaultBreakpoints;
    getWindowSize = getWindowSize || defaultGetWindowSize;
    var lastWindowSizeName = getWindowSizeName(breakpoints, getWindowSize());
    jsu.util.registerEvent(window, "resize", function() {
      var windowSize = getWindowSize();
      var newWindowSizeName = getWindowSizeName(breakpoints, windowSize);
      if (newWindowSizeName != lastWindowSizeName) {
        callback(newWindowSizeName, lastWindowSizeName, windowSize);
        lastWindowSizeName = newWindowSizeName;
      }
    });
    return lastWindowSizeName;
  }

  function getOrientationName() {
    if ("orientation" in window) {
      switch (window.orientation) {
      case 90:
      case -90:
        return "landscape";
      default:
        return "portrait";
      }
    } else {
      return undefined;
    }
  }

  function notifyOnWindowOrientationChanged(callback) {
    if ("onorientationchange" in window) {
      jsu.util.registerEvent(window, "orientationchange", function() { callback(getOrientationName()); });
    }
    return getOrientationName();
  }

  ns.getSizeName = getSizeName;
  ns.notifyOnWindowSizeChanged = notifyOnWindowSizeChanged;
  ns.breakpoints = defaultBreakpoints;

  ns.getOrientationName = getOrientationName;
  ns.notifyOnWindowOrientationChanged = notifyOnWindowOrientationChanged;
})(jsu.util);
jsu.ko = jsu.ko || {};
// DEPENDENCY: knockout

(function(ns) {
  "use strict";

  function AjaxStatus(owner, timeout) {
    var timeoutHandle = null;

    owner._ajaxStatus = ko.observable(null);
    owner.ajaxStatus = ko.computed({
      read: owner._ajaxStatus,
      write: function(v) {
        if (owner._ajaxStatus() != v) {
          clearTimeout(timeoutHandle);
          owner._ajaxStatus(v);
          if (v && v != "loading" && timeout) {
            timeoutHandle = setTimeout(clearAjaxStatus, timeout);
          }
        }
      }
    });

    function clearAjaxStatus() {
      owner.ajaxStatus(null);
    }
  }

  // Export
  ns.AjaxStatus = AjaxStatus;
})(jsu.ko);
(function(ns) {
  function Expandable(data) {
    if (!data) { data = {}; }

    var self = this;

    self.keys = ko.observableArray([]);

    function setup() {
      self.keys(_.keys(data).sort());

      for (var k in data) {
        if (k == "keys") {
          throw new Error("Expandable object cannot overwrite member 'keys'");
        }
        self[k] = ko.observable(data[k]);
      }

      jsu.ko.TrackChanges(self, self.keys());
    }

    setup();
  }

  Expandable.prototype.addAttribute = function(name, value) {
    var self = this;

    self[name] = ko.observable(value === undefined ? null : value);
    self.addChangeTracking(name);

    self.keys.splice(_.sortedIndex(self.keys(), name), 0, name);
  }

  Expandable.prototype.removeAttribute = function(name) {
    var self = this;

    self.removeChangeTracking(name);
    delete self[name];
    self.keys.splice(_.indexOf(self.keys(), name, true), 1);
  }

  Expandable.prototype.toJS = function() {
    var self = this;
    var data = {};

    _.each(self.keys(), function(k) {
      data[k] = self[k]();
    });

    return data;
  }

  ns.Expandable = Expandable;
})(jsu.ko);
// DEPENDENCY: knockout

(function(ns) {
  "use strict";

  function ToggleMethod(owner, methodName) {
    owner[methodName || "toggle"] = function(name) {
      owner[name](!owner[name]());
      return true;
    };
  }

  function SetValueMethod(owner, clearOnRepeatedSet) {
    owner.setValue = function(prop, val) {
      if (clearOnRepeatedSet && val === owner[prop]()) {
        owner[prop](null);
      } else {
        owner[prop](val);
      }
      return true;
    }
  }

  function OppositeProperty(owner, name, underlyingName, koComputedOpts) {
    owner[name] = ko.computed(_.extend({
      read: function() { return !owner[underlyingName](); },
      write: function(v) { owner[underlyingName](!v); }
    }, koComputedOpts));
  }

  function getMappingPair(basis) {
    function fromJS(data, target) {
      if (target === undefined) {
        target = {};
      }

      for (var k in basis) {
        var v = k in data ? data[k] : basis[k];

        if (_.isArray(v)) {
          target[k] = ko.observableArray(v);
        } else {
          target[k] = ko.observable(v);
        }
      }

      return target;
    }

    function toJS(target) {
      var js = {};

      for (var k in basis) {
        js[k] = ko.unwrap(target[k]);
      }

      return js;
    }

    return { fromJS: fromJS, toJS: toJS };
  }

  // Export
  ns.ToggleMethod = ToggleMethod;
  ns.SetValueMethod = SetValueMethod;
  ns.OppositeProperty = OppositeProperty;
  ns.getMappingPair = getMappingPair;
})(jsu.ko);
// DEPENDENCY: knockout
// DEPENDENCY: utils/general

(function(ns) {
  "use strict";

  // SerializedProperty(owner, name, args)
  // SerializedProperty(owner, nameArray, args)
  function SerializedProperty(owner, nameOrNames, args) {
    if (_.isArray(nameOrNames)) {
      _.each(nameOrNames, function(name) { _SerializedProperty(owner, name, args); });
    } else {
      _SerializedProperty(owner, nameOrNames, args);
    }
  }

  function _SerializedProperty(owner, name, args) {
    args = _.extend({}, serializedPropertyDefaultArgs, args);

    if (name in owner && args.initialValue === undefined) {
      args.initialValue = ko.unwrap(owner[name]);
    }

    if (!args.underlyingName) {
      args.underlyingName = args.transformName(name);
    }
    owner[args.underlyingName] = ko.observable(SerializedProperty.getInitialValue(args));

    owner[name] = ko.computed({
      owner: owner,
      read: function() { return args.deserialize(owner[args.underlyingName]()); },
      write: function(rawValue) {
        var newValue = args.serialize(rawValue);
        if (owner[args.underlyingName]() !== newValue) {
          var oldValue = owner[args.underlyingName]();

          if (args.willChange) {
            if (false === args.willChange(newValue, oldValue, name, owner, "willChange")) {
              return;
            }
          }

          owner[args.underlyingName](newValue);

          if (args.didChange) {
            args.didChange(newValue, oldValue, name, owner, "didChange");
          }

          jsu.util.localStorageWrite(args.localStorageKey, newValue, args.localStorageSerializer);
        }
      }
    });
  }

  SerializedProperty.getInitialValue = function(args) {
    var v = undefined;
    var sis = args.skipInitialSerialization;

    if (args.localStorageKey) {
      v = jsu.util.localStorageRead(args.localStorageKey, args.localStorageDeserializer);
    }

    if (v !== undefined) {
      sis = true;
    } else {
      v = args.initialValue;

      if (v === undefined) {
        v = null;
      }
    }

    return sis ? v : args.serialize(v);
  }

  var serializedPropertyDefaultArgs = {
    underlyingName: null,
    transformName: function(n) { return "_" + n; },
    serialize: _.identity,
    deserialize: _.identity,
    initialValue: undefined,
    skipInitialSerialization: false,
    didChange: null,
    willChange: null,
    localStorageKey: null,
    localStorageSerializer: _.identity,
    localStorageDeserializer: _.identity
  };

  function IntegerProperty(owner, name, args) {
    args = _.extend({}, integerPropertyDefaultArgs, args);
    args.serialize = function(v) {
      v = parseInt(v);
      return isNaN(v) ? args.defaultValue : v;
    };
    args.localStorageDeserializer = function(v) { return parseInt(v); };

    SerializedProperty(owner, name, args);
  }

  var integerPropertyDefaultArgs = {
    defaultValue: null
  };

  function FloatProperty(owner, name, args) {
    args = _.extend({}, floatPropertyDefaultArgs, args);
    args.serialize = function(v) {
      v = parseFloat(v);
      return isNaN(v) ? args.defaultValue : v;
    };
    args.localStorageDeserializer = function(v) { return praseFloat(v); }

    SerializedProperty(owner, name, args);
  }

  var floatPropertyDefaultArgs = {
    defaultValue: null
  };

  function IntegerArrayProperty(owner, name, args) {
    args = _.extend({}, integerArrayPropertyDefaultArgs, args);
    if (args.sort) {
      args.serialize = function(v) {
        var v = _.map(v, toInt);
        v.sort();
        return v;
      };
    } else {
      args.serialze = function(v) { return _.map(v, toInt); };
    }
    args.deserialize = function(v) { return _.map(v, toInt); };

    SerializedProperty(owner, name, args);
  }

  var integerArrayPropertyDefaultArgs = {
    sort: false
  };

  function RoundedFloatProperty(owner, name, args) {
    args = _.extend({}, roundedFloatPropertyDefaultArgs, args);
    args.serialize = function(v) {
      v = parseFloat(v);

      if (isNaN(v)) {
        return args.defaultValue;
      } else {
        v = v.toFixed(args.scale);
        if (args.stripZerosBeyond && args.stripZerosBeyond < args.scale && /^0+$/.test(v.substring(v.length - args.stripZerosBeyond))) {
          return v.substring(0, v.length - args.stripZerosBeyond);
        } else {
          return v;
        }
      }
    };

    SerializedProperty(owner, name, args);
  }

  var roundedFloatPropertyDefaultArgs = {
    scale: 2,
    defaultValue: null,
    stripZerosBeyond: null
  };

  function SteppedFloatProperty(owner, name, args) {
    args = _.extend({}, steppedFloatPropertyDefaultArgs, args);
    args.serialize = function(v) {
      v = parseFloat(v);

      if (isNaN(v)) {
        return args.defaultValue;
      } else {
        var s = (Math.round(v / args.step) * args.step).toFixed(args.scale);
        if (args.stripTrailingZeros) {
          return s.replace(stripTrailingZerosRe, "");
        }
        return s;
      }
    };

    SerializedProperty(owner, name, args);
  }

  var steppedFloatPropertyDefaultArgs = {
    step: 1,
    scale: 0,
    defaultValue: null,
    stripTrailingZeros: true
  };

  var stripTrailingZerosRe = /\.?0+$/;

  function TimeProperty(owner, name, args) {
    args = _.extend({}, timePropertyDefaultArgs, args);
    args.deserialize = function(v) {
      if (v) {
        var m = moment(v);
        var fmt = _.isArray(args.timeFormat) ? args.timeFormat[0] : args.timeFormat;
        return m.format(fmt);
      } else {
        return v;
      }
    };
    args.serialize = function(v) {
      if (v) {
        var m = moment(v, args.timeFormat);
        if (m.isValid()) {
          return m.format();
        } else {
          return null;
        }
        return moment(v, args.timeFormat).format();
      } else {
        return null;
      }
    };

    SerializedProperty(owner, name, args);
  }

  var timePropertyDefaultArgs = {
    timeFormat: "hh:mm A",
    skipInitialSerialization: true
  };

  function toInt(v) {
    return parseInt(v);
  }

  // Export
  ns.SerializedProperty = SerializedProperty;
  ns.IntegerProperty = IntegerProperty;
  ns.FloatProperty = FloatProperty;
  ns.IntegerArrayProperty = IntegerArrayProperty;
  ns.RoundedFloatProperty = RoundedFloatProperty;
  ns.SteppedFloatProperty = SteppedFloatProperty;
  ns.TimeProperty = TimeProperty;
})(jsu.ko);
// DEPENDENCY: knockout

(function(ns) {
  function StatusMessage(owner, timeouts) {
    var timeoutHandle = null;

    owner.statusMessageType = ko.observable(null);
    owner.statusMessage = ko.observable(null);

    owner.clearMessage = function() {
      owner.statusMessageType(null);
      owner.statusMessage(null);
    }

    owner.setMessage = function(type, message, timeout) {
      if (!(type && message)) {
        owner.clearMessage();
      }

      if (timeouts && timeout === undefined) {
        timeout = parseInt(timeouts[type]);
      }

      owner.statusMessageType(type);
      owner.statusMessage(message);

      clearTimeout(timeoutHandle);

      if (timeout && timeout > 0) {
        timeoutHandle = setTimeout(owner.clearMessage, timeout);
      }
    }
  }

  ns.StatusMessage = StatusMessage;
})(jsu.ko);
// DEPENDENCY: knockout
// DEPENDENCY: underscore

(function(ns) {
  "use strict";

  function TrackChanges(owner, properties, opts) {
    opts = _.extend({}, defaultOpts, opts);

    if ("hasChanges" in owner) {
      throw new Error("Change tracking can only be set once for an object");
    }

    var originalValues = { };
    var changeTrackedChildren = { };
    var subscriptions = { };

    function resetChangedFlags() {
      _.each(properties, function(p) {
        if (!(p in changeTrackedChildren)) {
          owner[opts.transformPropertyName(p)](false);
        }
      });
      owner.changedPropertyCount(0);
    }

    function getOriginalValue(propertyName) {
      var v = owner[propertyName]();

      // Create a copy of the array, otherwise changes to the
      // array in place will never cause changes to be triggered.
      if (_.isArray(v)) {
        return v.slice();
      } else {
        return v;
      }
    }

    function setOriginalValues() {
      originalValues = _.object(_.map(properties, function(p) {
        return [p, getOriginalValue(p)];
      }));
    }

    function getChangeSubscription(propertyName, changedPropertyName) {
      function subscription(newValue) {
        var oldChangedValue = owner[changedPropertyName]();
        var isSame = jsu.util.compareObjects(newValue, originalValues[propertyName], opts.strict);
        var newChangedValue = !isSame;

        owner[changedPropertyName](newChangedValue);

        if (oldChangedValue != newChangedValue) {
          // If the property is going from being changed to not being changed, we need to reduce the
          // number of changed properties. If it is going from not being changed to being changed,
          // we need to increase the number of changed properties.
          var delta = oldChangedValue ? -1 : 1;
          owner.changedPropertyCount(owner.changedPropertyCount() + delta);
        }

        if (opts.propertyObserver) {
          opts.propertyObserver(owner, propertyName);
        }
      }

      return subscription;
    }

    function trackProperty(propertyName, changedPropertyName) {
      if (!(owner[propertyName] && owner[propertyName].subscribe)) {
        throw new Error("Cannot change track " + propertyName);
      }

      if (propertyName in subscriptions) {
        throw new Error("Already tracking " + propertyName);
      }

      owner[changedPropertyName] = ko.observable(false);

      var subscriptionId = owner[propertyName].subscribe(getChangeSubscription(propertyName, changedPropertyName));

      subscriptions[propertyName] = subscriptionId;
    }

    function addChangeTracking(propertyName) {
      var changedPropertyName = opts.transformPropertyName(propertyName);
      trackProperty(propertyName, changedPropertyName);
      originalValues[propertyName] = getOriginalValue(propertyName);
    }

    function removeChangeTracking(propertyName) {
      if (!(propertyName in subscriptions)) {
        throw new Error("Not tracking " + propertyName);
      }

      var changedPropertyName = opts.transformPropertyName(propertyName);

      if (owner[changedPropertyName]()) {
        owner.changedPropertyCount(owner.changedPropertyCount() - 1);
      }

      subscriptions[propertyName].dispose();

      delete owner[changedPropertyName];
      delete originalValues[propertyName];
      delete subscriptions[propertyName];
    }

    function saveChanges(data, skipUpdate) {
      // If owner implements a saved function, it will be passed data to update its values. This can
      // be used to allow models to set autoincremented ids returned from the server, or timestamps,
      // etc.
      if (data && opts.savedMethodName in owner && !skipUpdate) {
        owner[opts.savedMethodName](data);
      }

      setOriginalValues();

      for (var k in changeTrackedChildren) {
        changeTrackedChildren[k].saveChanges(data && data[k] || null, skipUpdate);
      }

      resetChangedFlags();
    }

    function revertChanges() {
      for (var k in originalValues) {
        if (k in changeTrackedChildren) {
          changeTrackedChildren[k].revertChanges();
        } else {
          var n = k;
          var v = originalValues[k];

          if (_.isArray(v)) {
            v = v.slice();
          }

          owner[n](v);
        }
      }

      resetChangedFlags();
    }

    function setup() {
      for (var i = 0; i < properties.length; ++i) {
        var propertyName = properties[i];
        var currentValue = owner[propertyName]();

        if (currentValue && currentValue.hasChanges) {
          changeTrackedChildren[propertyName] = currentValue;
        } else {
          var changedPropertyName = opts.transformPropertyName(propertyName);

          if (changedPropertyName in owner) {
            throw new Error(changedPropertyName + " already exists.");
          }

          trackProperty(propertyName, changedPropertyName);
        }
      }
    }

    setup();
    setOriginalValues();

    owner.addChangeTracking = addChangeTracking;
    owner.removeChangeTracking = removeChangeTracking;
    owner.changedPropertyCount = ko.observable(0);
    owner.hasChanges = ko.computed(function() {
      for (var k in changeTrackedChildren) {
        if (changeTrackedChildren[k].hasChanges()) {
          return true;
        }
      }
      return owner.changedPropertyCount() != 0;
    });
    owner.saveChanges = saveChanges;
    owner.revertChanges = revertChanges;
  }

  function defaultTransformPropertyName(n) { return n + "_changed"; }

  var defaultOpts = {
    strict: false,
    transformPropertyName: defaultTransformPropertyName,
    propertyObserver: null,
    savedMethodName: "saved"
  };
  ns.TrackChanges = TrackChanges;
})(jsu.ko);
// DEPENDENCY: knockout
// DEPENDENCY: underscore

(function(ns) {
  "use strict";

  function TrackErrors(owner, propertyNames, transformPropertyName, multiple) {
    if ("hasErrors" in owner) {
      throw new Error("Error tracking can only be set once for an object");
    }

    if (!transformPropertyName) {
      transformPropertyName = defaultTransformPropertyName;
    }

    function clearError(propertyName) {
      var errorName = transformPropertyName(propertyName);
      var changesErrorCount = false;

      if (multiple) {
        if (owner[errorName].length != 0) {
          owner[errorName]([]);
          changesErrorCount = true;
        }
      } else {
        if (owner[errorName]()) {
          owner[errorName](null);
          changesErrorCount = true;
        }
      }

      if (changesErrorCount) {
        owner.errorPropertyCount(owner.errorPropertyCount() - 1);
      }
    }

    function clearErrors(propertyName) {
      if (propertyName === undefined) {
        _.each(propertyNames, function(pn) { clearError(pn); });
      } else {
        clearError(propertyName);
      }
    }

    function addError(propertyName, errorMessage) {
      if (!errorMessage) { return; }

      var errorName = transformPropertyName(propertyName);
      var changesErrorCount = false;

      if (multiple) {
        if (owner[errorName]().length == 0) {
          changesErrorCount = true;
        }
        owner[errorName].push(errorMessage);
      } else {
        if (!owner[errorName]()) {
          changesErrorCount = true;
        }
        owner[errorName](errorMessage);
      }

      if (changesErrorCount) {
        owner.errorPropertyCount(owner.errorPropertyCount() + 1);
      }
    }

    function mapErrors(errors) {
      clearErrors();

      for (var propertyName in errors) {
        var errorMessage = errors[propertyName];

        if (_.isArray(errorMessage)) {
          if (multiple) {
            _.each(errorMessage, function(em) {
              addError(propertyName, em);
            });
          } else {
            addError(propertyName, _.last(errorMessage));
          }
        } else {
          addError(propertyName, errorMessage);
        }
      }
    }

    function listErrors() {
      var list = [];

      _.each(propertyNames, function(propertyName) {
        var errorName = transformPropertyName(propertyName);

        if (multiple) {
          _.each(owner[errorName](), function(msg) {
            list.push({ propertyName: propertyName, errorMessage: msg });
          });
        } else {
          var msg = owner[errorName]();

          if (msg) {
            list.push({ propertyName: propertyName, errorMessage: msg });
          }
        }
      });

      return list;
    }

    _.each(propertyNames, function(propertyName) {
      var errorName = transformPropertyName(propertyName);

      if (multiple) {
        owner[errorName] = ko.observableArray([]);
      } else {
        owner[errorName] = ko.observable(null);
      }
    });

    owner.errorPropertyCount = ko.observable(0);
    owner.hasErrors = ko.computed(function() { return owner.errorPropertyCount() != 0; });
    owner.clearErrors = clearErrors;
    owner.addError = addError;
    owner.mapErrors = mapErrors;
    owner.listErrors = listErrors;
  }

  function defaultTransformPropertyName(n) { return n + "_error"; }

  ns.TrackErrors = TrackErrors;
})(jsu.ko);
