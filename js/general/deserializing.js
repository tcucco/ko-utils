// Deserialize a list of JSON objects that were serialized into a CSV-like format.

(function(ns) {
  "use strict";

  function getDeserializer(keys) {
    var body = "var obj = {};\n"
    for (var i = 0; i < keys.length; ++i) {
      body += "obj['" + keys[i].replace("'", "\\'") + "'] = data[" + i + "];\n";
    }
    body += "return obj;";
    return new Function("data", body);
  }

  function deserialize(keys, data) {
    var deserializer = getDeserializer(keys);
    var objs = [];
    for (var i = 0; i < data.length; ++i) {
      objs.push(deserializer(data[i]));
    }
    return objs
  }

  // Export
  ns.getDeserializer = getDeserializer;
  ns.deserialize = deserialize;
})(jsu.util);
