// A group of helpful array functions. Many of these are designed to work transparently with
// ko.ObservableArray or native arrays.

(function(ns) {
  "use strict";

  function indexOf(array, item) {
    // Allows us to use ko observable arrays.
    if ("indexOf" in array) {
      return array.indexOf(item);
    } else {
      return _.indexOf(array, item);
    }
  }

  function arrayLength(array) {
    // Allows us to use ko observable arrays
    return _.isFunction(array) ? array().length : array.length;
  }

  function arrayInsert(array, item, pivot, where) {
    if (where === undefined) { where = "before"; }

    var oi = indexOf(array, item);

    if (oi >= 0) { array.splice(oi, 1) };

    var pi = pivot === undefined ? -1 : indexOf(array, pivot);

    if (pi == -1) {
      if (where == "before") {
        array.unshift(item);
        return 0;
      } else {
        array.push(item);
        return arrayLength(array) - 1;
      }
    } else if (where == "before") {
      array.splice(pi, 0, item);
      return pi;
    } else {
      if (pi + 1 == arrayLength(array)) {
        array.push(item);
      } else {
        array.splice(pi + 1, 0, item);
      }
      return pi + 1;
    }
  }

  function _arrayRemove(array, item) {
    var i = indexOf(array, item);
    if (i != -1) {
      array.splice(i, 1);
    }
    return i;
  }

  // Remove an item from an array. If item is an array, then remove each of its members from the
  // target array.
  function arrayRemove(array, item) {
    if (_.isArray(item)) {
      return _.map(item, function(i) { return _arrayRemove(array, i); });
    } else {
      return _arrayRemove(array, item);
    }
  }

  // Add one array to another. If the `at` parameter is given, splice the values array into the
  // target list rather than just pushing to the end.
  function arrayExtend(to, values, at) {
    if (at === undefined) {
      to.push.apply(to, values);
    } else {
      to.splice.apply(to, [at, 0].concat(values));
    }
    return to;
  }

  // Replaces one item in an array with another.
  function arrayReplace(array, item, replaceWith) {
    var i = indexOf(array, item);
    if (i != -1) {
      array.splice(i, 1, replaceWith);
      return true;
    }
    return false;
  }

  // This method returns an ordinal for an item to be inserted in a list of ordinaled items.
  //
  // Suppose you have a list of objects of the from {name:value, ordinal:number}. Where the ordinal
  // value is a number that indicates the order the values should be presented. If you're adding a
  // new item to the list, you want its ordinal to be such that:
  // list[idx-1].ordinal < newOrdinal < list[idx+1].ordinal.
  //
  // This method handles that.
  function getNewOrdinal(list, atIndex, getOrdinal) {
    if (list.length == 0) {
      return 0;
    }

    if (atIndex == 0) {
      return getOrdinal(list[0]) - 1;
    }

    if (atIndex >= list.length) {
      return getOrdinal(list[list.length - 1]) + 1;
    }

    var pi = getOrdinal(list[atIndex - 1]);
    var ni = getOrdinal(list[atIndex]);
    return pi + (ni - pi) / 2;
  }

  ns.arrayInsert = arrayInsert;
  ns.arrayRemove = arrayRemove;
  ns.arrayExtend = arrayExtend;
  ns.arrayReplace = arrayReplace;
  ns.getNewOrdinal = getNewOrdinal;
})(jsu.util);
