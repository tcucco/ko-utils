(function(ns) {
  var defaultBreakpoints = [[768, "xs"], [992, "sm"], [1200, "md"], [Infinity, "lg"]];
  var hasRegisteredSize = false;

  function defaultGetWindowSize() {
    return window.innerWidth;
  }

  function getSizeName(breakpoints, length) {
    for (var i = 0; i < breakpoints.length; ++i) {
      if (length < breakpoints[i][0]) {
        return breakpoints[i][1];
      }
    }
    return undefined;
  }

  function getWindowSizeName(breakpoints, windowSize) {
    return getSizeName(breakpoints || defaultBreakpoints, windowSize || window.innerWidth);
  }

  function notifyOnWindowSizeChanged(callback, breakpoints, getWindowSize) {
    if (hasRegisteredSize) {
      throw "getWindowSizeName has already been registered";
    }
    breakpoints = breakpoints || defaultBreakpoints;
    getWindowSize = getWindowSize || defaultGetWindowSize;
    var lastWindowSizeName = getWindowSizeName(breakpoints, getWindowSize());
    jsu.util.registerEvent(window, "resize", function() {
      var windowSize = getWindowSize();
      var newWindowSizeName = getWindowSizeName(breakpoints, windowSize);
      if (newWindowSizeName != lastWindowSizeName) {
        callback(newWindowSizeName, lastWindowSizeName, windowSize);
        lastWindowSizeName = newWindowSizeName;
      }
    });
    return lastWindowSizeName;
  }

  function getOrientationName(unknown) {
    if ("orientation" in window) {
      switch (window.orientation) {
      case 90:
      case -90:
        return "landscape";
      default:
        return "portrait";
      }
    } else {
      return unknown;
    }
  }

  function notifyOnWindowOrientationChanged(callback, unknown) {
    if ("onorientationchange" in window) {
      jsu.util.registerEvent(window, "orientationchange", function() { callback(getOrientationName(unknown)); });
    }
    return getOrientationName(unknown);
  }

  ns.getSizeName = getSizeName;
  ns.notifyOnWindowSizeChanged = notifyOnWindowSizeChanged;
  ns.breakpoints = defaultBreakpoints;

  ns.getOrientationName = getOrientationName;
  ns.notifyOnWindowOrientationChanged = notifyOnWindowOrientationChanged;
})(jsu.util);
