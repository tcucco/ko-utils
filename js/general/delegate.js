// Contains a function for causing a method call on one object to be delegated to another object.

(function(ns) {
  "use strict";

  function _delegate(from, to, methodName) {
    from[methodName] = function() { return to[methodName].apply(to, arguments); };
  }

  function delegate() {
    var from = arguments[0]
    var to = arguments[1]
    for (var i = 2; i < arguments.length; ++i) {
      _delegate(from, to, arguments[i]);
    }
  }

  // Export
  ns.delegate = delegate;
})(jsu.util);
