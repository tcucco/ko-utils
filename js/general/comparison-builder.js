// Contains functionality to build comparison functions (for use in array.sort).

// DEPENDENCY: underscore

(function(ns) {
  "use strict";

  // args can either be:
  // {
  //   nullOrder: "first"|"last",
  //   field: string|array
  //   dir: 1|-1
  // }
  // or a string, which will be considered as the field in the above object.
  // If field is an array, the sort is a multi-level sort.
  function getComparer(args) {
    if (_.isString(args)) {
      args = { field: args };
    }
    args = _.extend({}, getCmpDefaultArgs, args);
    var body;
    if (args.field && _.isArray(args.field)) {
      var _arg = _.extend({}, args);
      var cmps = [];
      for (var i = 0; i < args.field.length; ++i) {
        _arg.field = args.field[i];
        cmps.push(getCmp(_arg, i == 0));
      }
      body = cmps.join("\n");
    } else {
       body = getCmp(args, true);
    }

    body += "\nreturn 0;";

    return new Function("a", "b", body);
  }

  var cmpDebug = false;

  function getCmp(args, declare) {
    args = _.extend({}, args);
    var fieldDefs;

    if (args.field !== undefined && args.field !== null) {
      var chain = args.field.toString().split(".");
      fieldDefs = [];
      for (var i = 0; i < chain.length; ++i) {
        fieldDefs.push(parseFieldDef(chain[i]));
      }

      if (!args.dir) {
        args.dir = fieldDefs[0].dir;
      }
    }

    var extractionA = getExtraction("a", "av", fieldDefs);
    var extractionB = getExtraction("b", "bv", fieldDefs);

    var nullDir = args.nullOrder == "first" ? -1 : 1;
    var nullHandling = "if (av === null && bv === null) { return 0; } else { if (av === null) { return " + nullDir  + "; } if (bv === null) { return " + -nullDir + "; } }";
    var nonNullHandling = "if (av < bv) { return " + -args.dir + "; } if (bv < av) { return " + args.dir + "; }"

    var rv = "";

    if (declare) {
      rv = "var av, bv;\n";
    }

    if (cmpDebug) {
      var debug = "console.log(av + ' <=> ' + bv);"
      return rv + extractionA + "\n" + extractionB + "\n" + debug + "\n" + nullHandling + "\n" + nonNullHandling;
    } else {
      return rv + extractionA + "\n" + extractionB + "\n" + nullHandling + "\n" + nonNullHandling;
    }
  }

  function getExtraction(from, to, fieldDefs) {
    var l1 = to + " = "; // + from;
    var rhs = from;
    var pathParts = [];
    if (fieldDefs) {
      for (var i = 0; i < fieldDefs.length; ++i) {
        rhs += "['" + fieldDefs[i].field + "']";
        if (fieldDefs[i].unwrap) {
          rhs += "()";
        }
        pathParts.push(rhs);
      }
    }
    l1 = l1 + pathParts.join(" && ");
    var l2 = to + " = (" + to + " === null || " + to + " === undefined) ? null : " + to + ".valueOf();";
    return l1 + ";\n" + l2;
  }

  function parseFieldDef(f) {
    var rv = {
      dir: 1,
      field: null,
      unwrap: false
    };

    var dir = f[0];
    var substringStart = 0;
    var substringEnd = f.length;

    if (dir == "-") {
      substringStart = 1;
      rv.dir = -1;
    } else if (dir == "+") {
      substringStart = 1;
      rv.dir = 1;
    }

    if (f.slice(-2) == "()") {
      substringEnd -= 2;
      rv.unwrap = true;
    }

    rv.field = f.slice(substringStart, substringEnd);

    return rv;
  }

  var getCmpDefaultArgs = {
    nullOrder: "first",
    field: null,
    dir: null
  };

  // Export
  ns.getComparer = getComparer;
})(jsu.util);
