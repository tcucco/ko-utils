// DEPENDENCY: jQuery

(function(ns) {
  "use strict";

  function clearTextSelection() {
    if (window.getSelection) {
      window.getSelection().removeAllRanges();
    } else if (document.selection) {
      document.selection.empty();
    }
  }

  // This function makes it so you hold down the mouse button and drag it over checkboxes to toggle
  // them. It makes selecting or unselecting a series of checkboxes much quicker.
  function enableDragToggle(containerClass) {
    var mouseIsDown = false;
    var isInContainer = false;

    function isInToggleContainer(target) {
      var n = target;
      while (n) {
        if (n.className && n.className.indexOf(containerClass) != -1) {
          return true;
        }
        n = n.parentNode;
      }
      return false;
    }

    ns.registerEvent(document, "mousedown", function(e) {
      mouseIsDown = true;
      isInContainer = isInToggleContainer(e.target);
    });

    ns.registerEvent(document, "mousemove", function(e) {
      if(mouseIsDown && isInContainer) {
        clearTextSelection();
        e.preventDefault();
      }
    });

    ns.registerEvent(document, "mouseup", function(e) {
      mouseIsDown = false;
      isInContainer = false;
    });

    ns.registerEvent(document, "mouseover", function(e) {
      if (mouseIsDown) {
        isInContainer = isInContainer || isInToggleContainer(e.target);
        if (e.target.nodeName == "INPUT" && e.target.type == "checkbox" && isInContainer && !e.target.disabled) {
          e.target.checked = !e.target.checked;
          $(e.target).triggerHandler("click");
        }
      }
    });
  }

  // Export
  ns.enableDragToggle = enableDragToggle;
})(jsu.util);
