(function(ns) {
  "use strict";

  var reservedChars = "\\^$*+?.()[]=!{},";

  // This function will escape all reserved regex characters except those (if any) passed into
  // okChars. This can be useful for building traditional wildcard search strings, for instance.
  function getRegexSafeText(text, okChars) {
    return text.replace(new RegExp("(\\" + reservedChars.replace(okChars || "", "").split("").join("|\\") + ")", "g"), "\\$1");
  }

  function getRegexReservedChars() {
    return reservedChars;
  }

  // Export
  ns.getRegexSafeText = getRegexSafeText;
  ns.getRegexReservedChars = getRegexReservedChars;
})(jsu.util);
