// DEPENDENCY: jQuery

(function(ns) {
  "use strict";

  var registrations = [];

  // For registering events that will live until the window goes out of scope.
  // They will all be deregistered when the window unloads.
  function registerEvent(element, eventName, handler) {
    $(element).on(eventName, handler);
    registrations.push([element, eventName, handler]);
  }

  function deregisterEvents() {
    for (var i = 0; i < registrations.length; ++i) {
      var r = registrations[i];
      $(r[0]).off(r[1], r[2]);
    }
  }

  registerEvent(window, "unload", function() { deregisterEvents(); });

  // Export
  ns.registerEvent = registerEvent;
})(jsu.util);
