// DEPENDENCY: underscore

(function(ns) {
  "use strict";

  // If obj is a string:
  // - if obj ends with (), then return subj[obj]()
  // - otherwise return subj[obj]
  // Otherwise treat obj as a function and return obj(subj);
  function indexOrCall(subj, obj) {
    if (_.isString(obj)) {
      if (subj === null || subj === undefined) {
        return subj;
      }
      if (obj.substring(obj.length - 2) == "()") {
        return subj[obj.substring(0, obj.length - 2)]();
      } else {
        return subj[obj];
      }
    } else if (_.isNumber(obj)) {
      return subj[obj];
    } else {
      return obj(subj);
    }
  }

  function indexObjects(objects, getKey) {
    getKey = getKey || "id";
    return _.chain(objects).map(function(o) { return [indexOrCall(o, getKey), o]; }).object().value();
  }

  function selectObjects(objects, keys, getKey, definedOnly) {
    if (definedOnly === undefined) { definedOnly = true; }
    var idx = indexObjects(objects, getKey);
    var sel = _.map(keys, function(k) { return idx[k]; });
    if (definedOnly) {
      return _.filter(sel, function(s) { return s !== undefined; });
    } else {
      return sel;
    }
  }

  var howRe = /(.*?)(=|\[\]|\(\)=|\(\)\[\])?$/;

  function assignValue(assigner, assignee, how) {
    if (!how) { return; }
    if (_.isString(how)) {
      // Looks for one of: field=, field[], field()=, field()[]
      var matches = howRe.exec(how);
      var field = matches[1];
      var method = matches[2];

      switch(method) {
        case "=":
          assigner[field] = assignee;
          break;
        case "[]":
          if (!assigner[field]) { assigner[field] = []; }
          assigner[field].push(assignee);
          break;
        case "()=":
          assigner[field](assignee);
          break;
        case "()[]":
          if (!assigner[field]()) { assigner[field]([]); }
          assigner[field].push(assignee);
          break;
        default:
          throw new Error("Unknown assignment method: " + how);
      }
    } else {
      how(assigner, assignee);
    }
  }

  // args
  // - [REQ] parents
  // - [REQ] children
  // - [REQ] getIdOfParent: (parent) -> id
  // - [REQ] getParentId: (child) -> parentId
  // - assignChild: '=', '[]', '()=', '()[]', or function(parent, child)
  // - assignParent: '=', '[]', '()=', '()[]', or function(child, parent)
  // - strict: raise an error if parent not found.
  function joinObjects(args) {
    var parentMapping = indexObjects(args.parents, args.getIdOfParent);
    for (var i = 0; i < args.children.length; ++i) {
      var child = args.children[i];
      var parentId = indexOrCall(child, args.getParentId);
      if (parentId === undefined) { continue; }
      var parent = parentMapping[parentId];
      if (parent) {
        assignValue(parent, child, args.assignChild);
        assignValue(child, parent, args.assignParent);
      } else {
        if (parentId === null) {
          // Do nothing.
        } else if (args.strict) {
          throw new Error("Parent with id {0} not found".format(parentId));
        }
      }
    }
  }

  // args
  // - [REQ] parents
  // - [REQ] children
  // - [REQ] joiners
  // - [REQ] getIdOfParent
  // - [REQ] getIdOfChild
  // - [REQ] getParentId
  // - [REQ] getChildId
  // - assignChild: '=', '[]', '()=', '()[]', or function(parent, child)
  // - assignParent: '=', '[]', '()=', '()[]', or function(child, parent)
  function joinObjectsThrough(args) {
    var parentMapping = indexObjects(args.parents, args.getIdOfParent);
    var childMapping = indexObjects(args.children, args.getIdOfChild);
    for (var i = 0; i < args.joiners.length; ++i) {
      var joiner = args.joiners[i];
      var parentId = indexOrCall(joiner, args.getParentId);
      var childId = indexOrCall(joiner, args.getChildId);
      if (parentId === null || childId === null || parentId === undefined || childId === undefined) { continue; }
      var parent = parentMapping[parentId];
      var child = childMapping[childId];
      if (parent === null || parent === undefined || child === null || child === undefined) { continue; }
      assignValue(parent, child, args.assignChild);
      assignValue(child, parent, args.assignParent);
    }
  }

  // Export
  ns.indexOrCall = indexOrCall;
  ns.indexObjects = indexObjects;
  ns.assignValue = assignValue;
  ns.selectObjects = selectObjects;
  ns.joinObjects = joinObjects;
  ns.joinObjectsThrough = joinObjectsThrough
})(jsu.util);
