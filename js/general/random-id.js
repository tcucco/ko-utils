(function(ns) {
  "use strict";

  var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz+/";

  function toChar(i) {
    if (i < 0) {
      throw new Error("Index must be in [0, " + (chars.length - 1) + "]");
    }
    if (i > chars.length - 1) {
      throw new Error("Cannot handle values greater than " + chars.length);
    }
    return chars.charAt(i);
  }

  function getRandomId(len, base) {
    base = Math.min(Math.max(Math.abs(base || 64), 2), 64);
    var codes = [];
    for (var i = 0; i < len; ++i) {
      codes.push(toChar(Math.floor(Math.random() * base)));
    }
    return codes.join("");
  }

  var uuidIds = "89AB";

  function getRandomUUID() {
    var randId = uuidIds.charAt(Math.floor(Math.random() * 4));
    return [
      getRandomId(8, 16),
      getRandomId(4, 16),
      "4" + getRandomId(3, 16),
      randId + getRandomId(3, 16),
      getRandomId(12, 16)
    ].join("-");
  }

  function getRandomIdSpace(base) {
    var base = base || 64;
    return chars.slice(0, base);
  }

  ns.getRandomId = getRandomId;
  ns.getRandomUUID = getRandomUUID;
  ns.getRandomIdSpace = getRandomIdSpace;
})(jsu.util);
