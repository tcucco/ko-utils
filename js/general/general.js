(function(ns) {
  "use strict";

  function parseQueryString(qs, skipDecode) {
    // Parse a query string into an object. If a key occurs multiple times the
    // the value will be an array holding the values in the order the appear.

    var result = {};
    var qsArgs = qs.split("&");

    for (var i = 0; i < qsArgs.length; ++i) {
      var p = qsArgs[i].split("=");
      var k = p[0];
      var v = p.length > 1 ? p[1] : "";

      if (!skipDecode) { v = decodeURIComponent(v); }

      if (!(k in result)) {
        result[k] = v;
      }
      else if (result[k].push) {
        result[k].push(v);
      }
      else {
        result[k] = [result[k], v];
      }
    }

    return result;
  }

  function parseCookies(container) {
    // Parse cookies into an object. Container must be an object with a cookies
    // property, it defaults to document.

    var cookies = (container || document).cookie.split(";");
    var result = {};
    for (var i = 0; i < cookies.length; ++i) {
      var cookie = cookies[i].split("=");
      result[trimString(cookie[0])] = trimString(cookie[1]);
    }
    return result;
  }

  function setCookie(name, value, opts, container) {
    // Set a cookie's value. Also set expires, domain and path if they
    // specified in opts. Container must be an object with a cookies property,
    // it defaults to document.

    opts = opts || {};
    var c = name + "=" + value;
    if (opts.expires) { c += " expires=" + opts.expires.toString(); }
    if (opts.domain) { c += " domain=" + opts.domain.toString(); }
    if (opts.path) { c += " path=" + opts.path.toString(); }

    (container || document).cookie = c;
  }

  function deleteCookie(name, opts, container) {
    // Delete a cookie by setting its value to empty, and setting its expires
    // date to the unix epoch. Also set domain and path if specified in opts.
    // Container must be an object with a cookies property, it defaults to
    // document.

    opts = opts || {};
    var c = name + "=; expires=Thu, 01 Jan 1970 00:00:00 GMT;";
    if (opts.domain) { c += " domain=" + opts.domain.toString(); }
    if (opts.path) { c += " path=" + opts.path.toString(); }

    (container || document).cookie = c;
  }

  function extractXhrError(xhr, status, error) {
    // Given the jQuery ajax error callback args, extract a sensible error
    // message.

    var contentType = xhr.getResponseHeader("content-type");
    var errorMessage = null;

    if (xhr.status == 0) {
      errorMessage = "We were unable to contact the server. It appears that you are offline or your internet connection is down. Please make sure you are connected and try again.";
    } else if (/^text\/plain/i.test(contentType)) {
      errorMessage = xhr.responseText;
    } else if (/^text\/html/i.test(contentType)) {
      errorMessage = error;
    } else if (/^application\/json/i.test(contentType) && "message" in xhr.responseJSON) {
      errorMessage = xhr.responseJSON["message"];
    } else {
      errorMessage = error;
    }

    return errorMessage;
  }

  function inherit(superclass, subclass) {
    // Alter a constructor's prototype chain. Uses Object.create when
    // available, and an older method when not.

    if (Object.create) {
      subclass.prototype = Object.create(superclass.prototype);
    } else {
      var proto = new superclass();
      var toDelete = [];

      for (var pn in proto) {
          if (proto.hasOwnProperty(pn)) {
              toDelete.push(pn);
          }
      }
      for (var i = 0; i < toDelete.length; ++i) {
         delete proto[toDelete[i]];
      }
      subclass.prototype = proto;
      proto.constructor = subclass;
    }
  }

  function makeBroadcaster(obj) {
    // Add methods to an object to allow it to broadcast messages to
    // subscribers

    var nextId = 1;
    var registrants = { };

    obj.on = function(name, handler) {
      // Add a handler for a named action. Returns an id that can be used to
      // unsubscribe.

      var id = nextId++;
      registrants[id] = [name, handler];
      return id;
    };

    obj.off = function(id) {
      // Remove a handler using the id returned from the on function. If id is
      // undefined, clear all registrants.

      if (id === undefined) {
        registrants = { };
        return true;
      } else {
        if (id in registrants) {
          delete registrants[id];
          return true;
        }
        return false;
      }
    }

    obj.trigger = function(name, args, sender) {
      // Send a message to all subscriptions on name with the given args. If
      // sender is not specified, it defaults to obj.

      for (var id in registrants) {
        var n = registrants[id][0];
        var h = registrants[id][1];
        if (n == name) {
          h(name, args, sender || obj);
        }
      }
    }
  }

  function getNS() {
    // Walk through a hierarchy from root, given as the first argument, through
    // children specified in the remaining arguments. If a child is not found
    // in its parent, create it as an empty object.

    var root = arguments[0];
    if (arguments.length == 0) { return root; }
    var current = root;
    for (var i = 1; i < arguments.length; ++i) {
      var next = arguments[i].toString();
      if (!(next in current)) { current[next] = {}; }
      current = current[next];
    }
    return current;
  }

  function getProperty(root, chain) {
    // Get a deeply nested property identified by chain starting at root.
    // chain format is: id0.id1.(...).idn
    // where each id is the name of a property on the preceeding object. Each
    // id can optionally be suffixed with '()' to cause the node to be
    // called as a function before continuing down the chain.

    var n = root;
    var c = null;

    if (_.isArray(chain)) {
      c = chain;
    } else {
      c = chain.split(".");
    }

    for (var i = 0; i < c.length; ++i) {
      var p = c[i];

      if (p.slice(-2) == "()") {
        n = n[p.slice(0, -2)]();
      } else {
        n = n[p];
      }

      if (!isPresent(n)) {
        break;
      }
    }

    return n;
  }

  var trimStringRe = /^\s*|\s*$/g;
  function trimString(s) {
    // Remove trailing and leading spaces from a string.

    if (s) {
      return s.replace(trimStringRe, "");
    } else {
      return s;
    }
  }

  var isBlankRe = /^\s+$/;
  function isBlank(o) {
    // Returns true if a string is not present, has zero length, or is all
    // space characters.

    var notEmpty = o && o.length && !isBlankRe.test(o);
    return !notEmpty;
  }

  function _isPresent(v) {
    // Returns true if v is not null and not undefined.

    return v !== null && v !== undefined;
  }

  function isPresent() {
    for (var i = 0; i < arguments.length; ++i) {
      if (!_isPresent(arguments[i])) {
        return false;
      }
    }
    return true;
  }

  function join(actions, callback, afterIntermediate) {
    // Given a list of async actions that call a callback when done, call each
    // action and when all have completed call callback.

    var completed = 0;
    var required = actions.length;

    function _callback() {
      if (++completed == required) {
        callback();
      } else if (afterIntermediate) {
        afterIntermediate(completed - 1);
      }
    }

    for (var i = 0; i < actions.length; ++i) {
      actions[i](_callback);
    }
  }

  function serialize(actions, callback, afterIntermediate) {
    // Given a list of async actions that call a callback when done, call each
    // action and when its calls its callback, call the next item. When all
    // have completed, call callback.

    var i = 0;

    function callback_() {
      if (i == actions.length) {
        if (callback) {
          callback();
        }
      } else {
        if (afterIntermediate && i > 0) {
          afterIntermediate(i);
        }
        actions[i++](callback_);
      }
    }

    callback_();
  }

  function queue(action, timeout) {
    // Request an action to be queued up after all pending actions have
    // completed.

    return setTimeout(action, timeout || 0);
  }

  function compareArrays(al, ar, strict, orderIndependent) {
    // Compares the two arrays item-by-item. If strict is true, '===' is used, otherwise '==' is
    // used. If orderIndependent is true, copies of the arrays, sorted, are compared.
    //
    // NOTE: Only works for scalar values.

    if (!_.isArray(al) || !_.isArray(ar)) { return false; }
    if (al === ar) { return true; }
    if (al.length != ar.length) { return false; }

    if (orderIndependent) {
      al = al.slice();
      al.sort();
      ar = ar.slice();
      ar.sort();
    }

    for (var i = 0; i < al.length; ++i) {
      if (strict) {
        if (al[i] !== ar[i]) {
          return false;
        }
      } else {
        if (al[i] != ar[i]) {
          return false;
        }
      }
    }

    return true;
  }

  function compareObjects(ol, or, strict, orderIndependent) {
    // Compares to objects.
    //
    // If the objects are arrays, calls compareArrays. If the objects are scalars, compares them
    // using '==' if strict is true, or '===' if strict is not true. Otherwise each key/value in ol
    // is compared to each in or using compareObjects.

    if (!(jsu.util.isPresent(ol) && jsu.util.isPresent(or))) { return ol === or; }
    if (ol === or) { return true; }

    if (_.isArray(ol)) {
      return compareArrays(ol, or, strict, orderIndependent);
    } else if (_.isObject(ol)) {
      if (!_.isObject(or)) {
        return false;
      }
      if (_.keys(ol).length != _.keys(or).length) {
        return false;
      }

      for (var k in ol) {
        if (!(k in or)) {
          return false;
        }
        var vl = ol[k];
        var vr = or[k];

        if (!compareObjects(vl, vr, strict, orderIndependent)) {
          return false;
        }
      }
    } else {
      if (strict) {
        if (ol !== or) {
          return false;
        }
      } else {
        if (ol != or) {
          return false;
        }
      }
    }

    return true;
  }

  var localStorage = typeof window == "undefined" ? null : window.localStorage;

  function localStorageRead(key, deserialize) {
    // Get a value from local storage (if available), optionally deserializing
    // it with the given deserializer.

    if (!(key && localStorage)) {
      return undefined;
    }
    var v = localStorage.getItem(key);
    if (v === null) {
      return undefined;
    }
    return deserialize ? deserialize(v) : v;
  }

  function localStorageWrite(key, value, serialize) {
    // Write a value to local storage (if available), optionally serializing it
    // with the given serializer.

    if (key && localStorage) {
      if (value === undefined || value === null) {
        localStorage.removeItem(key)
      } else {
        localStorage.setItem(key, serialize ? serialize(value) : value);
      }
    }
  }

  function clearLocalStorage(key) {
    // Delete a key from local storage.

    if (key === undefined) {
      localStorage.clear();
    } else if (_.isArray(key)) {
      _.each(key, function(k) { localStorage.removeItem(k); });
    } else {
      localStorage.removeItem(key);
    }
  }

  function validateLocalStorage(localStorageVersion, versionKey) {
    // Check that the value in localStorage[versionKey] == localStorageVersion,
    // and if not, clear out all of local storage. This makes it easy to clear
    // local storage when you make large changes to how you store things.

    versionKey = versionKey || "localStorageVersion";
    var currentValue = localStorage.getItem(versionKey);
    if (currentValue != localStorageVersion) {
      localStorage.clear();
      localStorage.setItem(versionKey, localStorageVersion);
    }
  }

  var abbreviateRe = /(?:^|\s)(\w[A-Z0-9]*)/g;
  function abbreviate(s, sep) {
    // Abbreviates a string to its initials.

    var p = "";
    sep = (sep === undefined || sep === null) ? "" : sep;
    while (true) {
      var m = abbreviateRe.exec(s);
      if (m) {
        p += m[1] + sep;
      } else {
        break;
      }
    }
    return p;
  }

  // Export
  ns.parseQueryString = parseQueryString;
  ns.parseCookies = parseCookies;
  ns.setCookie = setCookie;
  ns.deleteCookie = deleteCookie;
  ns.extractXhrError = extractXhrError;
  ns.inherit = inherit;
  ns.makeBroadcaster = makeBroadcaster;
  ns.getNS = getNS;
  ns.getProperty = getProperty;
  ns.trimString = trimString;
  ns.isBlank = isBlank;
  ns.isPresent = isPresent;
  ns.join = join;
  ns.serialize = serialize;
  ns.queue = queue;
  ns.compareArrays = compareArrays;
  ns.compareObjects = compareObjects;
  ns.localStorageWrite = localStorageWrite;
  ns.localStorageRead = localStorageRead;
  ns.clearLocalStorage = clearLocalStorage;
  ns.validateLocalStorage = validateLocalStorage;
  ns.abbreviate = abbreviate;
})(jsu.util);
