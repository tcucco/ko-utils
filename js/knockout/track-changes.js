// DEPENDENCY: knockout
// DEPENDENCY: underscore

(function(ns) {
  "use strict";

  function TrackChanges(owner, properties, opts) {
    opts = _.extend({}, defaultOpts, opts);

    if ("hasChanges" in owner) {
      throw new Error("Change tracking can only be set once for an object");
    }

    var originalValues = { };
    var changeTrackedChildren = { };
    var subscriptions = { };

    function resetChangedFlags() {
      _.each(properties, function(p) {
        if (!(p in changeTrackedChildren)) {
          owner[opts.transformPropertyName(p)](false);
        }
      });
      owner.changedPropertyCount(0);
    }

    function getOriginalValue(propertyName) {
      var v = owner[propertyName]();

      // Create a copy of the array, otherwise changes to the
      // array in place will never cause changes to be triggered.
      if (_.isArray(v)) {
        return v.slice();
      } else {
        return v;
      }
    }

    function setOriginalValues() {
      originalValues = _.object(_.map(properties, function(p) {
        return [p, getOriginalValue(p)];
      }));
    }

    function getChangeSubscription(propertyName, changedPropertyName) {
      function subscription(newValue) {
        var oldChangedValue = owner[changedPropertyName]();
        var isSame = jsu.util.compareObjects(newValue, originalValues[propertyName], opts.strict);
        var newChangedValue = !isSame;

        owner[changedPropertyName](newChangedValue);

        if (oldChangedValue != newChangedValue) {
          // If the property is going from being changed to not being changed, we need to reduce the
          // number of changed properties. If it is going from not being changed to being changed,
          // we need to increase the number of changed properties.
          var delta = oldChangedValue ? -1 : 1;
          owner.changedPropertyCount(owner.changedPropertyCount() + delta);
        }

        if (opts.propertyObserver) {
          opts.propertyObserver(owner, propertyName);
        }
      }

      return subscription;
    }

    function trackProperty(propertyName, changedPropertyName) {
      if (!(owner[propertyName] && owner[propertyName].subscribe)) {
        throw new Error("Cannot change track " + propertyName);
      }

      if (propertyName in subscriptions) {
        throw new Error("Already tracking " + propertyName);
      }

      owner[changedPropertyName] = ko.observable(false);

      var subscriptionId = owner[propertyName].subscribe(getChangeSubscription(propertyName, changedPropertyName));

      subscriptions[propertyName] = subscriptionId;
    }

    function addChangeTracking(propertyName) {
      var changedPropertyName = opts.transformPropertyName(propertyName);
      trackProperty(propertyName, changedPropertyName);
      originalValues[propertyName] = getOriginalValue(propertyName);
    }

    function removeChangeTracking(propertyName) {
      if (!(propertyName in subscriptions)) {
        throw new Error("Not tracking " + propertyName);
      }

      var changedPropertyName = opts.transformPropertyName(propertyName);

      if (owner[changedPropertyName]()) {
        owner.changedPropertyCount(owner.changedPropertyCount() - 1);
      }

      subscriptions[propertyName].dispose();

      delete owner[changedPropertyName];
      delete originalValues[propertyName];
      delete subscriptions[propertyName];
    }

    function saveChanges(data, skipUpdate) {
      // If owner implements a saved function, it will be passed data to update its values. This can
      // be used to allow models to set autoincremented ids returned from the server, or timestamps,
      // etc.
      if (data && opts.savedMethodName in owner && !skipUpdate) {
        owner[opts.savedMethodName](data);
      }

      setOriginalValues();

      for (var k in changeTrackedChildren) {
        changeTrackedChildren[k].saveChanges(data && data[k] || null, skipUpdate);
      }

      resetChangedFlags();
    }

    function revertChanges() {
      for (var k in originalValues) {
        if (k in changeTrackedChildren) {
          changeTrackedChildren[k].revertChanges();
        } else {
          var n = k;
          var v = originalValues[k];

          if (_.isArray(v)) {
            v = v.slice();
          }

          owner[n](v);
        }
      }

      resetChangedFlags();
    }

    function setup() {
      for (var i = 0; i < properties.length; ++i) {
        var propertyName = properties[i];
        if (!(propertyName in owner)) {
          throw new Error("Property name {0} not found".format(propertyName));
        }
        var currentValue = owner[propertyName]();

        if (currentValue && currentValue.hasChanges) {
          changeTrackedChildren[propertyName] = currentValue;
        } else {
          var changedPropertyName = opts.transformPropertyName(propertyName);

          if (changedPropertyName in owner) {
            throw new Error(changedPropertyName + " already exists.");
          }

          trackProperty(propertyName, changedPropertyName);
        }
      }
    }

    setup();
    setOriginalValues();

    owner.addChangeTracking = addChangeTracking;
    owner.removeChangeTracking = removeChangeTracking;
    owner.changedPropertyCount = ko.observable(0);
    owner.hasChanges = ko.computed(function() {
      for (var k in changeTrackedChildren) {
        if (changeTrackedChildren[k].hasChanges()) {
          return true;
        }
      }
      return owner.changedPropertyCount() != 0;
    });
    owner.saveChanges = saveChanges;
    owner.revertChanges = revertChanges;
  }

  function defaultTransformPropertyName(n) { return n + "_changed"; }

  var defaultOpts = {
    strict: false,
    transformPropertyName: defaultTransformPropertyName,
    propertyObserver: null,
    savedMethodName: "saved"
  };
  ns.TrackChanges = TrackChanges;
})(jsu.ko);
