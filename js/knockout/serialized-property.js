// DEPENDENCY: knockout
// DEPENDENCY: utils/general

(function(ns) {
  "use strict";

  // SerializedProperty(owner, name, args)
  // SerializedProperty(owner, nameArray, args)
  function SerializedProperty(owner, nameOrNames, args) {
    if (_.isArray(nameOrNames)) {
      _.each(nameOrNames, function(name) { _SerializedProperty(owner, name, args); });
    } else {
      _SerializedProperty(owner, nameOrNames, args);
    }
  }

  function _SerializedProperty(owner, name, args) {
    args = _.extend({}, serializedPropertyDefaultArgs, args);

    if (name in owner && args.initialValue === undefined) {
      args.initialValue = ko.unwrap(owner[name]);
    }

    if (!args.underlyingName) {
      args.underlyingName = args.transformName(name);
    }
    owner[args.underlyingName] = ko.observable(SerializedProperty.getInitialValue(args));

    if (args.extendObservable) {
      owner[args.underlyingName] = args.extendObservable(owner[args.underlyingName]);
    }

    owner[name] = ko.computed({
      owner: owner,
      read: function() { return args.deserialize(owner[args.underlyingName]()); },
      write: function(rawValue) {
        var newValue = args.serialize(rawValue);
        if (owner[args.underlyingName]() !== newValue) {
          var oldValue = owner[args.underlyingName]();

          // If willChange exists, its result will be the newValue. If it doesn't need to mutate the
          // new value, it can just return back newValue.
          if (args.willChange) {
            newValue = args.willChange(newValue, oldValue, name, owner, "willChange");
          }

          owner[args.underlyingName](newValue);

          if (args.didChange) {
            args.didChange(newValue, oldValue, name, owner, "didChange");
          }

          jsu.util.localStorageWrite(args.localStorageKey, newValue, args.localStorageSerializer);
        }
      }
    });
  }

  SerializedProperty.getInitialValue = function(args) {
    var v = undefined;
    var sis = args.skipInitialSerialization;

    if (args.localStorageKey) {
      v = jsu.util.localStorageRead(args.localStorageKey, args.localStorageDeserializer);
    }

    if (v !== undefined) {
      sis = true;
    } else {
      v = args.initialValue;

      if (v === undefined) {
        v = null;
      }
    }

    return sis ? v : args.serialize(v);
  }

  var serializedPropertyDefaultArgs = {
    underlyingName: null,
    transformName: function(n) { return "_" + n; },
    serialize: _.identity,
    deserialize: _.identity,
    initialValue: undefined,
    skipInitialSerialization: false,
    didChange: null,
    willChange: null,
    localStorageKey: null,
    localStorageSerializer: _.identity,
    localStorageDeserializer: _.identity,
    extendObservable: null
  };

  function IntegerProperty(owner, name, args) {
    args = _.extend({}, integerPropertyDefaultArgs, args);
    args.serialize = function(v) {
      v = parseInt(v);
      return isNaN(v) ? args.defaultValue : v;
    };
    args.localStorageDeserializer = function(v) { return parseInt(v); };

    SerializedProperty(owner, name, args);
  }

  var integerPropertyDefaultArgs = {
    defaultValue: null
  };

  function FloatProperty(owner, name, args) {
    args = _.extend({}, floatPropertyDefaultArgs, args);
    args.serialize = function(v) {
      v = parseFloat(v);
      return isNaN(v) ? args.defaultValue : v;
    };
    args.localStorageDeserializer = function(v) { return parseFloat(v); }

    SerializedProperty(owner, name, args);
  }

  var floatPropertyDefaultArgs = {
    defaultValue: null
  };

  function IntegerArrayProperty(owner, name, args) {
    args = _.extend({}, integerArrayPropertyDefaultArgs, args);
    if (args.sort) {
      args.serialize = function(v) {
        var v = _.map(v, toInt);
        v.sort();
        return v;
      };
    } else {
      args.serialze = function(v) { return _.map(v, toInt); };
    }
    args.deserialize = function(v) { return _.map(v, toInt); };

    SerializedProperty(owner, name, args);
  }

  var integerArrayPropertyDefaultArgs = {
    sort: false
  };

  function RoundedFloatProperty(owner, name, args) {
    args = _.extend({}, roundedFloatPropertyDefaultArgs, args);
    args.serialize = function(v) {
      v = parseFloat(v);

      if (isNaN(v)) {
        return args.defaultValue;
      } else {
        v = v.toFixed(args.scale);
        if (args.stripZerosBeyond && args.stripZerosBeyond < args.scale && /^0+$/.test(v.substring(v.length - args.stripZerosBeyond))) {
          return v.substring(0, v.length - args.stripZerosBeyond);
        } else {
          return v;
        }
      }
    };

    SerializedProperty(owner, name, args);
  }

  var roundedFloatPropertyDefaultArgs = {
    scale: 2,
    defaultValue: null,
    stripZerosBeyond: null
  };

  function SteppedFloatProperty(owner, name, args) {
    args = _.extend({}, steppedFloatPropertyDefaultArgs, args);
    args.serialize = function(v) {
      v = parseFloat(v);

      if (isNaN(v)) {
        return args.defaultValue;
      } else {
        var s = (Math.round(v / args.step) * args.step).toFixed(args.scale);
        if (args.stripTrailingZeros) {
          return s.replace(stripTrailingZerosRe, "");
        }
        return s;
      }
    };

    SerializedProperty(owner, name, args);
  }

  var steppedFloatPropertyDefaultArgs = {
    step: 1,
    scale: 0,
    defaultValue: null,
    stripTrailingZeros: true
  };

  var stripTrailingZerosRe = /\.?0+$/;

  function TimeProperty(owner, name, args) {
    args = _.extend({}, timePropertyDefaultArgs, args);
    args.deserialize = function(v) {
      if (v) {
        var m = moment(v);
        var fmt = _.isArray(args.timeFormat) ? args.timeFormat[0] : args.timeFormat;
        return m.format(fmt);
      } else {
        return v;
      }
    };
    args.serialize = function(v) {
      if (v) {
        var m = moment(v, args.timeFormat);
        if (m.isValid()) {
          return m.format();
        } else {
          return null;
        }
        return moment(v, args.timeFormat).format();
      } else {
        return null;
      }
    };

    SerializedProperty(owner, name, args);
  }

  var timePropertyDefaultArgs = {
    timeFormat: "hh:mm A",
    skipInitialSerialization: true
  };

  function toInt(v) {
    return parseInt(v);
  }

  // Export
  ns.SerializedProperty = SerializedProperty;
  ns.IntegerProperty = IntegerProperty;
  ns.FloatProperty = FloatProperty;
  ns.IntegerArrayProperty = IntegerArrayProperty;
  ns.RoundedFloatProperty = RoundedFloatProperty;
  ns.SteppedFloatProperty = SteppedFloatProperty;
  ns.TimeProperty = TimeProperty;
})(jsu.ko);
