(function(ns) {
  function Expandable(data) {
    if (!data) { data = {}; }

    var self = this;

    self.keys = ko.observableArray([]);

    function setup() {
      self.keys(_.keys(data).sort());

      for (var k in data) {
        if (k == "keys") {
          throw new Error("Expandable object cannot overwrite member 'keys'");
        }
        self[k] = ko.observable(data[k]);
      }

      jsu.ko.TrackChanges(self, self.keys());
    }

    setup();
  }

  Expandable.prototype.addAttribute = function(name, value) {
    var self = this;

    self[name] = ko.observable(value === undefined ? null : value);
    self.addChangeTracking(name);

    self.keys.splice(_.sortedIndex(self.keys(), name), 0, name);
  }

  Expandable.prototype.removeAttribute = function(name) {
    var self = this;

    self.removeChangeTracking(name);
    delete self[name];
    self.keys.splice(_.indexOf(self.keys(), name, true), 1);
  }

  Expandable.prototype.toJS = function() {
    var self = this;
    var data = {};

    _.each(self.keys(), function(k) {
      data[k] = self[k]();
    });

    return data;
  }

  ns.Expandable = Expandable;
})(jsu.ko);
