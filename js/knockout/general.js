// DEPENDENCY: knockout

(function(ns) {
  "use strict";

  function ToggleMethod(owner, methodName) {
    owner[methodName || "toggle"] = function(name) {
      owner[name](!owner[name]());
      return true;
    };
  }

  function SetValueMethod(owner, clearOnRepeatedSet) {
    owner.setValue = function(prop, val) {
      if (clearOnRepeatedSet && val === owner[prop]()) {
        owner[prop](null);
      } else {
        owner[prop](val);
      }
      return true;
    }
  }

  function OppositeProperty(owner, name, underlyingName, koComputedOpts) {
    owner[name] = ko.computed(_.extend({
      read: function() { return !owner[underlyingName](); },
      write: function(v) { owner[underlyingName](!v); }
    }, koComputedOpts));
  }

  function getMappingPair(basis) {
    function fromJS(data, target) {
      if (target === undefined) {
        target = {};
      }

      for (var k in basis) {
        var v = k in data ? data[k] : basis[k];

        if (_.isArray(v)) {
          target[k] = ko.observableArray(v);
        } else {
          target[k] = ko.observable(v);
        }
      }

      return target;
    }

    function toJS(target) {
      var js = {};

      for (var k in basis) {
        js[k] = ko.unwrap(target[k]);
      }

      return js;
    }

    return { fromJS: fromJS, toJS: toJS };
  }

  // Export
  ns.ToggleMethod = ToggleMethod;
  ns.SetValueMethod = SetValueMethod;
  ns.OppositeProperty = OppositeProperty;
  ns.getMappingPair = getMappingPair;
})(jsu.ko);
