// DEPENDENCY: knockout

(function(ns) {
  "use strict";

  function AjaxStatus(owner, timeout) {
    var timeoutHandle = null;

    owner._ajaxStatus = ko.observable(null);
    owner.ajaxStatus = ko.computed({
      read: owner._ajaxStatus,
      write: function(v) {
        if (owner._ajaxStatus() != v) {
          clearTimeout(timeoutHandle);
          owner._ajaxStatus(v);
          if (v && v != "loading" && timeout) {
            timeoutHandle = setTimeout(clearAjaxStatus, timeout);
          }
        }
      }
    });

    function clearAjaxStatus() {
      owner.ajaxStatus(null);
    }
  }

  // Export
  ns.AjaxStatus = AjaxStatus;
})(jsu.ko);
