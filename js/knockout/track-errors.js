// DEPENDENCY: knockout
// DEPENDENCY: underscore

(function(ns) {
  "use strict";

  function TrackErrors(owner, propertyNames, transformPropertyName, multiple) {
    if ("hasErrors" in owner) {
      throw new Error("Error tracking can only be set once for an object");
    }

    if (!transformPropertyName) {
      transformPropertyName = defaultTransformPropertyName;
    }

    function clearError(propertyName) {
      var errorName = transformPropertyName(propertyName);
      var changesErrorCount = false;

      if (multiple) {
        if (owner[errorName].length != 0) {
          owner[errorName]([]);
          changesErrorCount = true;
        }
      } else {
        if (owner[errorName]()) {
          owner[errorName](null);
          changesErrorCount = true;
        }
      }

      if (changesErrorCount) {
        owner.errorPropertyCount(owner.errorPropertyCount() - 1);
      }
    }

    function clearErrors(propertyName) {
      if (propertyName === undefined) {
        _.each(propertyNames, function(pn) { clearError(pn); });
      } else {
        clearError(propertyName);
      }
    }

    function addError(propertyName, errorMessage) {
      if (!errorMessage) { return; }

      var errorName = transformPropertyName(propertyName);
      var changesErrorCount = false;

      if (multiple) {
        if (owner[errorName]().length == 0) {
          changesErrorCount = true;
        }
        owner[errorName].push(errorMessage);
      } else {
        if (!owner[errorName]()) {
          changesErrorCount = true;
        }
        owner[errorName](errorMessage);
      }

      if (changesErrorCount) {
        owner.errorPropertyCount(owner.errorPropertyCount() + 1);
      }
    }

    function mapErrors(errors) {
      clearErrors();

      for (var propertyName in errors) {
        var errorMessage = errors[propertyName];

        if (_.isArray(errorMessage)) {
          if (multiple) {
            _.each(errorMessage, function(em) {
              addError(propertyName, em);
            });
          } else {
            addError(propertyName, _.last(errorMessage));
          }
        } else {
          addError(propertyName, errorMessage);
        }
      }
    }

    function listErrors() {
      var list = [];

      _.each(propertyNames, function(propertyName) {
        var errorName = transformPropertyName(propertyName);

        if (multiple) {
          _.each(owner[errorName](), function(msg) {
            list.push({ propertyName: propertyName, errorMessage: msg });
          });
        } else {
          var msg = owner[errorName]();

          if (msg) {
            list.push({ propertyName: propertyName, errorMessage: msg });
          }
        }
      });

      return list;
    }

    _.each(propertyNames, function(propertyName) {
      var errorName = transformPropertyName(propertyName);

      if (multiple) {
        owner[errorName] = ko.observableArray([]);
      } else {
        owner[errorName] = ko.observable(null);
      }
    });

    owner.errorPropertyCount = ko.observable(0);
    owner.hasErrors = ko.computed(function() { return owner.errorPropertyCount() != 0; });
    owner.clearErrors = clearErrors;
    owner.addError = addError;
    owner.mapErrors = mapErrors;
    owner.listErrors = listErrors;
  }

  function defaultTransformPropertyName(n) { return n + "_error"; }

  ns.TrackErrors = TrackErrors;
})(jsu.ko);
