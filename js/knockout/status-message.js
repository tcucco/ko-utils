// DEPENDENCY: knockout

(function(ns) {
  function StatusMessage(owner, timeouts) {
    var timeoutHandle = null;

    owner.statusMessageType = ko.observable(null);
    owner.statusMessage = ko.observable(null);

    owner.clearMessage = function() {
      owner.statusMessageType(null);
      owner.statusMessage(null);
    }

    owner.setMessage = function(type, message, timeout) {
      if (!(type && message)) {
        owner.clearMessage();
      }

      if (timeouts && timeout === undefined) {
        timeout = parseInt(timeouts[type]);
      }

      owner.statusMessageType(type);
      owner.statusMessage(message);

      clearTimeout(timeoutHandle);

      if (timeout && timeout > 0) {
        timeoutHandle = setTimeout(owner.clearMessage, timeout);
      }
    }
  }

  ns.StatusMessage = StatusMessage;
})(jsu.ko);
