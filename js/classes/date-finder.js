// DEPENDENCY: underscore

(function(ns) {
  "use strict";

  function shiftDate(dt, delta) {
    dt.add(delta);
    if ("quarters" in delta) {
      var months = delta.quarters * 3;
      dt.add("months", months);
    }
    return dt;
  }

  function rangeIncludes(dr, d) {
    var lb = dr[0].valueOf();
    var ub = dr[1].valueOf();
    var v = d.valueOf();
    return v >= lb && v <= ub;
  }

  // if what should be either "date" or "moment". Default is "date"
  function getDate(dt, what) {
    // NOTE: Commented out lines below to make sure a new object is always returned.
    if (what == "moment") {
      // if (moment.isMoment(dt)) { return dt; }
      return moment(dt);
    } else {
      // if (dt instanceof Date) { return dt; }
      if (moment.isMoment(dt)) { return dt.toDate(); }
      return moment(dt).toDate();
    }
  }

  // obj must be either a Date object, or a moment object.
  // fmt must be one of: ["date", "moment", "iso"] or a valid moment format string. Default "date"
  function formatDate(obj, fmt) {
    if (fmt && fmt != "date") {
      var mom = null;
      var converted = false;
      if (moment.isMoment(obj)) {
        mom = obj;
      } else {
        mom = moment(obj);
        converted = true;
      }
      switch (fmt) {
        case "moment":
          return converted ? mom : moment(mom);
        case "iso":
          return mom.format("YYYY-MM-DD");
        case "quarter-array":
          return [mom.year(), getQuarter(mom)];
        case "quarter-pretty":
          return "Q" + getQuarter(mom) + " '" + mom.year().toString().substring(2);
        case "value":
          return mom.valueOf();
        default:
          return mom.format(fmt);
      }
    } else {
      if (moment.isMoment(obj)) { return obj.toDate(); }
      return obj;
    }
  }

  // Returns 1-based quarter of moment object:
  // [jan, feb, mar] -> 1
  // [apr, may, jun] -> 2
  // [jul, aug, sep] -> 3
  // [oct, nov, dec] -> 4
  function getQuarter(mom) { return Math.ceil((mom.month() + 1) / 3); }

  // Finds dates and generates date ranges relative to a given week start day.
  function DateFinder(weekStartsOnDay, weekBasedBounds) {
    if (weekStartsOnDay < 0 || weekStartsOnDay > 6) {
      throw new Error("weekStartsOnDay must be in [0, 6]");
    }
    this.weekStartsOnDay = weekStartsOnDay;
    this.weekBasedBounds = weekBasedBounds !== undefined ? weekBasedBounds : true;
  }

  var cls = DateFinder;

  // arguments:
  // - dt - A value that will return a valid value from a call of moment(dt)
  // - unit - What to get the start of: ["week", "month", "quarter", "year"]
  // - fmt - The format for output, passed directly to formatDate
  // - delta - hash of units and values to shift date by.
  cls.prototype.getStartOf = function(dt, unit, fmt, opts) {
    if (!dt) { throw new Error("Invalid date"); }
    dt = getDate(dt, "moment");
    unit = unit || "week";
    fmt = fmt || "date";

    if (opts && opts.delta) { shiftDate(dt, opts.delta); }

    var sou = null;
    var fd = null;

    if (this.weekBasedBounds) {
      fd = moment(this._getStartOfWeek(dt));
    } else {
      fd = dt;
    }

    switch(unit) {
      case "week":
        sou = this._getStartOfWeek(fd);
        break;
      case "month":
        sou = fd.startOf("month");
        break;
      case "quarter":
        sou = moment([fd.year(), (getQuarter(fd) - 1) * 3]);
        break;
      case "year":
        sou = fd.startOf("year");
        break;
      default:
        throw new Error("Unknown unit: " + unit);
    }

    if (this.weekBasedBounds && !this._isStartOfWeek(sou)) {
      sou = moment(this._getStartOfWeek(sou)).add(7, "days");
    }

    return formatDate(sou, fmt);
  }

  cls.prototype.getEndOf = function(dt, unit, fmt, opts) {
    if (!dt) { throw new Error("Invalid date"); }
    dt = getDate(dt, "moment");
    unit = unit || "week";
    fmt = fmt || "date";

    if (opts && opts.delta) { shiftDate(dt, opts.delta); }

    var eou = null;

    switch(unit) {
      case "week":
        eou = this._getEndOfWeek(dt);
        break;
      case "month":
        eou = moment(dt).add(1, "months").startOf("month").subtract(1, "days");
        break;
      case "quarter":
        eou = moment([dt.year(), (getQuarter(dt) -1) * 3]).add(3, "months").startOf("month").subtract(1, "days");
        break;
      case "year":
        eou = moment([dt.year(), 11, 31]);
        break;
      default:
        throw new Error("Unknown unit: " + unit);
    }

    if (this.weekBasedBounds && unit != "week") {
      eou = this._getEndOfWeek(eou);
    }

    return formatDate(eou, fmt);
  }

  cls.prototype.getDateRanges = function(args) {
    args = _.extend({}, getDateRangesDefaultArgs, args);
    if (!args.fromDate) { throw new Error("Invalid from date"); }
    if (!args.toDate) { throw new Error("Invalid to date"); }

    var fd = this.weekBasedBounds ? this.getStartOf(args.fromDate, args.unit, "moment") : getDate(args.fromDate, "moment");
    var td = this.weekBasedBounds ? this.getEndOf(args.toDate, args.unit, "moment") : getDate(args.toDate, "moment");

    // var fdDebug = "From Date: " + fd.toString() + " / " + fd.valueOf();
    // var tdDebug = "  To Date: " + td.toString() + " / " + td.valueOf();

    // if (!td.isValid() || !fd.isValid() || td.valueOf() <= fd.valueOf()) {
    //   var txt = "There was an error with the date range provided.\n  " + fdDebug + "\n  " + tdDebug;
    //   throw txt;
    //   // return [];
    // }

    var currentRange = [moment(fd), this.getEndOf(fd, args.unit, "moment")];
    var ranges = [currentRange];

    // while (!(td.valueOf() < currentRange[0].valueOf() || rangeIncludes(currentRange, td))) {
    while (td.valueOf() > currentRange[0].valueOf() && !rangeIncludes(currentRange, td)) {
      if (td.valueOf() < currentRange[0].valueOf()) {
        break;
      }
      if (rangeIncludes(currentRange, td)) {
        break;
      }

      var nlb = moment(currentRange[1]).add(1, "days");
      var nub = this.getEndOf(nlb, args.unit, "moment");
      currentRange = [nlb, nub];
      ranges.push(currentRange);
    }

    if (args.format != "moment") {
      for (var i = 0; i < ranges.length; ++i) {
        var range = ranges[i];
        range[0] = formatDate(range[0], args.format);
        range[1] = formatDate(range[1], args.format);
      }
    }

    if (args.onlyRangeStart) {
      return _.map(ranges, function(r) { return r[0]; });
    } else {
      return ranges;
    }
  }

  var getDateRangesDefaultArgs = {
    fromDate: null,
    toDate: null,
    unit: "week",
    format: "date",
    onlyRangeStart: false
  };

  cls.prototype._getStartOfWeek = function(dt) {
    dt = getDate(dt, "date");
    var day = dt.getDay();
    var date = dt.getDate();
    var newDt = new Date(dt.valueOf());
    var diff = (day - this.weekStartsOnDay) % 7;
    diff = diff < 0 ? 7 + diff : diff;
    newDt.setDate(date - diff);
    return newDt;
  }

  cls.prototype._getEndOfWeek = function(dt) {
    var s = this._getStartOfWeek(dt);
    s.setDate(s.getDate() + 6);
    return s;
  }

  cls.prototype._isStartOfWeek = function(dt) {
    var m = null;
    if (dt instanceof Date) {
      m = dt.getDay();
    } else {
      if (moment.isMoment(dt)) {
        m = dt.day();
      } else {
        m = moment(dt).day();
      }
    }
    return m == this.weekStartsOnDay;
  }

  cls.prototype.formatDate = formatDate;

  // Export
  ns.DateFinder = DateFinder;
})(jsu);
