(function(ns) {
  "use strict";

  function ViewModelBase(opts) {
    if (this === window || this === undefined) {
      throw new Error("Error creating View Model");
    }

    opts = _.extend({ }, defaultOpts, opts)

    this.private = {
      root: opts.root,
      parent: opts.parent,
      eaSubscriptions: [],
      koSubscriptions: [],
      eventHandlers: [],
      viewModelPropertyNames: []
    };

    this.isLoading = ko.observable("all");

    if (opts.root) {
      // If this is not the root, don't do anything.
    } else {
      var self = this;

      self.private.root = self;
      self._eventAggregator = new jsu.EventAggregator();
      self._formatter = new jsu.Formatter();
      self._dataAccess = opts.dataAccess;
      jsu.ko.StatusMessage(self, opts.statusMessageTimeouts);
      self.viewportWidth = ko.observable(
        jsu.util.notifyOnWindowSizeChanged(
          function(nv, ov, sz) {
            self.private.root.viewportWidth(nv);
            if (opts.onWindowSizeChanged) {
              opts.onWindowSizeChanged();
            }
          }));
      self.windowOrientation = ko.observable(
        jsu.util.notifyOnWindowOrientationChanged(
          function(nv) {
            self.private.root.windowOrientation(nv);
            if (opts.onWindowOrientationChanged) {
              opts.onWindowOrientationChanged();
            }
          },
          opts.unknownOrientation));
    }
  }

  var defaultOpts = { };

  var rootDefaultOpts = {
    statusMessageTimeouts: { "success": 5000, "info": 5000 }
  };

  ViewModelBase.prototype.open = function(callback) {
    var self = this;
    this.isLoading("all");
    this.load(function(arg) {
      if (self.isLoading() === "all") {
        self.isLoading(null);
      }
      if (callback) {
        callback(arg);
      }
    });
  }

  ViewModelBase.prototype.close = function(callback, skipSave) {
    var self = this;

    function _callback(arg) {
      if (self.private.eaSubscriptions) {
        _.forEach(self.private.eaSubscriptions, function(k) { self.private.root._eventAggregator.unsubscribe(k); });
      }

      if (self.private.koSubscriptions) {
        _.forEach(self.private.koSubscriptions, function(s) { s.dispose(); });
      }

      if (self.private.eventHandlers) {
        _.forEach(self.private.eventHandlers, function(s) { $(s[0]).off(s[1], s[2]); });
      }

      if (self.private.viewModelPropertyNames) {
        _.forEach(self.private.viewModelPropertyNames, function(pn) { self.closeViewModel(pn, null, true); });
      }

      self.clearMessage();
      self.finalize();

      // self.private.root = null;
      // self.private.parent = null;

      if (callback) {
        callback(arg);
      }
    };

    if (this.hasChanges() && !skipSave) {
      this.save(_callback);
    } else {
      _callback(this);
    }
  }

  ViewModelBase.prototype.getViewModelOpts = function() {
    var opts = {
      root: this.private.root,
      parent: this
    };
    return $.extend(opts, this.getCustomViewModelOpts());
  }

  ViewModelBase.prototype.openViewModel = function(propertyName, viewModelConstructor, callback, opts, create) {
    if (!propertyName) {
      var vm = new viewModelConstructor($.extend(this.getViewModelOpts(), opts));
      vm.open(callback);
      return vm;
    } else {
      if (!this[propertyName] && create) {
        this[propertyName] = ko.observable(null);
      }
      if (!this[propertyName]()) {
        this[propertyName](new viewModelConstructor($.extend(this.getViewModelOpts(), opts)));
        this.private.viewModelPropertyNames.push(propertyName);
      }
      this[propertyName]().open(callback);
      return this[propertyName]();
    }
  }

  ViewModelBase.prototype.closeViewModel = function(propertyName, callback, skipSave) {
    if (this[propertyName]()) {
      this[propertyName]().close(callback, skipSave);
      this[propertyName](null);
    }
  }

  ViewModelBase.prototype.koSubscribe = function(kob, fx) {
    this.private.koSubscriptions.push(kob.subscribe(fx));
  }

  ViewModelBase.prototype.eaSubscribe = function() {
    this.private.eaSubscriptions.push(this.private.root._eventAggregator.subscribe.apply(this.private.root._eventAggregator, arguments));
  }

  ViewModelBase.prototype.registerEventHandler = function(element, eventName, handler) {
    $(element).on(eventName, handler);
    this.private.eventHandlers.push([element, eventName, handler]);
  }

  ViewModelBase.prototype.broadcast = function(message, data) {
    this.private.root._eventAggregator.broadcast(this, message, data);
  }

  ViewModelBase.prototype.api = function(method, args) {
    var da = this.private.root._dataAccess;
    if (!(method in da)) {
      throw new Error(jsu.Formatter.string("API method {0} not found", method));
    }
    return da[method](args);
  }

  ViewModelBase.prototype.eventAggregator = function() { return this.private.root._eventAggregator; }

  ViewModelBase.prototype.formatter = function() { return this.private.root._formatter; }

  ViewModelBase.prototype.setMessage = function(type, message, timeout) { return this.private.root.setMessage(type, message, timeout); }

  ViewModelBase.prototype.clearMessage = function() { return this.private.root.clearMessage(); }

  // NOTE: These methods are expected to be overridden in most cases.
  ViewModelBase.prototype.hasChanges = function() { return false; }
  ViewModelBase.prototype.isActive = function() { return true; }
  ViewModelBase.prototype.save = function(callback) {
    if (callback) {
      callback(this);
    }
  }
  ViewModelBase.prototype.load = function(callback) {
    if (callback) {
      callback(this);
    }
  }
  ViewModelBase.prototype.finalize = function() { }
  ViewModelBase.prototype.getCustomViewModelOpts = function() { return { }; }

  // Export
  // ns.generateViewModelBase = generateViewModelBase;
  ns.ViewModelBase = ViewModelBase;
})(jsu);
