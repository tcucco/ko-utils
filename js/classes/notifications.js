// DEPENDENCY: underscore
// DEPENDENCY: jQuery

(function(ns) {
  var webkitPermissions = ["granted", "default", "denied", "unavailable"];

  // This class provides a common interface to webkit notifications and
  // standardized notifications. It also fails silently in browsers without
  // notifications
  function Notification(title, opts) {
    if (Notification.permission() != "granted") { return; }
    opts = _.extend({}, { icon: "", body: "", tag: "" }, opts);

    if (window.webkitNotifications) {
      var notification = window.webkitNotifications.createNotification(opts.icon, title, opts.body);
      notification.show();
    } else if (window.Notification) {
      new window.Notification(title, opts);
    }
  }

  Notification.permission = function() {
    if (window.webkitNotifications) {
      return webkitPermissions[window.webkitNotifications.checkPermission()];
    } else if (window.Notification) {
      return window.Notification.permission;
    } else {
      return "unavailable";
    }
  }

  Notification.requestPermission = function(callback) {
    if (Notification.permission() == "default") {
      if (window.webkitNotifications) {
        window.webkitNotifications.requestPermission(callback);
      } else if (window.Notification) {
        window.Notification.requestPermission(callback);
      } else {
        callback();
      }
    }
  }

  ns.Notification = Notification;


  // This class regularly checks for notifications at a given API endpoint, and
  // shows any that are found. This class will do nothing if notifications
  // aren't available, or permission is not granted.
  function NotificationChecker(args) {
    var self = this;

    self.getUri = args.getUri;
    self.delay = args.delay || 60 * 1000;
    self.immediate = args.immediate === false ? false : true;
    self.successCallback = args.successCallback;
    self.icon = args.icon || undefined;

    if (Notification.permission() != "granted") {
      return;
    }

    if (self.immediate) {
      self.checkForNotifications();
    }

    self.intervalHandle = setInterval(function() { self.checkForNotifications() }, self.delay);
  }

  NotificationChecker.prototype.checkForNotifications = function() {
    var self = this;

    $.ajax(self.getUri(), {
      success: function(data) {
        self.showNotification(_.map(data.notifications, _.identity));
        if (self.successCallback) { self.successCallback(data); }
      },
      error: function(xhr, status, error) {
        clearInterval(self.intervalHandle);
      }
    });
  };

  NotificationChecker.prototype.showNotification = function(nlist) {
    if (!(nlist && nlist.length)) { return; }

    var self = this;
    var n = nlist.shift();

    NotificationChecker.showNotification(n.subject, { body: n.body, icon: self.icon }, function() { self.showNotification(nlist); });
  };

  NotificationChecker.showNotification = function(subject, opts, callback) {
    new Notification(subject, opts);
    if (callback) { callback(); }
  };

  // Export
  ns.Notification = Notification;
  ns.NotificationChecker = NotificationChecker;
})(jsu);
