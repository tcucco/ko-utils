// DEPENDENCY: jQuery
// DEPENDENCY: utils/event-registration

(function(ns) {
  "use strict";

  function DragDropManager(args) {
    var self = this;

    self.movementThreshold = args.movementThreshold;
    self.flyBack = args.flyBack;

    self.isMouseDown = false;
    self.isDragging = false;
    self.dragOrigin = null;
    self.getDragData = null;
    self.getDragVisual = null;
    self.onDragCancelled = null;
    self.dragData = null;
    self.dragVisual = null;

    jsu.util.registerEvent(document, "mouseup", function(e) { self.globalMouseUp(e); });
    jsu.util.registerEvent(document, "mousemove", function(e) { self.globalMouseMove(e); });
  }

  DragDropManager.prototype.dropCleanup = function() {
    var self = this;
    self.isMouseDown = false;
    self.isDragging = false;
    self.dragOrigin = null;
    self.getDragData = null;
    self.getDragVisual = null;
    self.onDragCancelled = null;
    self.dragData = null;
    if (self.dragVisual) {
      self.dragVisual.remove();
    }
    self.dragVisual = null;
  };

  DragDropManager.prototype.dragCancelled = function() {
    var self = this;
    if (self.flyBack && self.dragVisual) {
      self.dragVisual.animate({ left: self.dragOrigin[0], top: self.dragOrigin[1] }, 400, "swing", function() {
        if (self.onDragCancelled) { self.onDragCancelled(self.dragData); }
        self.dropCleanup();
      });
    } else {
      if (self.onDragCancelled) { self.onDragCancelled(self.dragData); }
      self.dropCleanup();
    }
  }

  DragDropManager.prototype.globalMouseUp = function(e) {
    var self = this;
    if (self.isDragging) {
      self.dragCancelled();
    } else {
      self.dropCleanup();
    }
  };

  DragDropManager.prototype.globalMouseMove = function(e) {
    var self = this;
    if (self.isMouseDown && !self.isDragging && distance(self.dragOrigin, [e.pageX, e.pageY]) >= self.movementThreshold) {
      self.isDragging = true;
      self.dragData = self.getDragData();
      if (self.getDragVisual) {
        self.dragVisual = $(self.getDragVisual(self.dragData, e));
        self.dragVisual.appendTo(document.body);
        self.dragVisual.css({ top: e.pageY + 5, left: e.pageX + 5 });
      }
      self.getDragData = null;
      self.getDragVisual = null;
    } else if (self.dragVisual) {
      self.dragVisual.css({ top: e.pageY + 5, left: e.pageX + 5 });
    }
  };

  DragDropManager.prototype.registerDropElement = function(element, scope, processDrop, acceptsDrop) {
    var self = this;
    var handler = function(e) {
      if (self.isDragging) {
        e.preventDefault();
        var successful = false;

        try {
          if (!acceptsDrop || acceptsDrop(scope, self.dragData)) {
            processDrop(scope, self.dragData);
            successful = true;
            e.stopPropagation();
          } else if (acceptsDrop && !acceptsDrop(scope, self.dragData)) {
            self.dragCancelled();
          }
        } finally {
          if (!successful) {
            // if (self.onDragCancelled) { self.onDragCancelled(); }
            self.dragCancelled();
            e.stopPropagation();
          } else {
            self.dropCleanup();
          }
        }
      }
    };
    jsu.util.registerEvent(element, "mouseup", handler);
  };

  DragDropManager.prototype.registerDragElement = function(element, getDragData, onDragCancelled, getDragVisual) {
    var self = this;
    var handler = function(e) {
      if (!DragDropManager.elementCanDrag(e.target.nodeName)) { return; }
      e.preventDefault();
      e.stopPropagation();
      self.isMouseDown = true;
      self.getDragData = getDragData;
      self.getDragVisual = getDragVisual;
      self.onDragCancelled = onDragCancelled;
      self.dragOrigin = [e.pageX, e.pageY];
    };
    jsu.util.registerEvent(element, "mousedown", handler);
  };

  DragDropManager.elementCanDrag = function(nodeName) {
    switch(nodeName) {
      case "INPUT":
      case "SELECT":
      case "TEXTAREA":
      case "BUTTON":
      case "OPTGROUP":
      case "OPTION":
      case "DATALIST":
      case "KEYGEN":
      case "OUTPUT":
        return false;
      default:
        return true;
    }
  }

  function distance(p0, p1) {
    return Math.sqrt(Math.pow(p0[0] - p1[0], 2) + Math.pow(p0[1] - p1[1], 2));
  }

  // Export
  ns.DragDropManager = DragDropManager;
})(jsu);
