(function(ns) {
  "use strict";

  function DelayedActor(action, interval) {
    this.action = action;
    this.interval = interval;
    this.needsAction = false;
    this.timeout = null;
  }

  DelayedActor.prototype.act = function() {
    clearTimeout(this.timeout);
    var da = this;
    this.timeout = setTimeout(function() { da.actNow(); }, this.interval);
    this.needsAction = true;
  }

  DelayedActor.prototype.actNow = function(args, force) {
    clearTimeout(this.timeout);
    if (force || this.needsAction) {
      this.action(args);
      this.needsAction = false;
    }
  }

  DelayedActor.prototype.cancel = function() {
    clearTimeout(this.timeout);
  }

  ns.DelayedActor = DelayedActor;
})(jsu);
