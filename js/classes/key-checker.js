(function(ns) {
  "use strict";

  // nodeType is one of:
  // "i": only matches when event.target is an input type
  // "o": only matches when event.target is an output type (any non-input type)
  // "a": (default) doesn't check nodeType
  function KeyChecker(keyDef, nodeType) {
    this.keyDef = keyDef;
    this.parsedKeyDef = KeyChecker.parseKeyDefinition(keyDef);
    this.nodeType = nodeType || "a";
  }

  KeyChecker.prototype.matches = function(e) {
    return nodeTypeMatches(this.nodeType, e.target.nodeName, e.target) &&
      KeyChecker.matches(this.parsedKeyDef, e);
  }

  KeyChecker.parseKeyDefinition = function(keyDef) {
    var groups = keyDef.split("+");
    for (var i = 0; i < groups.length; ++i) {
      groups[i] = groups[i].split("|");
    }
    return groups;
  }

  KeyChecker.matchesKeyDefinition = function(kdef, e) {
    return KeyChecker.matches(KeyChecker.parseKeyDefinition(kdef), e);
  }

  KeyChecker.matches = function(pkdef, e) {
    var allMatch = false;

    for (var gi = 0; gi < pkdef.length; ++gi) {
      var group = pkdef[gi];
      var groupMatches = false;
      for (var ci = 0; ci < group.length; ++ci) {
        if (groupMatches) { break; }
        switch (group[ci]) {
          case "shift":
            groupMatches = e.shiftKey;
            break;
          case "control":
            groupMatches = e.ctrlKey;
            break;
          case "meta":
            groupMatches = e.metaKey;
            break;
          case "alt":
            groupMatches = e.altKey;
            break;
          default:
            groupMatches = (e.keyCode === keyMap[group[ci]]);
            break;
        }
      }
      if (!groupMatches) {
        return false;
      }
    }

    return true;
  }

  function nodeTypeMatches(typeCode, nodeName, node) {
    if (typeCode == "a") {
      return true;
    } else if (typeCode == "i") {
      return isInput(nodeName, node);
    } else {
      return !isInput(nodeName, node);
    }
  }

  function isInput(nodeName, node) {
    switch(nodeName) {
      case "INPUT":
      case "SELECT":
      case "TEXTAREA":
      case "BUTTON":
      case "OPTGROUP":
      case "OPTION":
      // case "DATALIST":
      // case "KEYGEN":
      // case "OUTPUT":
        return true;
      default:
        return node.contentEditable === "true";
    }
  }

  var keyMap = {
    "backspace": 8,
    "tab": 9,
    "enter": 13,
    "shift": 16,
    "control": 17,
    "alt": 18,
    "capslock-on": 20,
    "escape": 27,
    "space": 32,
    "left": 37,
    "up": 38,
    "right": 39,
    "down": 40,
    "0": 48,
    "1": 49,
    "2": 50,
    "3": 51,
    "4": 52,
    "5": 53,
    "6": 54,
    "7": 55,
    "8": 56,
    "9": 57,
    "a": 65,
    "b": 66,
    "c": 67,
    "d": 68,
    "e": 69,
    "f": 70,
    "g": 71,
    "h": 72,
    "i": 73,
    "j": 74,
    "k": 75,
    "l": 76,
    "m": 77,
    "n": 78,
    "o": 79,
    "p": 80,
    "q": 81,
    "r": 82,
    "s": 83,
    "t": 84,
    "u": 85,
    "v": 86,
    "w": 87,
    "x": 88,
    "y": 89,
    "z": 90,
    "meta-left": 91,
    "meta-right": 93,
    "f1": 112,
    "f2": 113,
    "f3": 114,
    "f4": 115,
    "f5": 116,
    "f6": 117,
    "f7": 118,
    "f8": 119,
    "f9": 120,
    "f10": 121,
    "f11": 122,
    "f12": 123,
    ";": 186,
    "+": 187,
    ",": 188,
    "-": 189,
    ".": 190,
    "/": 191,
    "`": 192,
    "[": 219,
    "\\": 220,
    "]": 221,
    "'": 222
  };

  ns.KeyChecker = KeyChecker;
})(jsu);
