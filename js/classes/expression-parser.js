jsu.expression = jsu.expression || {};

(function(ns) {
  "use strict";

  var Associativity = {
    left: 1,
    right: 2
  };

  function setParseId(cons, value) {
    cons.prototype.parseId = cons.parseId = value;
  }

  function OperatorToken(symbol, precedence, associativity, cardinality, execute, isShortCircuit) {
    this.symbol = symbol;
    this.precedence = precedence;
    this.associativity = associativity;
    this.cardinality = cardinality;
    this.execute = execute;
    this.isShortCircuit = isShortCircuit;
  }

  function applyOr(evaluator, lht, rht) {
    var lh = evaluator.getValue(lht);

    if (lh) { return true; }

    var rh = evaluator.getValue(rht);

    return rh;
  }

  function applyAnd(evaluator, lht, rht) {
    var lh = evaluator.getValue(lht);

    if (!lh) { return false; }

    var rh = evaluator.getValue(rht);

    return rh;
  }

  function saveDiv(n, d, fallback) {
    if (n === null || n === undefined || d === null || d === undefined || d === 0) {
      return fallback;
    } else {
      return n / d;
    }
  }

  setParseId(OperatorToken, "operator");

  OperatorToken.prototype.toString = function() { return this.symbol; }

  OperatorToken.or                 = new OperatorToken("|",  1, Associativity.left, 2, applyOr, true);

  OperatorToken.and                = new OperatorToken("&",  2, Associativity.left, 2, applyAnd, true);

  OperatorToken.add                = new OperatorToken("+",  3, Associativity.left, 2, function(a, b) { return a + b; });
  OperatorToken.subtract           = new OperatorToken("-",  3, Associativity.left, 2, function(a, b) { return a - b; });

  OperatorToken.equal              = new OperatorToken("=",  3, Associativity.left, 2, function(a, b) { return a == b; });
  OperatorToken.greaterThan        = new OperatorToken(">",  3, Associativity.left, 2, function(a, b) { return a > b; });
  OperatorToken.greaterThanOrEqual = new OperatorToken(">=", 3, Associativity.left, 2, function(a, b) { return a >= b; })
  OperatorToken.lessThan           = new OperatorToken("<",  3, Associativity.left, 2, function(a, b) { return a < b; });;
  OperatorToken.lessThanOrEqual    = new OperatorToken("<=", 3, Associativity.left, 2, function(a, b) { return a <= b; });
  OperatorToken.notEqual           = new OperatorToken("!=", 3, Associativity.left, 2, function(a, b) { return a != b; });

  OperatorToken.multiply           = new OperatorToken("*",  4, Associativity.left, 2, function(a, b) { return a * b; });
  OperatorToken.divide             = new OperatorToken("/",  4, Associativity.left, 2, function(a, b) { return a / b; });
  OperatorToken.safeDivide         = new OperatorToken("//", 4, Associativity.left, 2, function(a, b) { return saveDiv(a, b, 0); });

  OperatorToken.not                = new OperatorToken("!",  5, Associativity.right, 1, function(a) { return !a; });

  OperatorToken.power              = new OperatorToken("^",  6, Associativity.right, 2, function(a, b) { return Math.pow(a, b); });
  // Specially handled operators
  OperatorToken.negate             = new OperatorToken("-",  4, Associativity.right, 1, function(a) { return -a; });
  OperatorToken.identity           = new OperatorToken("+",  4, Associativity.right, 1, function(a) { return a; });

  OperatorToken.operators = [
    OperatorToken.add,
    OperatorToken.and,
    OperatorToken.divide,
    OperatorToken.safeDivide,
    OperatorToken.equal,
    OperatorToken.greaterThan,
    OperatorToken.greaterThanOrEqual,
    OperatorToken.lessThan,
    OperatorToken.lessThanOrEqual,
    OperatorToken.multiply,
    OperatorToken.not,
    OperatorToken.notEqual,
    OperatorToken.or,
    OperatorToken.power,
    OperatorToken.subtract
  ].sort(function(a, b) { return b.symbol.length - a.symbol.length });


  function VariableToken(name) {
    this.name = name;
  }

  setParseId(VariableToken, "variable");

  VariableToken.prototype.toString = function() { return this.name; }


  function SubexpressionToken(input) {
    this.input = input;
  }

  setParseId(SubexpressionToken, "subexpression");

  SubexpressionToken.prototype.expression = function() {
    if (!this._expression) {
      this._expression = this.input.slice(1, -1);
    }
    return this._expression;
  }

  SubexpressionToken.prototype.toString = function() { return this.expression(); }


  // For wrapping native values: numbers, strings, booleans
  function WrapperToken(value) {
    this.value = value;
  }

  setParseId(WrapperToken, "wrapper");


  function FunctionToken(input, evaluateArgs) {
    this.input = input;
    if (evaluateArgs) {
      this.argumentTokens();
    }
  }

  setParseId(FunctionToken, "function");

  FunctionToken.prototype.name = function() {
    if (!this._name) {
      var op = this.input.indexOf("(");
      this._name = this.input.slice(0, op).replace(/\s+$/, "");
    }
    return this._name;
  }

  FunctionToken.prototype.argumentTokens = function() {
    if (!this._argumentTokens) {
      var op = this.input.indexOf("(");
      var argsInput = this.input.slice(op + 1, -1);
      var startPosition = 0;
      var args = [];

      while (startPosition < argsInput.length) {
        var tokenizer = new Tokenizer(argsInput, { haltOn: ',', startPosition: startPosition });
        var tokens = [];

        while (!tokenizer.done) {
          var t = tokenizer.getNextToken();

          if (t !== undefined) {
            tokens.push(t);
          }
        }

        args.push(tokens);
        startPosition = tokenizer.currentPosition + 1;
      }

      this._argumentTokens = args;
    }
    return this._argumentTokens;
  }

  FunctionToken.prototype.toString = function() { return this.input; }


  function Tokenizer(input, opts) {
    opts = opts || {};
    this.input = input;
    this.haltOn = opts.haltOn;
    this.currentPosition = opts.startPosition || 0;
    this.done = !(input && input.length);
  }

  Tokenizer.prototype.advance = function(n) {
    this.currentPosition += n || 1;
    if (this.currentPosition >= this.input.length) {
      this.done = true;
    }
  }

  Tokenizer.prototype.currentCharacter = function() { return this.input.charAt(this.currentPosition); }

  Tokenizer.prototype.relativeCharacter = function(n) {
    if (n === undefined) {
      n = 0;
    }
    return this.input.charAt(this.currentPosition + n);
  }

  Tokenizer.prototype.getNextToken = function() {
    var cc = this.currentCharacter();

    while (Tokenizer.isWhiteSpace(cc) && !this.done) {
      this.advance();
      cc = this.currentCharacter();
    }

    if (cc == this.haltOn) {
      this.done = true;
    }

    var token = undefined;

    // NOTE: Order here is important.
    // First check functions, then operators.
    if (this.done) { /* pass */ }
    else if (this.operatorIsNext()) { token = this.getOperatorToken(); }
    else if (this.subexpressionIsNext()) { token = this.getSubexpressionToken(); }
    else if (this.stringIsNext()) { token = this.getStringToken(); }
    else if (this.regExpIsNext()) { token = this.getRegExpToken(); }
    else if (this.numberIsNext()) { token = this.getNumberToken(); }
    else if (this.booleanIsNext()) { token = this.getBooleanToken(); }
    else if (this.functionIsNext()) { token = this.getFunctionToken(); }
    else if (this.variableIsNext()) { token = this.getVariableToken(); }
    else if (this.whitespaceVariableIsNext()) { token = this.getWhitespaceVariable(); }
    else { throw new Error("Unable to parse character '" + cc + "' at position " + this.currentPosition); }

    return token;
  }

  Tokenizer.prototype.operatorIsNext = function() {
    // NOTE: It's important that the operators are sorted by symbol length
    //       descending.
    for (var i = 0; i < OperatorToken.operators.length; ++i) {
      var symbol = OperatorToken.operators[i].symbol;

      if (this.input.indexOf(symbol, this.currentPosition) == this.currentPosition) {
        return true;
      }
    }

    return false;
  }

  Tokenizer.prototype.getOperatorToken = function() {
    var op = null;

    // NOTE: It's important that the operators are sorted by symbol length
    //       descending.
    for (var i = 0; i < OperatorToken.operators.length; ++i) {
      var symbol = OperatorToken.operators[i].symbol;

      if (this.input.indexOf(symbol, this.currentPosition) == this.currentPosition) {
        op = OperatorToken.operators[i];
        break;
      }
    }

    if (op == null) {
      throw new Error("Unrecognized operator '" + c + "' at position " + this.currentPosition);
    }

    this.advance(op.symbol.length);

    return op;
  }

  Tokenizer.prototype.subexpressionIsNext = function() { return this.currentCharacter() == "("; }

  Tokenizer.prototype.getSubexpressionToken = function() {
    var fp = this.currentPosition;
    var depth = 1;
    this.advance();

    while(!this.done && depth > 0) {
      var cc = this.currentCharacter();

      if (cc == "(") {
        depth += 1;
      } else if (cc == ")") {
        depth -= 1;
      }

      this.advance();
    }

    if (this.input.charAt(this.currentPosition - 1) != ")") {
      throw "Unbalanced parentheses starting at position " + fp;
    }

    return new SubexpressionToken(this.input.slice(fp, this.currentPosition));
  }

  Tokenizer.prototype.stringIsNext = function() {
    var c = this.currentCharacter();
    return c == "'" || c == "\"";
  }

  Tokenizer.prototype.getStringToken = function() {
    var fp = this.currentPosition;
    var delimiter = this.currentCharacter();
    var escaped = false;
    var rv = "";

    this.advance();
    var cc = this.currentCharacter();

    while (!this.done && (cc != delimiter || escaped)) {
      if (escaped) {
        rv += cc;
        escaped = false;
      } else if (cc == "\\") {
        escaped = true;
      } else {
        rv += cc;
      }

      this.advance();
      cc = this.currentCharacter();
    }

    if (this.done) {
      throw new Error("Unterminated string starting at position " + fp);
    }

    this.advance();

    return new WrapperToken(rv);
  }

  Tokenizer.prototype.regExpIsNext = function() {
    return this.input.indexOf("re/", this.currentPosition) == this.currentPosition;
  }

  Tokenizer.prototype.getRegExpToken = function() {
    var fp = this.currentPosition;
    var delimiter = "/";
    var escaped = false;
    var rva = [];

    this.advance(3);
    var cc = this.currentCharacter();

    while(!this.done && (cc != delimiter || escaped)) {
      if (cc == "\\") {
        escaped = !escaped;
      } else if (cc == delimiter) {
        if (escaped) {
          rva.pop();
        }
      } else {
        escaped = false;
      }

      rva.push(cc);
      this.advance();
      cc = this.currentCharacter();
    }

    if (this.done) {
      throw new Error("Unterminated regex starting at position " + fp);
    }

    this.advance();

    return new WrapperToken(new RegExp(rva.join("")));
  }

  Tokenizer.prototype.numberIsNext = function() { return Tokenizer.numberStartRe.test(this.currentCharacter()); }

  Tokenizer.prototype.getNumberToken = function() {
    var decimalFound = false;
    var fp = this.currentPosition;
    var cc = this.currentCharacter();

    while(!this.done) {
      if (cc == ".") {
        if (decimalFound) {
          throw new Error("Malformed number starting at position " + fp);
        } else {
          decimalFound = true;
        }
      } else if (!Tokenizer.numberRe.test(cc)) {
        break;
      }

      this.advance();
      var cc = this.currentCharacter();
    }

    var s = this.input.slice(fp, this.currentPosition);

    return new WrapperToken(decimalFound ? parseFloat(s) : parseInt(s));
  }

  Tokenizer.prototype.booleanIsNext = function() {
    return /^(true|false)($|\W)/.test(this.input.slice(this.currentPosition));
  }

  Tokenizer.prototype.getBooleanToken = function() {
    var cc = this.currentCharacter();

    if (cc == "t") {
      this.advance(4);
      return new WrapperToken(true);
    } else {
      this.advance(5);
      return new WrapperToken(false);
    }
  }

  Tokenizer.prototype.functionIsNext = function() {
    return /^[a-z_]\w*\s*\(/i.test(this.input.slice(this.currentPosition));
  }

  Tokenizer.prototype.getFunctionToken = function() {
    var fp = this.currentPosition;
    var argumentsFound = false;
    var depth = 0;

    this.advance();

    while (!this.done && (!argumentsFound || depth != 0)) {
      var cc = this.currentCharacter();

      if (cc == "(") {
        argumentsFound = true;
        depth += 1;
      } else if (cc == ")") {
        if (argumentsFound) {
          depth -= 1;
        } else {
          throw new Error("Malformed function expression starting at position " + fp);
        }
      }

      this.advance();
    }

    if (this.input.charAt(this.currentPosition - 1) != ")") {
      throw new Error("Malformed function expression starting at position " + fp);
    }

    return new FunctionToken(this.input.slice(fp, this.currentPosition), true);
  }

  Tokenizer.prototype.variableIsNext = function() { return Tokenizer.variableStartRe.test(this.currentCharacter()); }

  Tokenizer.prototype.getVariableToken = function() {
    var fp = this.currentPosition;

    this.advance();

    var cc = this.currentCharacter();

    while (!this.done && Tokenizer.variableRe.test(cc)) {
      this.advance();
      cc = this.currentCharacter();
    }

    return new VariableToken(this.input.slice(fp, this.currentPosition));
  }

  Tokenizer.prototype.whitespaceVariableIsNext = function() { return this.currentCharacter() == "["; }

  Tokenizer.prototype.getWhitespaceVariable = function() {
    var fp = this.currentPosition;

    this.advance();

    while(!this.done && this.currentCharacter() != "]") { this.advance(); }

    if (this.done) {
      throw new Error("Unterminated whitespace variable starting at position " + fp);
    }

    this.advance();

    return new VariableToken(this.input.slice(fp + 1, this.currentPosition - 1));
  }

  Tokenizer.isWhiteSpace = function(c) { return Tokenizer.whiteSpaceRe.test(c); }

  Tokenizer.whiteSpaceRe = /\s/;
  Tokenizer.numberStartRe = /\d|\./;
  Tokenizer.numberRe = /\d/;
  Tokenizer.variableStartRe = /[a-z_\$]/i;
  Tokenizer.variableRe = /\w|\$/;


  function Parser() { }

  Parser.prototype.stringToQueue = function(input) {
    var tokenizer = new Tokenizer(input);
    var tokens = [];
    var lastToken = null;

    while (!tokenizer.done) {
      var token = tokenizer.getNextToken();

      // Properly handle signed variables, function calls and numbers. If the
      // last token is null, or was an operator, and the current token is + or
      // -, then we should push unary identity or negation into the queue.
      if (token.parseId == OperatorToken.parseId && (lastToken == null || lastToken.parseId == OperatorToken.parseId)) {
        if (token == OperatorToken.add) {
          token = OperatorToken.identity;
        } else if (token == OperatorToken.subtract) {
          token = OperatorToken.negate;
        }
      }

      lastToken = token;

      tokens.push(token);
    }

    return this.tokensToQueue(tokens);
  }

  Parser.prototype.tokensToQueue = function(tokens) {
    var operators = [];
    var output = [];
    var currentOperator = null;

    for (var i = 0; i < tokens.length; ++i) {
      var token = tokens[i];

      switch(token.parseId) {
        case SubexpressionToken.parseId:
          output.push.apply(output, this.stringToQueue(token.expression()));
          break;
        case WrapperToken.parseId:
        case VariableToken.parseId:
        case FunctionToken.parseId:
          output.push(token);
          break;
        case OperatorToken.parseId:
          if (currentOperator && (currentOperator.precedence > token.precedence || currentOperator.precedence == token.precedence && currentOperator.associativity == Associativity.left)) {
            output.push(operators.pop());
          }
          currentOperator = token;
          operators.push(token);
          break;
        default:
          throw new Error("Unrecognized token: " + token + " (" + token.parseId + ")");
      }
    }

    while(operators.length) {
      output.push(operators.pop());
    }

    return output;
  }


  function Evaluator(scope, debug) {
    this.scope = scope || new EvaluatorScope();
    this.debug = debug;
    this.values = [];
  }

  Evaluator.prototype.evaluateString = function(input) {
    var queue = new Parser().stringToQueue(input);
    return this.evaluateQueue(queue);
  }

  Evaluator.prototype.evaluateQueue = function(queue) {
    this.values = [];
    for (var i = 0; i < queue.length; ++i) {
      var token = queue[i];

      switch(token.parseId) {
        case WrapperToken.parseId:
        case VariableToken.parseId:
        case FunctionToken.parseId:
          this.values.push(token);
          break;
        case OperatorToken.parseId:
          this.applyOperator(token);
          break;
        default:
          throw new Error("Unrecognized token: " + token + " (" + token.parseId + ")");
      }
    }

    if (this.values.length != 1) {
      throw new Error("Invalid formula");
    }

    return this.getValue(this.values[0]);
  }

  Evaluator.prototype.applyOperator = function(operator) {
    if (this.values.length < operator.cardinality) {
      throw new Error("Operator '" + operator.symbol + "' expects " + operator.cardinality + " operands");
    }

    var rht = this.values.pop();
    var result = null;

    if (operator.cardinality == 1) {
      var rhs = this.getValue(rht);
      result = operator.execute(rhs);
      if (this.debug) { console.log(operator + " " + rhs + " = " + result); }
    } else {
      var result = null;

      if (operator.isShortCircuit) {
        var lht = this.values.pop();
        result = operator.execute(this, lht, rht);
      } else {
        var rhs = this.getValue(rht);
        var lhs = this.getValue(this.values.pop());
        result = operator.execute(lhs, rhs);
      }

      if (this.debug) { console.log(lhs + " " + operator + " " + rhs + " = " + result); }
    }

    this.values.push(new WrapperToken(result));
  }

  Evaluator.prototype.getValue = function(token) {
    switch(token.parseId) {
      case WrapperToken.parseId:
        return token.value;
      case VariableToken.parseId:
        return this.evaluateVariable(token);
      case FunctionToken.parseId:
        return this.evaluateFunction(token);
      default:
        "Unrecognized token: " + token + " (" + token.parseId + ")";
    }
  }

  Evaluator.prototype.evaluateVariable = function(variable) {
    var v = this.scope.getVariable(variable.name);

    if (v === undefined) { throw new Error("Undefined variable: " + variable.name); }

    return v;
  }

  Evaluator.prototype.evaluateFunction = function(functionExpression) {
    var fx = this.scope.getFunction(functionExpression.name());

    if (fx === undefined) { throw new Error("Undefined function: " + functionExpression.name()); }

    var argsTokens = functionExpression.argumentTokens();
    var args = [];

    if (Evaluator.hasSpecialProperty(fx, "scopeAware")) {
      args.push(this.scope);
    }

    if (Evaluator.hasSpecialProperty(fx, "unevaluatedArguments")) {
      for (var i = 0; i < argsTokens.length; ++i) {
        args.push(argsTokens[i]);
      }
    } else {
      for (var i = 0; i < argsTokens.length; ++i) {
        args.push(Evaluator.mapFunctionArg(this.scope, argsTokens[i]));
      }
    }

    return fx.apply(this, args);
  }

  Evaluator.hasSpecialProperty = function(fx, propertyName) {
    if (!fx.specialProperties) { return false; }
    if (propertyName in Evaluator.specialProperties) {
      return (fx.specialProperties & Evaluator.specialProperties[propertyName]) != 0
    } else {
      throw new Error("Unknown special property: " + propertyName);
    }
  }

  Evaluator.addSpecialProperties = function(fx) {
    var props = 0;
    for (var i = 1; i < arguments.length; ++i) {
      var n = arguments[i];
      if (n in Evaluator.specialProperties) {
        props = props | Evaluator.specialProperties[n];
      } else {
        throw new Error("Unknown special property: " + n);
      }
    }
    if (!fx.specialProperties) {
      fx.specialProperties = props;
    } else {
      fx.specialProperties = fx.specialProperties | props;
    }
    return fx;
  }

  Evaluator.mapFunctionArg = function(scope, tokens, mapToken) {
    var parser = new Parser();
    var editedTokens;

    if (mapToken) {
      editedTokens = [];
      for (var i = 0; i < tokens.length; ++i) {
        editedTokens.push(mapToken(tokens[i]));
      }
    } else {
      editedTokens = tokens;
    }

    var queue = parser.tokensToQueue(editedTokens);
    var evaluator = new Evaluator(scope);

    return evaluator.evaluateQueue(queue);
  }

  Evaluator.specialProperties = {
    none: 0x00,
    unevaluatedArguments: 0x01,
    scopeAware: 0x02,
    all: 0xff
  };


  function EvaluatorScope(global) {
    this.scope = [];

    if (global) { this.scope.push(global); }
  }

  EvaluatorScope.prototype.pushScope = function() {
    this.scope.push.apply(this.scope, arguments);
    return this;
  }

  EvaluatorScope.prototype.popScope = function() { return this.scope.pop(); }

  EvaluatorScope.prototype.each = function(fx) {
    for (var i = 0; i < this.scope.length; ++i) {
      fx(this.scope[i]);
    }
  }

  EvaluatorScope.prototype.getVariable = function(name) {
    for (var i = this.scope.length - 1; i >= 0; --i) {
      if (this.scope[i].getVariable) {
        var v = this.scope[i].getVariable(name);
        if (v !== undefined) {
          return v;
        }
      }
    }
    return undefined;
  }

  EvaluatorScope.prototype.getFunction = function(name) {
    for (var i = this.scope.length - 1; i >= 0; --i) {
      if (this.scope[i].getFunction) {
        var f = this.scope[i].getFunction(name);
        if (f !== undefined) {
          return f;
        }
      }
    }
    return undefined;
  }

  EvaluatorScope.mapToGetter = function(map, caseSensitive) {
    caseSensitive = caseSensitive === undefined ? true : caseSensitive;
    map = map || {};
    if (caseSensitive) {
      return function(name) { return map[name]; };
    } else {
      return function(name) { return map[name.toLowerCase()]; };
    }
  }


  function Dependency(args) {
    args = args || {};
    this.getName = args.getName;
    this.getFormula = args.getFormula;
    this.getTokenQueue = args.getTokenQueue;
    this.setTokenQueue = args.setTokenQueue;
  }

  // Get a dictionary whose keys are items that form circular references.
  Dependency.prototype.findCircularReferences = function(list) {
    var variables = this.prepareItems(list);
    var references = {};
    var circular = null;

    for (var k in variables) {
      references[k] = this.getVariables(variables[k]);
    }

    for (var k in variables) {
      if (this._hasCircularReferences(k, references)) {
        if (!circular) { circular = {}; }
        circular[k] = true;
      }
    }

    return circular;
  }

  // Return list in an order that ensures each items dependants are resolved before
  // it is calculated.
  Dependency.prototype.order = function(list) {
    var variables = this.prepareItems(list);
    var shallowReferences = {};
    var allReferences = {};

    for (var k in variables) {
      shallowReferences[k] = this.getVariables(variables[k]);
    }

    for (var k in variables) {
      allReferences[k] = this._getAllVariables(k, shallowReferences, true);
      if (k in allReferences[k]) {
        throw new Error("Circular reference found for " + k);
      }
    }

    var inserted = {};
    var insertedCount = 0;
    var ordered = [];
    var __looped = 0;

    while (insertedCount != list.length) {
      for (var i = 0; i < list.length; ++i) {
        var vname = this.getName(list[i]);
        var missingReferences = false;

        if (vname in inserted) { continue; }

        for (var rname in allReferences[vname]) {
          if (rname in variables && !(rname in inserted)) {
            missingReferences = true;
          }
        }

        if (!missingReferences) {
          inserted[vname] = true;
          insertedCount += 1;
          ordered.push(variables[vname]);
        }
      }

      if (++__looped > 1000) {
        throw new Error("Infinite loop?");
      }
    }

    return ordered;
  }

  // Get a map of all variables named in list that item depends on for resolution.
  Dependency.prototype.getAllVariables = function(list, item) {
    var variables = this.prepareItems(list);
    var shallowReferences = {};

    for (var k in variables) {
      shallowReferences[k] = this.getVariables(variables[k]);
    }

    return this._getAllVariables(this.getName(item), shallowReferences, true);
  }

  // Get the map of variables that are referenced directly by item.
  Dependency.prototype.getVariables = function(item) {
    var variables = {};
    var queue = this.getTokenQueue(item);

    for (var i = 0; i < queue.length; ++i) {
      this._getReferencedItems(queue[i], variables, VariableToken.parseId);
    }

    return variables;
  }

  Dependency.prototype.getFunctions = function(item) {
    this.prepareItems([item]);
    var fxs = {};
    var queue = this.getTokenQueue(item);

    for (var i = 0; i < queue.length; ++i) {
      this._getReferencedItems(queue[i], fxs, FunctionToken.parseId, "name()");
    }

    return fxs;
  }

  Dependency.prototype.prepareItems = function(list) {
    var vars = {};
    var parser = new Parser();

    for (var i = 0; i < list.length; ++i) {
      var item = list[i];
      vars[this.getName(item)] = item;
      if (!this.getTokenQueue(item)) {
        var queue = [];
        try {
          var queue = parser.stringToQueue(this.getFormula(item));
        } catch (ex) {
          queue = [];
        }
        this.setTokenQueue(item, queue);
      }
    }

    return vars;
  }

  Dependency.prototype._getReferencedItems = function(token, items, parseId, getName) {
    if (getName === undefined) {
      getName = "name";
    }
    if (token.parseId == parseId) {
      items[jsu.util.indexOrCall(token, getName)] = true;
    }
    if (token.parseId == FunctionToken.parseId) {
      var functionArgs = token.argumentTokens();
      for (var i = 0; i < functionArgs.length; ++i) {
        var argTokens = functionArgs[i];
        for (var j = 0; j < argTokens.length; ++j) {
          this._getReferencedItems(argTokens[j], items, parseId, getName);
        }
      }
    }
  }

  // Get the map of all variables referenced by name in allRefs
  Dependency.prototype._getAllVariables = function(name, allRefs, breakOnCircular) {
    var refs = {};
    var checked = {};
    var newRefFound = true;

    for (var k in allRefs[name]) {
      refs[k] = true;
    }

    // Prevent infinite loops
    checked[name] = true;

    while (newRefFound) {
      var newRefs = {};
      newRefFound = false;

      for (var ref in refs) {
        if (ref in checked) { continue; }

        if (ref in allRefs) {
          for (var newRef in allRefs[ref]) {
            if (!(newRef in refs)) {
              newRefs[newRef] = true;
              newRefFound = true;
            }
            if (newRef == name && breakOnCircular) {
              newRefFound = false;
              break;
            }
          }
        }
      }

      for (var newRef in newRefs) {
        refs[newRef] = true;
      }
    }

    return refs;
  }

  Dependency.prototype._hasCircularReferences = function(name, allRefs) {
    return (name in this._getAllVariables(name, allRefs, true));
  }


  // Export
  ns.OperatorToken = OperatorToken;
  ns.VariableToken = VariableToken;
  ns.SubexpressionToken = SubexpressionToken;
  ns.FunctionToken = FunctionToken;
  ns.Tokenizer = Tokenizer;
  ns.Parser = Parser;
  ns.Evaluator = Evaluator;
  ns.EvaluatorScope = EvaluatorScope;
  ns.Dependency = Dependency;
})(jsu.expression);
