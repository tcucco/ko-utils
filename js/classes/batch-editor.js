// DEPENDENCY: underscore
// DEPENDENCY: knockout

(function(ns) {
  function BatchEditor(args) {
    args = _.extend({}, batchEditorDefaultArgs, args);

    var self = this;
    var selectedItemsMap = {};
    var batchProperties = {};

    function updateInclusion(item, list, e) {
      if (!item) {
        clearItems();
      } else {
        if (e) {
          if (e.shiftKey) {
            if (self.selectedItem()) {
              var items = itemsInRange(list, self.selectedItem(), item);
              for (var i = 0; i < items.length; ++i) {
                addItem(items[i]);
              }
            } else {
              resetWith(item);
            }
          } else if (e.ctrlKey || e.metaKey) {
            if (isSelected(item)) {
              removeItem(item);
            } else {
              addItem(item);
            }
          } else {
            resetWith(item);
          }
        } else {
          resetWith(item);
        }
      }
      updatePublicMap();
    }

    function removeItem(item) {
      var i = self.selectedItems.remove(item);
      var id = args.getId(item);
      delete selectedItemsMap[id];
    }

    function resetWith(item) {
      clearItems();
      addItem(item);
    }

    function addItem(item) {
      if (!isSelected(item)) {
        selectedItemsMap[args.getId(item)] = true;
        self.selectedItems.unshift(item);
        updatePropertiesWithItem(item);
      }
    }

    function isSelected(item) {
      return args.getId(item) in selectedItemsMap;
    }

    function clearItems() {
      self.selectedItems([]);
      selectedItemsMap = {};
      resetItemProperties();
    }

    function resetItemProperties() {
      for (var i = 0; i < args.trackProperties.length; ++i) {
        var f = args.trackProperties[i];
        var v;

        if (f.type == "array") {
          v = [];
        } else {
          v = null;
        }

        if (!batchProperties[f.id]) {
          batchProperties[f.id] = new BatchEditProperty(v);
        } else {
          batchProperties[f.id].reset();
        }
      }

      for (var i = 0; i < self.selectedItems().length; ++i) {
        var item = self.selectedItems()[i];
        updatePropertiesWithItem(item);
      }
    }

    function updatePropertiesWithItem(item) {
      for (var i = 0; i < args.trackProperties.length; ++i) {
        var property = args.trackProperties[i];
        updatePropertyWithItem(property, item);
      }
    }

    function updatePropertyWithItem(property, item) {
      var reset = self.selectedItems().length == 1;

      if (property.type == "array") {
        var include = _.map(property.getValue(item), property.getId);
        if (reset) {
          batchProperties[property.id].value(include);
        } else {
          batchProperties[property.id].value(_.intersection(batchProperties[property.id].value(), include));
        }
      } else {
        var v = property.getValue(item);
        if (!batchProperties[property.id].value() == v) {
          batchProperties[property.id].reset(false);
        }
      }
    }

    function updatePublicMap() {
      self.selectedItemsMap(selectedItemsMap);
    }

    self.selectedItems = ko.observableArray([]);
    self.selectedItem = ko.computed(function() {
      switch (self.selectedItems().lengt) {
        case 0:
          return null;
        case 1:
          return self.selectedItems()[0];
        default:
          return args.singleSelectionOnMultiple ? self.selectedItems()[0] : null;
      }
    });
    self.hasSelection = ko.computed(function() { return self.selectedItems().length; });
    self.isSingle = ko.computed(function() { return self.selectedItems().length == 1; });
    self.selectedItemsMap = ko.observable({});

    self.updateInclusion = updateInclusion;
    self.removeItem = function(i) {
      removeItem(i);
      updatePublicMap();
    }
    self.resetWith = function(i) {
      resetWith(i);
      updatePublicMap();
    }
    self.addItem = function(i) {
      addItem(i);
      updatePublicMap();
    }
    self.isSelected = isSelected;
    self.clear = function() {
      clearItems();
      updatePublicMap();
    }
    self.batchProperties = function() { return batchProperties; };
  }

  var batchEditorDefaultArgs = {
    getId: function(v) { return v; },
    singleSelectionOnMultiple: true,
    trackProperties: []
  }

  function BatchEditProperty(value, defaultValue) {
    var self = this;

    self.value = ko.observable(value);
    self.isActive = ko.observable(true);

    self.reset = function(isActive) {
      self.value(defaultValue !== undefined ? defaultValue : value);
      self.isActive(isActive);
    }
  }

  function itemsInRange(array, a, b) {
    var i = _.indexOf(array, a);
    var j = _.indexOf(array, b);

    if (i == -1) {
      if (j == -1) {
        return [];
      } else {
        return array.slice(0, j + 1);
      }
    } else {
      if (j == -1) {
        return array.slice(0, i + 1);
      } else {
        return array.slice(Math.min(i, j), Math.max(i, j) + 1);
      }
    }
  }

  // Export
  ns.BatchEditor = BatchEditor;
})(jsu);
