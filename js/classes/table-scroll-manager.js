// DEPENDENCY: jQuery
// DEPENDENCY: utils/general

(function(ns) {
  "use strict";

  function TableScrollManager(selector, args) {
    this.selector = selector;
    this.args = $.extend({}, TableScrollManager.defaultArgs, args);
    this.elem = null; //$(selector);
    this.lastWidth = null;
    this.lastHeight = null;
    this.lastLeft = null;
    this.lastTop = null;
    this.elements = null;
    this.hidden = false;

    this.resetElement();

    if (jsu.util.makeBroadcaster) {
      var tsm = this;
      jsu.util.makeBroadcaster(tsm);
      tsm.headerClicked = function(e) { TableScrollManager.headerClicked(tsm, e); }
    }

    if (this.args.defaultOn) {
      this.turnOn();
    }
  }

  TableScrollManager.prototype.resetElement = function() {
    this.elem = $(this.selector);
    return this.elem;
  }

  TableScrollManager.headerClicked = function(tableScrollManager, e) {
    if (e.isPropagationStopped()) { return; }
    if (!e.target) { return; }
    var th = $(e.target).closest("th")[0];
    if (!th) { return; }
    var div = getAncestor(th, 4);
    if (!div) { return; }
    if (div.className.indexOf(tableScrollManager.args.horizontalTableContainerClass) >= 0) {
      var coords = TableResizer.getCellCoords(th);
      var ofst = 0;
      if (tableScrollManager.args.firstCol) {
        ofst = 1;
      }
      tableScrollManager.trigger("columnHeaderClicked", { eventObject: e, column: coords.x - ofst });
    } else if (div.className.indexOf(tableScrollManager.args.verticalTableContainerClass) >= 0) {
      var coords = TableResizer.getCellCoords(th);
      tableScrollManager.trigger("rowHeaderClicked", { eventObject: e, row: coords.y });
    } else if (div.className.indexOf(tableScrollManager.args.coverTableContainerClass) >= 0) {
      tableScrollManager.trigger("allHeadersClicked", { eventObject: e });
    }
  }

  TableScrollManager.prototype.getDimensions = function() {
    var o = this.elem.offset();

    return {
      width: this.elem.width(),
      height: this.elem.height(),
      top: o ? o.top : 0,
      left: o ? o.left : 0
    };
  }

  TableScrollManager.prototype.dimensionsChanged = function(d) {
    d = d || this.getDimensions();

    return this.lastWidth != d.width || this.lastHeight != d.height || this.lastTop != d.top || this.lastLeft != d.left;
  }

  TableScrollManager.prototype.updateDimensions = function(d) {
    d = d || this.getDimensions();

    this.lastWidth = d.width;
    this.lastHeight = d.height;
    this.lastLeft = d.left;
    this.lastTop = d.top;
  }

  TableScrollManager.prototype.update = function(force, opts) {
    var d = this.getDimensions();

    if (force || this.dimensionsChanged(d)) {
      this.turnOff();
      this.resetElement();
      this.elements = TableScrollManager.makeTableScrollable(this.elem, this.args, opts);
      var tsm = this;
      this.each(function(e) { e.on("mouseup", tsm.headerClicked); });
      this.updateDimensions(d);
      this.hidden = false;
    }
  }

  TableScrollManager.prototype.isOn = function() { return this.elements !== null; }

  TableScrollManager.prototype.turnOn = function() { this.update(true); }

  TableScrollManager.prototype.turnOff = function() {
    if (this.isOn()) {
      var tsm = this;
      this.each(function(e) {
        $(e).off("click", tsm.headerClicked);
        $(e).remove(); });
      this.elements = null;
    }
  }

  TableScrollManager.prototype.toggle = function() {
    if (this.isOn()) {
      this.turnOff();
    } else {
      this.turnOn();
    }
  }

  TableScrollManager.prototype.hide = function() {
    if (this.isOn() && !this.hidden) {
      this.each(function(e) { $(e).css("visibility", "hidden"); });
      this.hidden = true;
    }
  }

  TableScrollManager.prototype.show = function() {
    if (this.isOn() && this.hidden) {
      this.each(function(e) { $(e).css("visibility", "visible"); });
      this.hidden = false;
    }
  }

  TableScrollManager.prototype.toggleVisibility = function() {
    if (this.hidden) {
      this.show();
    } else {
      this.hide();
    }
  }

  TableScrollManager.prototype.each = function(a) {
    for (var k in this.elements) {
      a(this.elements[k], k);
    }
  }

  TableScrollManager.prototype.getColumnWidths = function() {
    if (this.elements.firstRowContainer) {
      var ths = $(this.elements.firstRowContainer).find("thead th");
      var widths = [];
      for (var i = 0; i < ths.length; ++i) {
        widths.push($(ths[i]).css("width"));
      }
      return widths;
    }
    return null;
  }

  // TableScrollManager.prototype.setColumnWidths = function(columnWidths) {
  //   if (this.elements.firstRowContainer) {
  //     var headerThs = $(this.elements.firstRowContainer).find("thead th");
  //     var bodyThs = $(this.elem).find("thead th");
  //     for (var i = 0; i < headerThs.length && i < columnWidths.length; ++i) {
  //       if (!columnWidths[i]) { continue; }
  //       TableScrollManager.setSize(headerThs[i], "width", columnWidths[i]);
  //     }
  //     TableScrollManager.matchSizes(headerThs, bodyThs, this.args.boxSizing, TableScrollManager.dimension.width);
  //   }
  // }

  TableScrollManager.makeTableScrollable = function(selector, args, opts) {
    args = $.extend({}, TableScrollManager.makeTableScrollable.defaultArgs, args);

    var $sel = $(selector);
    var tbl = $sel.children("table");
    var firstRowContainer = null;
    var firstColContainer = null;
    var coverContainer = null;
    var o = $sel.offset();
    var elements = {};
    var pos, top, left, append;

    if (!o) { return; }

    if (args.attachToBody) {
      pos = "fixed";
      top = o.top + "px";
      left = o.left + "px";
      append = "body";
    } else {
      pos = "absolute";
      top = "0px";
      left = "0px";
      append = $sel.parent();
    }


    if (args.firstRow) {
      var firstRow = TableScrollManager.copyFirstRow(tbl);

      firstRowContainer = $("<div></div>").css({
        // "position": "fixed",
        // "top": o.top + "px",
        // "left": o.left + "px",
        "position": pos,
        "top": top,
        "left": left,
        "width": $sel.width() + "px",
        "overflow-x": "hidden"
      })
        .addClass(args.horizontalTableContainerClass)
        .append(firstRow)
        //.appendTo("body");
        .appendTo(append);
      elements.firstRowContainer = firstRowContainer;
    }

    if (args.firstCol) {
      var firstCol = TableScrollManager.copyFirstCol(tbl);
      firstColContainer = $("<div></div>").css({
          // "position": "fixed",
          // "top": o.top + "px",
          // "left": o.left + "px",
          "position": pos,
          "top": top,
          "left": left,
          "height": $sel.height() + "px",
          "overflow-y": "hidden"
        })
          .addClass(args.verticalTableContainerClass)
          .append(firstCol)
          //.appendTo("body");
          .appendTo(append);
      elements.firstColContainer = firstColContainer;
    }

    if (args.firstCol && args.firstRow) {
      var cover = TableScrollManager.getCover(tbl);
      coverContainer = $("<div></div>").css({
        // "position": "fixed",
        // "top": o.top + "px",
        // "left": o.left + "px"
        "position": pos,
        "top": top,
        "left": left,
      })
        .addClass(args.coverTableContainerClass)
        .append(cover)
        //.appendTo("body");
        .appendTo(append);
      elements.coverContainer = coverContainer;
    }

    TableScrollManager.sync($sel, firstRowContainer, firstColContainer, coverContainer, args, opts);

    if (args.resizable) {
      if (args.firstRow) {
        new TableResizer(firstRowContainer, tbl, TableResizer.direction.horizontal, args);
      }
      if (args.firstCol) {
        new TableResizer(firstColContainer, tbl, TableResizer.direction.vertical, args);
      }
    }

    return elements;
  }

  TableScrollManager.makeTableScrollable.defaultArgs = {
    firstRow: true,
    firstCol: true,
    horizontalTableContainerClass: "horizontal-scroll-container",
    verticalTableContainerClass: "vertical-scroll-container",
    coverTableContainerClass: "cover-container",
    boxSizing: "border",
    resizable: true,
    attachToBody: true
  };

  TableScrollManager.matchScroll = function(orig, horiz, verti) {
    if (horiz) { TableScrollManager.matchProp(orig, horiz, "scrollLeft"); }
    if (verti) { TableScrollManager.matchProp(orig, verti, "scrollTop"); }
  }

  TableScrollManager.sync = function(orig, horiz, verti, cover, args, opts) {
    var ignoreOrig = false;
    var ignoreProxies = false;

    TableScrollManager.matchScroll(orig, horiz, verti);

    orig.on("scroll", function(e) {
      if (ignoreOrig) { return; }

      ignoreProxies = true;
      TableScrollManager.matchScroll(orig, horiz, verti);
      ignoreProxies = false;
    });

    if (horiz) {
      horiz.on("scroll", function(e) {
        if (ignoreProxies) { return; }

        ignoreOrig = true;
        TableScrollManager.matchProp(horiz, orig, "scrollLeft");
        ignoreOrig = false;
      });

      var ofr = orig.find("thead th");
      var pfr = horiz.find("thead th");

      if (opts && opts.columnWidths) {
        var cw = opts.columnWidths;
        for (var i = 0; i < ofr.length && i < cw.length; ++i) {
          if (!cw[i]) { continue; }
          TableScrollManager.setSize(ofr[i], "width", cw[i]);
        }
      }

      TableScrollManager.matchSizes(ofr, pfr, args.boxSizing, TableScrollManager.dimension.both);
    }

    if (verti) {
      verti.on("scroll", function(e) {
        if (ignoreProxies) { return; }

        ignoreOrig = true;
        TableScrollManager.matchProp(verti, orig, "scrollTop");
        ignoreOrig = false;
      });

      var ohc = orig.find("thead tr:first-child th:first-child")[0];
      var phc = verti.find("thead tr:first-child th:first-child")[0];

      TableScrollManager.matchSize(ohc, phc, args.boxSizing, TableScrollManager.dimension.both);

      var ofc = orig.find("tbody th");
      var pfc = verti.find("tbody th");

      TableScrollManager.matchSizes(ofc, pfc, args.boxSizing, TableScrollManager.dimension.both);
    }

    if (cover) {
      var ohc = orig.find("thead tr:first-child th:first-child")[0];
      var phc = cover.find("thead tr:first-child th:first-child")[0];

      TableScrollManager.matchSize(ohc, phc, args.boxSizing, TableScrollManager.dimension.both);
    }
  }

  TableScrollManager.matchSizes = function(from, to, sizing, dim, offset) {
    for (var i = 0; i < from.length; ++i) {
      TableScrollManager.matchSize(from[i], to[i], sizing, dim, offset);
    }
  }

  TableScrollManager.setSize = function(elem, dim, v) {
    var css = {};
    css["min-" + dim] = v;
    css["max-" + dim] = v;
    css[dim] = v;

    $(elem).css(css);
  }

  TableScrollManager.matchSize = function(from, to, sizing, dim, offset) {
    dim = dim || TableScrollManager.dimension.both;
    offset = offset || {};

    if ((dim & TableScrollManager.dimension.height) != 0) {
      var h = TableScrollManager.getSize(from, "height", sizing, offset) + "px";
      TableScrollManager.setSize(to, "height", h);
    }

    if ((dim & TableScrollManager.dimension.width) != 0) {
      var w = TableScrollManager.getSize(from, "width", sizing, offset) + "px";
      TableScrollManager.setSize(to, "width", w);
    }
  }

  TableScrollManager.getSize = function(elem, dim, sizing, offset) {
    if (dim == "width") {
      if (sizing == "border") {
        return elem.offsetWidth + (offset.width || 0);
      } else {
        return $(elem).width() + (offset.width || 0);
      }
    } else {
      if (sizing == "border") {
        return elem.offsetHeight + (offset.height || 0);
      } else {
        return $(elem).height() + (offset.height || 0);
      }
    }
  }

  TableScrollManager.matchProp = function(from, to, fprop, tprop) {
    tprop = tprop || fprop;
    if (to[tprop]() != from[fprop]()) {
      to[tprop](from[fprop]());
    }
  }

  TableScrollManager.copyFirstRow = function(tbl) {
    var $tbl = $(tbl);

    return $("<table></table>")
      .attr("class", $tbl.attr("class"))
      .css("width", $tbl.width())
      .append($tbl.children("thead").clone());
  }

  TableScrollManager.copyFirstCol = function(tbl) {
    var $tbl = $(tbl);
    var thead = $("<thead></thead>").append($("<tr></tr>").append(
      $tbl.find("thead tr:first-child th:first-child").clone()));
    var tbody = $("<tbody></tbody>");

    $tbl.children("tbody").children("tr").each(function() {
      tbody.append($("<tr></tr>").append($(this).children("th").clone()));
    });

    return $("<table></table>")
      .attr("class", $tbl.attr("class"))
      .append(thead)
      .append(tbody);
  }

  TableScrollManager.getCover = function(tbl) {
    var $tbl = $(tbl);
    var thead = $("<thead></thead>").append($("<tr></tr>").append($tbl.find("Thead tr:first-child th:first-child").clone()));
    return $("<table></table>")
      .attr("class", $tbl.attr("class"))
      .append(thead);
  }

  TableScrollManager.defaultArgs = $.extend({}, TableScrollManager.makeTableScrollable.defaultArgs, {
    defaultOn: true
  });

  TableScrollManager.dimension = {
    none: 0,
    width: 1,
    height: 2,
    both: 3
  };

  function getAncestor(e, n) {
    n = Math.abs(parseInt(n) || 1);
    var i = 0;
    var c = e;
    while (c && i++ < n) {
      c = c.parentNode;
    }
    return c;
  }



  function TableResizer(container, target, direction, opts) {
    var self = this;

    this.opts = $.extend({}, TableResizer.defaultOpts, opts);
    this.container = $(container);
    this.target = $(target);
    this.direction = direction;
    this.zone = TableResizer.direction.none;
    this.isMouseDown = false;
    this.lastCoords = null;
    this.resizeTarget = null;
    this.hasResized = false;

    this.container.on("mousedown", function(e) { self.onMouseDown(e); });
    this.container.on("mouseup", function(e) { self.onMouseUp(e); });
    this.container.on("mousemove", function(e) { self.onMouseMove(e); });

    if (!TableResizer.on && jsu.util.makeBroadcaster) { jsu.util.makeBroadcaster(TableResizer); }
    if (this.direction != TableResizer.direction.both && this.opts.maintainOppositeAxis) {
      TableResizer.on("resized", function(name, args, sender) { self.otherDirectionResized(args); });
    }
  }

  TableResizer.prototype.onMouseDown = function(e) {
    this.isMouseDown = true;
    this.lastCoords = { x: e.pageX, y: e.pageY };
    this.hasResized = false;
  }

  TableResizer.prototype.onMouseUp = function(e) {
    this.isMouseDown = false;
    this.lastCoords = null;
    if (this.hasResized) {
      this.hasResized = false;
      e.stopPropagation();
      return false;
    }
  }

  TableResizer.prototype.onMouseMove = function(e) {
    if (this.isMouseDown) {
      var coords = { x: e.pageX, y: e.pageY };
      var cellCoords = TableResizer.getCellCoords(this.resizeTarget[0]);
      var proxy = this.resizeTarget[0];
      var targetCell = this.target && TableResizer.getCell(this.target.children("tbody")[0], cellCoords);
      var offset = { width: coords.x - this.lastCoords.x, height: coords.y - this.lastCoords.y };
      var resizedOn = TableResizer.direction.none;

      if ((this.direction & TableResizer.direction.horizontal) != 0 && coords.x != this.lastCoords.x) {
        TableScrollManager.matchSize(proxy, proxy, this.opts.boxSizing, TableScrollManager.dimension.width, offset);
        if (targetCell) {
          TableScrollManager.matchSize(proxy, targetCell, this.opts.boxSizing, TableScrollManager.dimension.width);
        }
        resizedOn = resizedOn | TableResizer.direction.horizontal;
      }

      if ((this.direction & TableResizer.direction.vertical) != 0 && coords.y != this.lastCoords.y) {
        TableScrollManager.matchSize(proxy, proxy, this.opts.boxSizing, TableScrollManager.dimension.height, offset);
        if (targetCell) {
          TableScrollManager.matchSize(proxy, targetCell, this.opts.boxSizing, TableScrollManager.dimension.height);
        }
        resizedOn = resizedOn | TableResizer.direction.vertical;
      }

      this.lastCoords = coords;
      if (resizedOn != TableResizer.direction.none && TableResizer.trigger) {
        this.hasResized = true;
        TableResizer.trigger("resized", { direction: resizedOn });
      }
    } else {
      if (e.target.nodeName != "TH") { return; }

      var lastZone = this.zone;
      this.zone = this.direction & this.getActiveZone(e);
      this.resizeTarget = $(e.target);

      if (lastZone != this.zone) {
        this.setCursor();
      }
    }
  }

  TableResizer.prototype.setCursor = function() {
    var cursor = null;

    switch(this.zone) {
      case TableResizer.direction.none:
        cursor = "auto";
        break;
      case TableResizer.direction.vertical:
        cursor = "ns-resize";
        break;
      case TableResizer.direction.horizontal:
        cursor = "ew-resize";
        break;
      case TableResizer.direction.both:
        cursor = "nwse-resize";
        break;
    }

    this.container.css({ "cursor": cursor });
  }

  TableResizer.prototype.getActiveZone = function(e) {
    var zone = TableResizer.direction.none;
    var box = TableResizer.getBoundingBox(e.target);

    if (e.pageX >= box.left + box.width - this.opts.zoneSize) {
      zone = zone | TableResizer.direction.horizontal;
    }

    if (e.pageY >= box.top + box.height - this.opts.zoneSize) {
      zone = zone | TableResizer.direction.vertical;
    }

    return zone;
  }

  TableResizer.prototype.otherDirectionResized = function(args) {
    if ((args.direction & this.direction) == 0) {
      var selector;
      var dim;
      if (this.direction == TableResizer.direction.horizontal) {
        selector = "thead th";
        dim = TableScrollManager.dimension.width;
      } else {
        selector = "tbody th";
        dim = TableScrollManager.dimension.height;
      }
      var from = this.target.find(selector);
      var to = this.container.find(selector);
      TableScrollManager.matchSizes(from, to, "border", dim);
    }
  }

  TableResizer.getBoundingBox = function(elem) {
    var o = $(elem).offset();

    return {
      left: o.left,
      top: o.top,
      width: elem.offsetWidth,
      height: elem.offsetHeight
    };
  }

  TableResizer.getCellCoords = function(c) {
    var x = 0;
    var y = 0;

    while (c.previousSibling) {
      c = c.previousSibling;
      if (c.nodeName == "TD" || c.nodeName == "TH") {
        x += 1;
      }
    }

    c = c.parentNode;

    while (c.previousSibling) {
      c = c.previousSibling;
      if (c.nodeName == "TR") {
        y += 1;
      }
    }

    return { x: x, y: y };
  }

  TableResizer.getCell = function(cont, coords) {
    var x = 0;
    var y = 0;
    var tr = cont.firstChild;

    while (tr) {
      if (tr.nodeName == "TR") {
        if (y++ == coords.y) {
          break;
        }
      }
      tr = tr.nextSibling;
    }

    if (!tr) { return null; }

    var cl = tr.firstChild;

    while (cl) {
      if (cl.nodeName == "TD" || cl.nodeName == "TH") {
        if (x++ == coords.x) {
          break;
        }
      }
      cl = cl.nextSibling;
    }

    return cl;
  }

  TableResizer.defaultOpts = {
    zoneSize: 5,
    maintainOppositeAxis: true
  };

  TableResizer.direction = {
    none: 0,
    horizontal: 1,
    vertical: 2,
    both: 3
  };

  ns.TableScrollManager = TableScrollManager;
  ns.TableResizer = TableResizer;
})(jsu);
