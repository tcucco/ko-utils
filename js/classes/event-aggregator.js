// DEPENDENCY: underscore

(function(ns) {
  "use strict";

  function EventAggregator() {
    var self = this;

    var specificSubscriptions = {};
    var globalSubscriptions = {};
    var keys = {};
    var id = 0;

    function newSubscriptionKey() {
      return id++;
    }

    self.subscribe = function() {
      switch (arguments.length) {
      case 1:
        if (!_.isFunction(arguments[0])) throw new Error("EventAggregator.subscribe called with one parameter expects a function");
        return subscribeToAll(arguments[0]);
        break;
      case 2:
        if (!_.isString(arguments[0])) throw new Error("EventAggregator.subscribe called with two parameters expects a string as the first parameter");
        if (!_.isFunction(arguments[1])) throw new Error("EventAggregator.subscribe called with two parameters expects a function as the second parameter");
        return subscribeToMessage(arguments[0], arguments[1]);
        break;
      default:
        throw new Error("EventAggregator.subscribe expects either a single function parameter, or a string and a function parameter");
      }
    }

    self.unsubscribe = function(key) {
      var message = keys[key];

      if (message === null) {
        unsubscribeFromAll(key);
      } else {
        unsubscribeFromMessage(message, key);
      }
      return self;
    }

    self.broadcast = function(sender, message, data) {
      if (specificSubscriptions[message]) {
        for (var callback in specificSubscriptions[message]) {
          specificSubscriptions[message][callback](sender, message, data);
        }
      }
      for (var callback in globalSubscriptions) {
        globalSubscriptions[callback](sender, message, data);
      }
      return self;
    }

    function subscribeToMessage(message, callback) {
      if (!specificSubscriptions[message]) {
        specificSubscriptions[message] = {};
      }
      var key = newSubscriptionKey();
      specificSubscriptions[message][key] = callback;
      keys[key] = message;
      return key;
    }

    function subscribeToAll(callback) {
      var key = newSubscriptionKey();
      globalSubscriptions[key] = callback;
      keys[key] = null;
      return key;
    }

    function unsubscribeFromMessage(message, subscriptionKey) {
      if (specificSubscriptions[message] && specificSubscriptions[message][subscriptionKey]) {
        delete specificSubscriptions[message][subscriptionKey];
        delete keys[subscriptionKey];
      }
    }

    function unsubscribeFromAll(subscriptionKey) {
      if (globalSubscriptions[subscriptionKey]) {
        delete globalSubscriptions[subscriptionKey];
        delete keys[subscriptionKey];
      }
    }
  };

  // Export
  ns.EventAggregator = EventAggregator;
})(jsu);
