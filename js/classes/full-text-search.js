// DEPENDENCY: underscore
// DEPENDENCY: utils/general
// DEPENDENCY: utils/regex-builder

(function(ns) {
  "use strict";

  function TextIndex(args) {
    this.properties = _.map(args.properties, TextProperty.parse);
    this.indices = [];
  }

  TextIndex.prototype.createIndex = function(obj) {
    return new ObjectIndex(this, obj);
  };

  TextIndex.prototype.indexObjects = function(objs, extend) {
    var self = this;
    var indices = _.map(objs, function(p) { return self.createIndexPair(p); });
    if (extend) {
      this.indices.push.apply(this.indices, indices)
    } else {
      this.indices = indices;
    }
  };

  TextIndex.prototype.removeFromIndex = function(obj) {
    var index = null;
    for (var i = 0; i < this.indices.length; ++i) {
      var pair = this.indices[i];
      if (pair.subject === obj) {
        index = i;
        break;
      }
    }
    if (index !== null) {
      this.indices.splice(index, 1);
    }
  };

  TextIndex.prototype.updateIndex = function(obj) {
    for (var i = 0; i < this.indices.length; ++i) {
      var pair = this.indices[i];
      if (pair.subject === obj) {
        pair.index.update(pair.subject);
        return;
      }
    }
    this.indices.push(this.createIndexPair(obj));
  };

  TextIndex.prototype.reindex = function() {
    _.each(this.indices, function(i) {
      i.index.update(i.subject);
    });
  };

  TextIndex.prototype.createIndexPair = function(obj) {
    return { subject: obj, index: this.createIndex(obj) };
  };

  TextIndex.prototype.filter = function(textSearches, autoWildcard) {
    // textSearches should be either an array of TextSearch objects (created manually or from
    // TextSearch.parse) or a string that will be sent to TextSearch.parse.

    if (_.isString(textSearches)) {
      textSearches = TextSearch.parse(textSearches, autoWildcard);
    }

    return _.map(
      _.filter(
        this.indices,
        function(pair) { return pair.index.test(textSearches); }
      ),
      function(p) { return p.subject; }
    );
  };


  function ObjectIndex(textIndexDefinition, obj) {
    this.textIndexDefinition = textIndexDefinition;
    this.index = {};
    this.allWords = [];

    if (obj) {
      this.update(obj);
    }
  }

  ObjectIndex.prototype.update = function(obj) {
    var props = this.textIndexDefinition.properties;
    this.index = {};
    for (var i = 0; i < props.length; ++i) {
      var property = props[i];
      var words = property.getWords(obj);
      if (words !== null) {
        this.index[property.identifier] = words;
      }
    }
    this.allWords = _.uniq(_.flatten(_.values(this.index)).sort(), true);
  }

  ObjectIndex.prototype.test = function(textSearches) {
    if (textSearches.length == 1 && textSearches[0].identifier == null) {
      return textSearches[0].test(this.allWords);
    } else {
      for (var tsIndex = 0; tsIndex < textSearches.length; ++tsIndex) {
        var ts = textSearches[tsIndex];

        if (!ts.identifier in this.index) {
          throw "Invalid identifier: " + ts.identifier;
        }

        var words = this.index[ts.identifier];

        if (!ts.test(words)) { return false; }
      }

      return true;
    }
  }


  function Property(name, unwrap) {
    // A property defines how to get a value from an object.

    this.name = name;
    this.unwrap = unwrap;
  }

  Property.prototype.getValue = function(obj) {
    var v = obj[this.name];
    return this.unwrap ? v() : v;
  }

  Property.parse = function(name) {
    if (name.slice(-2) == "()") {
      return new Property(name.slice(0, -2), true);
    } else {
      return new Property(name, false);
    }
  };


  function PropertyChain(properties) {
    // A PropertyChain defines how to get a (potentially) nested value from an object.
    // properties can be either:
    // - a function, which will be called directly on objects to get their value
    // - an array of Property objects.

    this.properties = properties;
  }

  PropertyChain.prototype.getValue = function(obj) {
    if (_.isFunction(this.properties)) {
      return this.properties(obj);
    } else {
      var co = obj;
      for (var i = 0; i < this.properties.length; ++i) {
        var property = this.properties[i];
        co = property.getValue(co);
        if (!jsu.util.isPresent(co)) {
          break;
        }
      }
      return co;
    }
  }

  PropertyChain.parse = function(name) {
    if (_.isFunction(name)) {
      return new PropertyChain(name);
    } else {
      return new PropertyChain(_.map(name.split("."), Property.parse));
    }
  }


  function TextProperty(identifier, propertyChain) {
    // A TextProperty is an identifier and propertyChain pair, defining what name particular values
    // are being indexed under, and how to get the data for that index.

    this.identifier = identifier.toLowerCase();
    this.propertyChain = propertyChain;
  }

  TextProperty.prototype.getWords = function(obj) {
    var v = this.propertyChain.getValue(obj);

    if (v === undefined || v === null) {
      return null;
    }

    if (_.isArray(v)) {
      return _.uniq(_.flatten(_.map(v, normalizeText)).sort(), true);
    } else {
      return normalizeText(v, true);
    }
  }

  TextProperty.parse = function(def) {
    // def can be either an array of:
    // - [pc, id]
    //   where pc is a valid argument for PropertyChain.parse and id is the index identifier
    // - propertyPath
    //   where propertyPath is a valid string for PropertyChain.parse, and identifier will be the
    //   the name of propertyChain.properties[0]. This makes it simple to index a property on a
    //   model by just giving it's name. E.G. index model.description by just passing in
    //   'description'

    var identifier;
    var propertyChain;

    if (_.isArray(def)) {
      propertyChain = PropertyChain.parse(def[0]);
      identifier = def[1];
    } else {
      propertyChain = PropertyChain.parse(def);
      identifier = propertyChain.properties[0].name;
    }

    return new TextProperty(identifier, propertyChain);
  }


  function TextSearch(identifier, words, mode) {
    // A TextSearch defines how to search a text index.
    // - identifier: if given, the specific field to search on
    // - words: an array of strings or regexes to search with
    // - mode: any or all. How many of the words must match.
    // TODO: Change mode to be a number? <= 0 means all, positive integer is count? Can't be more
    //       than length of words?

    this.identifier = identifier;
    this.words = words;
    this.mode = mode || "all";
  }

  TextSearch.prototype.test = function(words) {
    if (!words || words.length == 0) { return false; }

    if (this.mode == "all") {
      for (var i = 0; i < this.words.length; ++i) {
        if (!TextSearch.contains(words, this.words[i])) {
          return false;
        }
      }
      return true;
    } else {
      for (var i = 0; i < this.words.length; ++i) {
        if (TextSearch.contains(words, this.words[i])) {
          return true;
        }
      }
      return false;
    }
  }

  TextSearch.parse = function(def, autoWildcard) {
    // Parse a string into one or more TextSearch objects. If the string is of the form:
    // identifier (: or =) words ... where identifier contains no spaces, there's optional spaces
    // around the method (: or =), and words is one or more space-separated word. The operators
    // determine the search mode. = means all words must match, : means any words can match.
    // If the def string does not match that general pattern, then a single TextSearch object is
    // created with no identifier, and a mode of 'all'.

    var textSearches = [];
    var match;

    while (match = paramSearchRe.exec(def)) {
      var identifier = match[1].toLowerCase();
      var mode = match[2] == "=" ? "all" : "any";
      var words = TextSearch.textToWords(match[3], autoWildcard);
      textSearches.push(new TextSearch(identifier, words, mode));
    }

    if (textSearches.length == 0) {
      textSearches.push(new TextSearch(null, TextSearch.textToWords(def, autoWildcard), "all"));
    }

    return textSearches;
  }

  TextSearch.textToWords = function(words, autoWildcard) {
    var normalizedWords = normalizeText(words, true);

    if (autoWildcard) {
      normalizedWords = _.map(normalizedWords, function(w) { return "*" + w + "*"; });
    }

    return _.map(normalizedWords, TextSearch.textToWord);
  }

  TextSearch.textToWord = function(word) {
    return word.indexOf("*") == -1 ? word : getWildcardRegex(word);
  }

  TextSearch.contains = function(words, word) {
    if (word instanceof RegExp) {
      for (var i = 0; i < words.length; ++i) {
        if (word.test(words[i])) {
          return true;
        }
      }
      return false;
    } else {
      return _.indexOf(words, word, true) != -1;
    }
  }


  function normalizeText(t, uniq) {
    var rawWords = textContentsRe.exec(t.toString())[0].split(textSplitRe);
    var words = _.filter(rawWords, normalizeWord);

    if (uniq) {
      return _.uniq(words.sort(), true);
    }
    return words;
  }

  function normalizeWord(w) {
    return w.toLowerCase().replace(removeFromWordsRe, "");
  }

  function getWildcardRegex(word) {
    return new RegExp("^" + jsu.util.getRegexSafeText(word, "*").replace(/\*+/g, ".*") + "$", "i");
  }

  var paramSearchRe =  /(\w+)\s*(=|\:)(.*?)(?=(?:\s*\w+\s*(?:=|\:))|$)/ig;
  var textContentsRe = /^\s*(.*?)\s*$/;
  var textSplitRe = /[\s\/\-&]+/g;
  var removeFromWordsRe = /['"]|^[,\.\(\)]|[,\.\(\)]$/g;

  // Export
  ns.TextIndex = TextIndex;
  ns.TextSearch = TextSearch;
})(jsu);
