// DEPENDENCY: underscore

(function(ns) {
  "use strict";

  function getX(p) { return p[0]; }
  function getY(p) { return p[1]; }

  function sum(data, extract) {
    extract = extract || _.identity;

    return _.reduce(data, function(t, d) { return t + extract(d); }, 0);
  }

  function average(data, extract, sum_) {
    if (sum_ === undefined) {
      sum_ = sum(data, extract);
    }
    var avg = sum_ / data.length;
    return avg;
  }

  function varianceSum(data, extract, avg) {
    if (avg === undefined) {
      avg = average(data, extract);
    }
    return sum(_.map(data, function(d) { return Math.pow(extract(d) - avg, 2); }), _.identity);
  }

  function variancePopulation(data, extract, avg) {
    return varianceSum(data, extract, avg) / data.length;
  }

  function varianceSample(data, extract, avg) {
    return varianceSum(data, extract, avg) / (data.length - 1);
  }

  function standardDeviationPopulation(data, extract, avg) {
    return Math.sqrt(variancePopulation(data, extract, avg));
  }

  function standardDeviationSample(data, extract, avg) {
    return Math.sqrt(varianceSample(data, extract, avg));
  }

  function linearRegression(data, getX, getY) {
    var sumX = sum(data, getX);
    var sumY = sum(data, getY);
    var sumXY = sum(data, function(p) { return getX(p) * getY(p); });
    var sumX2 = sum(data, function(p) { return Math.pow(getX(p), 2); });
    var n = data.length;
    var slope = (n * sumXY - sumX * sumY) / (n * sumX2 - Math.pow(sumX, 2));
    var intercept = (sumY - slope * sumX) / n;

    return [intercept, slope];
  }

  // TODO: getX / getY?
  function getRollingAveragePoints(points, width, nullValue, type) {
    if (nullValue === undefined) { nullValue = null; }
    if (type === undefined) { type = "centered"; }

    var rollingAveragePoints = [];
    var lradius = null;
    var rradius = null;

    switch (type) {
    case "trailing":
      lradius = width - 1;
      rradius = 1;
      break;
    case "leading":
      lradius = 1;
      rradius = width - 1;
      break;
    case "centered":
    default:
      lradius = Math.floor(width / 2);
      rradius = lradius;
      break;
    }

    for (var i = 0; i < points.length; ++i) {
      var sum = 0;
      var count = 0;

      for (var j = Math.max(i - lradius, 0); j < Math.min(i + rradius, points.length); ++j) {
        var v = points[j][1];
        if (jsu.util.isPresent(v)) {
          sum += v;
          count += 1;
        }
      }

      rollingAveragePoints.push([points[i][0], count === 0 ? nullValue : sum / count]);
    }

    return rollingAveragePoints;
  }

  ns.stats = {
    getX: getX,
    getY: getY,
    sum: sum,
    average: average,
    varianceSum: varianceSum,
    variancePopulation: variancePopulation,
    varianceSample: varianceSample,
    standardDeviationPopulation: standardDeviationPopulation,
    standardDeviationSample: standardDeviationSample,
    linearRegression: linearRegression,
    getRollingAveragePoints: getRollingAveragePoints
  };
})(jsu);
