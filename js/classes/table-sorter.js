// DEPENDENCY: underscore
// DEPENDENCY: knockout

(function(ns) {
  "use strict";

  function TableSorter(columnDefs, opts) {
    opts = _.extend({}, tableSorterDefaultArgs, opts);
    var self = this;
    var sorts = [];

    self.sorts = ko.observable({});

    self.compare = function(l, r) {
      for (var i = 0; i < sorts.length; ++i) {
        var c = columnDefs[sorts[i].columnName].compare(l, r);
        if (c != 0) {
          return sorts[i].direction * c;
        }
      }
      return 0;
    }

    self.addSort = function(columnName) {
      if (!(columnName in columnDefs)) {
        throw "Unknown column name: " + columnName;
      }

      var direction = opts.firstDirection;

      for (var i = 0; i < sorts.length; ++i) {
        if (sorts[i].columnName == columnName) {
          direction = -sorts[i].direction;
          sorts.splice(i, 1);
          break;
        }
      }

      sorts.unshift({ columnName: columnName, direction: direction });

      if (sorts.length > opts.maxSorts) {
        sorts.pop();
      }

      var sortsObj = {};
      for (var i = 0; i < sorts.length; ++i) {
        sortsObj[sorts[i].columnName] = sorts[i].direction;
        if (!opts.showAllSorts) { break; }
      }
      self.sorts(sortsObj);
    }
  }

  var tableSorterDefaultArgs = {
    maxSorts: 3,
    firstDirection: 1,
    showAllSorts: false
  }

  // Export
  ns.TableSorter = TableSorter;
})(jsu);
