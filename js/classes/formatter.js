// DEPENDENCY: moment
// DEPENDENCY: underscore

(function(ns) {
  "use strict";

  var predefinedDateFormats = {
    "ISO": "YYYY-MM-DD",
    "US-Long": "MM/DD/YYYY",
    "US-Short": "MM/DD/YY",
    "US": "MM/DD/YYYY",
    "UK-Long": "DD/MM/YYYY",
    "UK-Short": "DD/MM/YY",
    "UK": "DD/MM/YYYY",
  };
  var predefinedTimeFormats = {
    "24 HR-Long": "HH:mm:ss",
    "24 HR-Short": "HH:mm",
    "24 HR": "HH:mm",
    "12 HR-Long": "h:mm:ss a",
    "12 HR-Short": "h:mm a",
    "12 HR": "h:mm a",
  };
  var formatNumberDefaultOpts = {
    decimalPlaces: -1,
    thousandsSeparator: ",",
    decimalSeparator: ".",
    nullString: "N/A",
    decimalMode: "fixed",
    abbreviate: false,
    prefix: "",
    suffix: "",
    signed: false
  };
  var truncateRepeatingDecimalsRegex = /^(\d*?)(\d+?)\2+$/;
  // NOTE: The trailing \d? will allow this to truncate numbers such as 1.666666667
  var truncateRepeatingDecimalsGreedyRegex = /^(\d*?)(\d+?)\2+\d?$/;
  var stripTrailingZerosRegex = /^(.+?)0*$/;
  var numberBreakpoints = [[1e12, "T"], [1e9, "B"], [1e6, "M"], [1e3, "K"]];
  var stringReplacementRegex = /\{(\d+)\}/g;

  function formatDate(dt, ofmt, ifmt) {
    if (!ofmt) {
      ofmt = predefinedDateFormats["ISO"];
    } else if (ofmt in predefinedDateFormats) {
      ofmt = predefinedDateFormats[ofmt];
    }

    if (ifmt && ifmt in predefinedDateFormats) {
      ifmt = predefinedDateFormats[ifmt];
    }

    // return moment.utc(dt, ifmt).format(ofmt);
    return moment(dt, ifmt).format(ofmt);
  }

  function formatTime(dt, ofmt, ifmt) {
    if (!ofmt) {
      ofmt = predefinedTimeFormats["24 HR"];
    } else if (ofmt in predefinedTimeFormats) {
      ofmt = predefinedTimeFormats[ofmt];
    }

    if (ifmt && ifmt in predefinedTimeFormats) {
      ifmt = predefinedTimeFormats[ifmt];
    }

    // return moment.utc(dt, ifmt).format(ofmt);
    return moment(dt, ifmt).format(ofmt);
  }

  function formatDateTime(dt, dfmt, tfmt, joiner, ifmt) {
    if (joiner === undefined) { joiner == " " };
    return formatDate(dt, dfmt, ifmt) + joiner + formatTime(dt, tfmt, ifmt);
  }

  function formatNumber(n, opts) {
    opts = _.extend({}, formatNumberDefaultOpts, opts);
    var suffix = null;

    if (!_.isNumber(n)) { return opts.nullString; }

    if (opts.abbreviate) {
      for (var i = 0; i < numberBreakpoints.length; ++i) {
        var bp = numberBreakpoints[i];
        if (n >= bp[0]) {
          n = n / bp[0];
          suffix = bp[1];
        }
      }
    }

    var numberString = opts.decimalPlaces >= 0 ? n.toFixed(opts.decimalPlaces) : n.toString();
    var parts = numberString.split(".");

    var whole = parts[0]
    var fractional = parts[1];

    if (n < 0) {
      whole = whole.slice(1);
    }

    if (opts.thousandsSeparator && whole.length > 3) {
      var separatorCount = Math.floor((whole.length - 1) / 3);

      for (var i = 0; i < separatorCount; ++i) {
        var pivot = whole.length - 3 * (i + 1) - i;
        whole = whole.slice(0, pivot) + opts.thousandsSeparator + whole.slice(pivot);
      }
    }

    // if (n < 0) {
    //   whole = "-" + whole;
    // }

    var theNumber = null;

    if (fractional !== undefined) {
      switch (opts.decimalMode) {
      case "trimzeros":
        fractional = fractional.replace(stripTrailingZerosRegex, "$1");
        break;
      case "trimrepeating+":
        fractional = fractional.replace(truncateRepeatingDecimalsGreedyRegex, "$1$2");
        break;
      case "trimrepeating":
        fractional = fractional.replace(truncateRepeatingDecimalsRegex, "$1$2");
        break;
      case "fixed":
      default:
        break;
      }

      theNumber = whole + opts.decimalSeparator + fractional;
    } else {
      theNumber = whole;
    }

    if (suffix) {
      theNumber += suffix;
    }

    var sign = n < 0 ? "-" : opts.signed ? "+" : "";

    return sign + opts.prefix + theNumber + opts.suffix;
  }

  function formatString(str) {
    var re = new RegExp(stringReplacementRegex);
    var parts = [];
    var fromIndex = 0;
    var m = re.exec(str);

    while (m) {
      parts.push(str.slice(fromIndex, m.index));
      var argIndex = parseInt(m[1]);
      var arg = arguments[argIndex + 1];
      if (arg === undefined) {
        throw new Error("Invalid index: " + argIndex + ". In format string " + str);
      }
      parts.push((arg !== null && arg !== undefined) ? arg.toString() : arg);
      fromIndex = re.lastIndex;
      m = re.exec(str);
    }

    parts.push(str.slice(fromIndex));

    return parts.join("");
  }

  function humanizeNumber(n) {
    var n = parseInt(n);
    var mh = n % 100;
    if (mh >= 11 && mh <= 13) { return n + "th"; }
    switch(n % 10) {
      case 1: return n + "st";
      case 2: return n + "nd";
      case 3: return n + "rd";
      default: return n + "th";
    }
  }

  function Formatter(opts) {
    opts = opts || {};
    this.opts = {};
    _.extend(this.opts, formatterDefaultOpts, opts);
    this.opts.numberFormat = _.extend({}, formatterDefaultOpts.numberFormat, opts.numberFormat);
  }

  Formatter.date = formatDate;
  Formatter.time = formatTime;
  Formatter.dateTime = formatDateTime;
  Formatter.number = formatNumber;
  Formatter.string = formatString;
  Formatter.humanizeNumber = humanizeNumber;

  Formatter.prototype.dateFormat = function() { return predefinedDateFormats[this.opts.dateFormat] || this.opts.dateFormat; }
  Formatter.prototype.timeFormat = function() { return predefinedTimeFormats[this.opts.timeFormat] || this.opts.timeFormat; }
  Formatter.prototype.dateTimeFormat = function() { return this.dateFormat() + this.opts.timestampJoiner + this.timeFormat(); }
  Formatter.prototype.date = function(dt, ofmt, ifmt) { return formatDate(dt, ofmt || this.dateFormat(), ifmt); }
  Formatter.prototype.time = function(dt, ofmt, ifmt) { return formatTime(dt, ofmt || this.timeFormat(), ifmt); }
  Formatter.prototype.dateTime = function(dt, dfmt, tfmt, joiner, ifmt) {
    return formatDateTime(dt, dfmt || this.opts.dateFormat, tfmt || this.opts.timeFormat, joiner || this.opts.timestampJoiner, ifmt);
  }
  Formatter.prototype.number = function(n, opts) { return formatNumber(n, _.extend({}, this.opts.numberFormat, opts)); }
  Formatter.prototype.humanizeNumber = humanizeNumber;
  Formatter.prototype.integer = function(n, opts) { return formatNumber(n, _.extend({}, this.opts.numberFormat, opts, { decimalPlaces: 0 })); }
  Formatter.prototype.money = function(n, opts) {
    return formatNumber(n, _.extend({}, this.opts.numberFormat, {
      decimalPlaces: 2,
      prefix: this.opts.numberFormat.currencySymbol,
      decimalMode: "fixed"
    }, opts));
  }
  Formatter.prototype.compactNumber = function(n, opts) {
    return formatNumber(n, _.extend({}, this.opts.numberFormat, {
      decimalPlaces: -1,
      decimalMode: "trimrepeating+"
    }, opts));
  }
  Formatter.prototype.abbrNumber = function(n, opts) {
    return formatNumber(n, _.extend({}, this.opts.numberFormat, {
      decimalPlaces: 2,
      decimalMode: "trimzeros",
      abbreviate: true
    }, opts));
  }
  Formatter.prototype.string = formatString;
  Formatter.dateFormats = _.keys(predefinedDateFormats).sort();
  Formatter.timeFormats = _.keys(predefinedTimeFormats).sort();

  var formatterDefaultOpts = {
    dateFormat: "ISO",
    timeFormat: "24 HR",
    timestampJoiner: " ",
    numberFormat: _.extend({ currencySymbol: "$" }, formatNumberDefaultOpts)
  };

  ns.Formatter = Formatter;
})(jsu);
