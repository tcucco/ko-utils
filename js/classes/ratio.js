(function(ns) {
  "use strict";

  function Ratio(n, d) {
    var self = this;

    self.numerator = n || 0;
    self.denominator = d || 0;
  }

  Ratio.prototype.straightAdd = function(ratio) {
    var self = this;

    self.numerator += ratio.numerator;
    self.denominator += ratio.denominator;

    return self;
  }

  Ratio.prototype.valueOf = function() { return this.denominator ? this.numerator / this.denominator : null; }

  Ratio.prototype.toString = function(fmt) {
    if (!fmt || fmt == "/") {
      return this.numerator + " / " + this.denominator;
    } else if (fmt[0] == "%") {
      var r = this.numerator / this.denominator * 100;

      if (fmt.length > 1) {
        var f = parseInt(fmt.substring(1)) || 0;
        return r.toFixed(f) + "%";
      } else {
        return r.toString() + "%";
      }
    }
  }

  Ratio.copy = function(r) { return new Ratio(r.numerator, r.denominator); }

  Ratio.straightAdd = function(a, b) { return Ratio.copy(a).add(b); }

  // Export
  ns.Ratio = Ratio;
})(jsu);
