# JS Utils

A collection of various JavaScript classes and functions I've developed over the years for use in many projects. Parts of the library depend on jQuery, Underscore, Moment and Knockout.

## Usage

In the future I may use a more capable 'compiler' for the JS code, but in the meantime it all runs through a simple shell script: complie.sh. Example usage:

./compile.sh -L > manifest.txt
cat manifest.txt | ./compile.sh -mc

This will create a list of all available js modules in manifest.txt. You can then exclude modules by placing a # at the start of the line naming that module. Then pipe the manifest.txt file into compile.sh with the -c option to join all the files into one, and the -m option to minify them using the node.js minify command.

For full usage details, see ./compile.sh -h

## What's Here?

There are 3 basic sections:
* KnockoutJS Utilities
* General Utilities
* Other Classes

### KnockoutJS Utilities
I like KnockoutJS a lot, and I use it for all of my front-end JS development. It's very simple and flexible. As such, to do complex things you often need to build it yourself. This set of KnockoutJS utilities does just that. It includes:

* Change tracking
* Error tracking
* Serialized Properties

### General Utilities
An extremely disjointed set of utilities that I commonly use. It includes things such as:

* Object indexing and joining
* Method delegation
* Number formatting
* Random ID generation

### Other Classes
These are utility classes that are generally more complex than the utility functions found in General Utilities. They include things such as:

* Drag Drop Manager
* Event Aggregator
* Expression Parser & Evaluator
* Full Text Indexing and Searching

#### Please note: The structure and content of this library is under heavy revision at the time of this writing. I'm still working on organizing things as I work through several projects that include this code.
